<?php

class Session22 extends CDbHttpSession {

    protected function createSessionTable($db,$tableName){
        switch($db->getDriverName()){
            case 'mysql':
                $blob='LONGBLOB';
                break;
            case 'pgsql':
                $blob='BYTEA';
                break;
            case 'sqlsrv':
            case 'mssql':
            case 'dblib':
                $blob='VARBINARY(MAX)';
                break;
            default:
                $blob='BLOB';
                break;
        }
        $db->createCommand()->createTable($tableName,array(
            'id'=>'CHAR(32) PRIMARY KEY',
            'expire'=>'integer',
            'data'=>$blob,
            'user'=>'varchar(50)',
        ));
    }

    public function writeSession($id,$data) {
        // exception must be caught in session write handler
        // http://us.php.net/manual/en/function.session-set-save-handler.php
        try {
            $expire=time()+$this->getTimeout();
            $db=$this->getDbConnection();
            if($db->getDriverName()=='sqlsrv' || $db->getDriverName()=='mssql' || $db->getDriverName()=='dblib')
                $data=new CDbExpression('CONVERT(VARBINARY(MAX), '.$db->quoteValue($data).')');
            if($db->createCommand()->select('id')->from($this->sessionTableName)->where('id=:id',array(':id'=>$id))->queryScalar()===false)
                $db->createCommand()->insert($this->sessionTableName,array(
                    'id'=>$id,
                    'data'=>$data,
                    'expire'=>$expire,
                    'user'=>Yii::app()->user->id,
                ));
            else
                $db->createCommand()->update($this->sessionTableName,array(
                    'data'=>$data,
                    'expire'=>$expire,
                    'user'=>Yii::app()->user->id,
                ),'id=:id',array(':id'=>$id));
        }
        catch(Exception $e) {
            if(YII_DEBUG)
                echo $e->getMessage();
            // it is too late to log an error message here
            return false;
        }
        return true;
    }


    public function open() {
        if($this->getUseCustomStorage())
            @session_set_save_handler(array($this,'openSession'),array($this,'closeSession'),array($this,'readSession'),array($this,'writeSession'),array($this,'destroySession'),array($this,'gcSession'));

        @session_start();
        $session_id = session_id();
        $userId = -1;
        if (!Yii::app()->user->isGuest)
            $userId = Yii::app()->user->id;

        $data=Yii::app()->db->createCommand()
            ->select('id')
            ->from($this->sessionTableName)
            ->where('expire>:expire AND id=:id',array(':expire'=>time(),':id'=>$session_id))
            ->queryScalar();

        if(YII_DEBUG && session_id()=='') {
            $message=Yii::t('yii','Failed to start session.');
            if(function_exists('error_get_last'))
            {
                $error=error_get_last();
                if(isset($error['message']))
                    $message=$error['message'];
            }
            Yii::log($message, CLogger::LEVEL_WARNING, 'system.web.CHttpSession');
        }
    }
}