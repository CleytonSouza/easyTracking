<?php

Yii::setPathOfAlias('bootstrap', dirname(__FILE__) . '/../extensions/bootstrap');

$serverArquivos = 'http://127.0.0.1';
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'easyTracking',
    'theme' => 'bootstrap',
    'language' => 'pt_br',
    'behaviors' => array('ApplicationUserBehavior'),
    // preloading 'log' component
    //  'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.vendor.mpdf57.*',
    ),
    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'gii',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
            'generatorPaths' => array(
                'bootstrap.gii',
            ),
        ),
    ),
    // application components
    'components' => array(
        'bootstrap' => array(
            'class' => 'bootstrap.components.Bootstrap',
        ),
        'session' => array(
            'class' => 'ASession',
            'autoCreateSessionTable' => true,
            'connectionID' => 'db',
            'sessionTableName' => 'sessions',
        ),
        'mail' => array(
            'class' => 'ext.yii-mail.YiiMail',
            'transportType' => 'smtp',
            'transportOptions' => array(
                'host' => 'smtp.pitneybowes.com.br',
                'username' => 'pmec@pitneybowes.com.br',
                'password' => 'pitney1a',
                'port' => '587',
            ),
            'viewPath' => 'application.views.mail',
            'logging' => true,
            'dryRun' => false
        ),
        'user' => array('allowAutoLogin' => true),
        'widgetFactory' => array(
            'widgets' => array(
                'CJuiDatePicker' => array(
                    'language' => 'pt-BR',
                    'htmlOptions' => array('class' => 'CJuiDatePicker'),
                ),
                'TbDetailView' => array(
                    'nullDisplay' => '',
                ),
            ),
        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost:3306;dbname=easytracking',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'pitneybowesbrasil@123',
            'charset' => 'utf8',
            'tablePrefix' => 'tb_',
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'DbLogRoute',
                    'connectionID' => 'db',
                    'levels' => 'error, warning, trace, info',
                ),
            ),
        ),
    ),
    'params' => array(
        'adminEmail' => 'admin@localhost.com',
        'registerEmail' => 'it@pitneybowes.com.br',
        
         #enviar e-mail 
        'eviaremail' => 'it@pitneybowes.com.br',
        'eviaremail1' => 'it@pitneybowes.com.br',
        'eviaremail2' => 'it@pitneybowes.com.br',

        'esporadicoAddr' => "$serverArquivos:8080/espo",
        'esporadicoArquivos' => "$serverArquivos/vivo/vivo/docs/espo/",
		
		//terra
	    'esporadico_terraAddr' => "$serverArquivos:8080/espo_terra",
        'esporadico_terraArquivos' => "$serverArquivos/vivo/vivo/docs/espo/",
		'deleteEsporadico_terra' => "$serverArquivos:8080/espo_del_terra",
		
        'deleteEsporadico' => "$serverArquivos:8080/espo_del",
        'maloteAddr' => "$serverArquivos:8080/malote",
        'maloteArquivos' => "$serverArquivos/pitneybowes/documentos/Malote/",

        'poolespecialAddr' => "$serverArquivos:8080/poolespecial",
        'poolespecialArquivos' => "$serverArquivos/vivo/vivo/docs/poolespecial/",
        'deletePoolespecial' => "$serverArquivos:8080/poolespecial_del",

        'recebeMaloteAddr' => "$serverArquivos:8080/malote_rec",
        'msgArquivos' => "$serverArquivos/vivo/vivo/docs/Msg/",
        'cartaAddr' => "$serverArquivos:8080/malote_carta",  //carta
        'cartaArquivos' => "$serverArquivos/vivo/vivo/docs/Carta/",
        'deleteMovimento' => "$serverArquivos:8080/carta_del",
        'postagemAddr' => "$serverArquivos:8080/post",
        'postagemArquivos' => "$serverArquivos/vivo/vivo/docs/post/",
        'deletepostagem' => "$serverArquivos:8080/post_del",
        'aprovapostagem' => "$serverArquivos:8080/post_apv",
        'recebepostagem' => "$serverArquivos:8080/post_rec",
        'juridicopostagem' => "$serverArquivos:8080/post_jur",
	  	//'protocoloAddr' => "$serverArquivos:8080/vivon",
        'protocoloArquivos' => "$serverArquivos/vivo/vivo/docs/msg/",
//		'poolAddr'            => "$serverArquivos:8080/pool_carta",
//		'poolArquivos'        => "$serverArquivos/docs/pool/",
//		'deletePool'          => "$serverArquivos:8080/del_pool",
//		'rotaAddr'            => "$serverArquivos:8080/pool_rota",
//		'rotaArquivos'    	  => "$serverArquivos/docs/pool/rota/",
//		'deleteRota'      	  => "$serverArquivos:8080/del_rota",
		'recPoolAddr'     	  => "$serverArquivos:8080/correio",
		'recPoolArquivos' 	  => "$serverArquivos/vivo/vivo/docs/correio/",
//		'arquivo_csvArquivos' => "$serverArquivos/docs/pool/csv/",
//		'captureArquivos'     => "$serverArquivos/docs/pool/rota/",
//		'recebidoArquivos'    => "$serverArquivos/docs/pool/rota/",
		'protocoloAddr' => "$serverArquivos:8080/vivo",
        'protocoloArquivos' => "$serverArquivos/vivo/vivo/docs/msg/",	

		/*
		'malote.codigobarras'=>array(
			'tipo'=>'tamanho', 
			'tamanho'=>15,
		),
		 */
	
		
		'malote.codigobarras'=>array(
			'tipo'=>'ascii', 
			'codigo'=>13,
		),
		
		'protocolo.transportes'=>array(
			'Correios'=>array(
				'CRE' => 'Carta Registrada',
				'CSI' => 'Carta Simples',
				'MDP' => 'Mala Direta Postal',
				'PAC' => 'PAC',
				'REV' => 'Revistas',
				'SEC' => 'Sedex Convencional',
				'SED' => 'Sedex 10',
				'TEL' => 'Telegrama',
				'ECT' => 'ECT_PJ',
				'FAC' => 'FAC',
			),
			'Jurídico'=>array(
				'CRJ'=>'Carta Registrada',
				'ENJ'=>'Envelope',
				'SEJ'=>'Sedex',
				'TEJ'=>'Telegrama',  
				'PRT'=>'Protesto',
			),
			'Malote'=>array(
				'MAL'=>'Malote Lacre',  
				'VEV'=>'Vai e Vem',
                'ENV'=>'Envelope',
                'CX'=>'Caixa',
                'AND'=>'Andar para Andar', 
                'SV'=>'Saca Vermelha - CSC',
                'SVE'=>'Saca Verde - Ambulatório',
                'SA'=>'Saca Azul - Braskem',  				
			),
			'Mensageiro Externo'=>array(
				'CXS'=>'Caixas',  
				'DHL'=>'DHL',  
				'ENV'=>'Envelopes',  
				'FED'=>'FEDEX',  
				'PCT'=>'Pacotes',  
				'SAC'=>'Sacolas',  
				'TNT'=>'TNT',  
				'UPS'=>'UPS',  				
			),
		),
		'report.format'=>array('view', 'csv', 'pdf'),
        'status' => array(
            'Impresso' => 'Aberto',
            'Em Andamento' => 'Aberto',
            'Malote' => 'Aberto',
            'Expedição' => 'Aberto',
            'Entregue' => 'Fechado',
            'Cancelado' => 'Fechado',
        ),
        'aprovacao' => array(
            '' => 'Aberto',
            'Aprovado' => 'Fechado',
            'Não Aprovado' => 'Fechado',
            'Cancelado' => 'fechado',
			'Reimpresso' => 'fechado',
        ),
        'PerfilPadrao' => array(
            'interno' => 1,
            'externo' => 2,
        ),
    ),
);
?>