CREATE DATABASE  IF NOT EXISTS `vivo` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `vivo`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: vivo
-- ------------------------------------------------------
-- Server version	5.5.34

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `vivo_correio`
--

DROP TABLE IF EXISTS `vivo_correio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vivo_correio` (
  `id` int(15) unsigned NOT NULL,
  `uf` varchar(2) NOT NULL,
  `tipo` varchar(2) NOT NULL,
  `inicial` int(10) NOT NULL,
  `final` int(10) NOT NULL,
  `atual` int(10) DEFAULT NULL,
  `data` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `qtd` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vivo_correio`
--

LOCK TABLES `vivo_correio` WRITE;
/*!40000 ALTER TABLE `vivo_correio` DISABLE KEYS */;
INSERT INTO `vivo_correio` VALUES (1,'SP','JL',67397799,67399798,NULL,'05/02/2014','admin',2000);
/*!40000 ALTER TABLE `vivo_correio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vivo_espo`
--

DROP TABLE IF EXISTS `vivo_espo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vivo_espo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `track_expo` char(15) DEFAULT NULL,
  `d_nome` varchar(60) DEFAULT NULL,
  `d_local` varchar(60) DEFAULT NULL,
  `d_numero` varchar(45) DEFAULT NULL,
  `d_andar` varchar(45) DEFAULT NULL,
  `d_baia` varchar(45) DEFAULT NULL,
  `d_bairro` varchar(60) DEFAULT NULL,
  `d_cidade` varchar(60) DEFAULT NULL,
  `d_uf` varchar(2) DEFAULT NULL,
  `d_cep` varchar(9) DEFAULT NULL,
  `d_ramal` varchar(45) DEFAULT NULL,
  `d_area` varchar(45) DEFAULT NULL,
  `d_email` varchar(45) DEFAULT NULL,
  `p_nome` varchar(60) DEFAULT NULL,
  `p_local` varchar(60) DEFAULT NULL,
  `p_numero` varchar(45) DEFAULT NULL,
  `p_andar` varchar(45) DEFAULT NULL,
  `p_baia` varchar(45) DEFAULT NULL,
  `p_bairro` varchar(60) DEFAULT NULL,
  `p_cidade` varchar(60) DEFAULT NULL,
  `p_cep` varchar(9) DEFAULT NULL,
  `p_uf` varchar(2) DEFAULT NULL,
  `p_ramal` varchar(45) DEFAULT NULL,
  `p_area` varchar(45) DEFAULT NULL,
  `p_email` varchar(45) DEFAULT NULL,
  `ts_impressao` timestamp NULL DEFAULT NULL,
  `ts_entrega` timestamp NULL DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `descricao` text,
  `Valor` float DEFAULT NULL,
  `c_custo` varchar(45) DEFAULT NULL,
  `os` varchar(45) DEFAULT NULL,
  `obs` text,
  `servico` varchar(45) DEFAULT NULL,
  `aprovacao` varchar(45) DEFAULT NULL,
  `recebedor` varchar(45) DEFAULT NULL,
  `local` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vivo_espo`
--

LOCK TABLES `vivo_espo` WRITE;
/*!40000 ALTER TABLE `vivo_espo` DISABLE KEYS */;
INSERT INTO `vivo_espo` VALUES (32,'E20140204003131','sa','x',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'x','x','admin@localhost.com','x','x',NULL,'x','',NULL,NULL,NULL,NULL,'x','x','','2014-02-04 02:31:31',NULL,'Impresso','xx',0,'1',NULL,NULL,'Autoboy Avulso','Não Aprovado',NULL,NULL);
/*!40000 ALTER TABLE `vivo_espo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vivo_local`
--

DROP TABLE IF EXISTS `vivo_local`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vivo_local` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) DEFAULT NULL,
  `logradouro` varchar(60) DEFAULT NULL,
  `numero` varchar(45) DEFAULT NULL,
  `andar` varchar(45) DEFAULT NULL,
  `complemento` varchar(45) DEFAULT NULL,
  `bairro` varchar(60) DEFAULT NULL,
  `cidade` varchar(60) DEFAULT NULL,
  `cep` varchar(9) DEFAULT NULL,
  `uf` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vivo_local`
--

LOCK TABLES `vivo_local` WRITE;
/*!40000 ALTER TABLE `vivo_local` DISABLE KEYS */;
INSERT INTO `vivo_local` VALUES (2,'Regional AC','Travessa Campo do Rio Branco','450','','Complemento 436','Centro','Rio Branco','69905-022','AC'),(3,'Regional AM','Av. Djalma Batista','1015','','Parte','Chapada','Mananus','69050-010','AM'),(4,'Regional AP','Rua Tiradentes','1295','','','','Macap','68900-098','AP'),(5,'Regional AL ','Av. Governador Osman Loureiro','49','','Sala 05','','Macei','57037-630','AL'),(6,'Regional BA','Rua Silveira Martins','1036','','','','Salvador ','41150-000','BA'),(7,'Regional CE','Avenida Senador Virgilio Tavora','1001','','','','Fortaleza','60170-250','CE'),(8,'Regional DF','SC/ Norte QD 04 BL B','100','','Sala 1204','Asa Norte','Bras','70310-500','DF'),(9,'Regional ES','Av. Nossa Senhora da Penha','275','','Parte A','Santa Helena','Vit','29055-022','ES');
/*!40000 ALTER TABLE `vivo_local` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vivo_perfil`
--

DROP TABLE IF EXISTS `vivo_perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vivo_perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `admin` tinyint(4) NOT NULL DEFAULT '0',
  `movimento` tinyint(4) NOT NULL DEFAULT '0',
  `transportadora` tinyint(4) NOT NULL DEFAULT '0',
  `delete_movimento` tinyint(4) NOT NULL DEFAULT '0',
  `criar_carta` tinyint(4) NOT NULL DEFAULT '0',
  `criar_malote` tinyint(4) NOT NULL DEFAULT '0',
  `recebe_malote` tinyint(4) NOT NULL DEFAULT '0',
  `pools` tinyint(4) NOT NULL DEFAULT '0',
  `criar_pool` tinyint(4) NOT NULL DEFAULT '0',
  `receber_pool` tinyint(4) NOT NULL DEFAULT '0',
  `delete_pools` tinyint(4) NOT NULL DEFAULT '0',
  `rota` tinyint(4) NOT NULL DEFAULT '0',
  `criar_rota` tinyint(4) NOT NULL DEFAULT '0',
  `delete_rota` tinyint(4) NOT NULL DEFAULT '0',
  `esporadico` tinyint(4) NOT NULL DEFAULT '0',
  `criar_esporadico` tinyint(4) NOT NULL DEFAULT '0',
  `modificar_esporadico` tinyint(4) NOT NULL DEFAULT '0',
  `auto_esporadico` tinyint(4) NOT NULL DEFAULT '0',
  `delete_esporadico` tinyint(4) NOT NULL DEFAULT '0',
  `aprova_esporadico` tinyint(4) NOT NULL DEFAULT '0',
  `postagem` tinyint(4) NOT NULL DEFAULT '0',
  `criar_postagem` tinyint(4) NOT NULL DEFAULT '0',
  `modificar_postagem` tinyint(4) NOT NULL DEFAULT '0',
  `auto_postagem` tinyint(4) NOT NULL DEFAULT '0',
  `delete_postagem` tinyint(4) NOT NULL DEFAULT '0',
  `aprova_postagem` tinyint(4) NOT NULL DEFAULT '0',
  `recebe_postagem` tinyint(4) NOT NULL DEFAULT '0',
  `juridico_postagem` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vivo_perfil`
--

LOCK TABLES `vivo_perfil` WRITE;
/*!40000 ALTER TABLE `vivo_perfil` DISABLE KEYS */;
INSERT INTO `vivo_perfil` VALUES (1,'Administradores',1,1,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1),(2,'Comun',0,1,0,1,1,0,0,0,0,0,0,0,0,0,1,1,0,0,1,0,1,1,0,0,1,0,1,1),(3,'Malote',0,1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(4,'Gerencial',0,1,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1),(5,'Tranportadora Esprádico',0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0),(6,'Esporádico',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `vivo_perfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vivo_pool_carta`
--

DROP TABLE IF EXISTS `vivo_pool_carta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vivo_pool_carta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Track_carta` char(20) NOT NULL,
  `pool` char(20) NOT NULL,
  `ts_impressao` timestamp NULL DEFAULT NULL,
  `ts_malote` varchar(45) DEFAULT NULL,
  `ts_sede` timestamp NULL DEFAULT NULL,
  `ts_transp` timestamp NULL DEFAULT NULL,
  `ts_rota` timestamp NULL DEFAULT NULL,
  `ts_entrega` timestamp NULL DEFAULT NULL,
  `devolucao` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `ts_tentativa1` timestamp NULL DEFAULT NULL,
  `ts_tentativa2` timestamp NULL DEFAULT NULL,
  `ts_tentativa3` timestamp NULL DEFAULT NULL,
  `cad_ccusto` varchar(20) DEFAULT NULL,
  `cad_num_doc` varchar(20) DEFAULT NULL,
  `cad_emitente` varchar(45) DEFAULT NULL,
  `cad_local` varchar(20) DEFAULT NULL,
  `cad_cod_cliente` varchar(20) DEFAULT NULL,
  `cad_nome` varchar(100) DEFAULT NULL,
  `cad_endereco` varchar(100) DEFAULT NULL,
  `cad_bairro` varchar(60) DEFAULT NULL,
  `cad_cidade` varchar(60) DEFAULT NULL,
  `cad_UF` varchar(2) DEFAULT NULL,
  `cad_cep` varchar(9) DEFAULT NULL,
  `cad_obs` varchar(100) DEFAULT NULL,
  `cad_telefone` varchar(20) DEFAULT NULL,
  `cad_cod_entrega` varchar(45) DEFAULT NULL,
  `cad_tipo_doc` varchar(45) DEFAULT NULL,
  `cad_cod_impressao` varchar(20) DEFAULT NULL,
  `cod_instal` varchar(45) DEFAULT NULL,
  `rota` int(1) DEFAULT '0',
  `d_email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vivo_pool_carta`
--

LOCK TABLES `vivo_pool_carta` WRITE;
/*!40000 ALTER TABLE `vivo_pool_carta` DISABLE KEYS */;
/*!40000 ALTER TABLE `vivo_pool_carta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vivo_pool_rota`
--

DROP TABLE IF EXISTS `vivo_pool_rota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vivo_pool_rota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pool` varchar(20) NOT NULL,
  `d_local` varchar(45) DEFAULT NULL,
  `p_local` varchar(45) DEFAULT NULL,
  `ts_impresso` timestamp NULL DEFAULT NULL,
  `ts_malote` timestamp NULL DEFAULT NULL,
  `ts_sede` timestamp NULL DEFAULT NULL,
  `ts_transp` timestamp NULL DEFAULT NULL,
  `ts_rota` timestamp NULL DEFAULT NULL,
  `arquivo_csv` varchar(20) DEFAULT NULL,
  `arquivo_capture` varchar(20) DEFAULT NULL,
  `pool_recebido` varchar(20) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vivo_pool_rota`
--

LOCK TABLES `vivo_pool_rota` WRITE;
/*!40000 ALTER TABLE `vivo_pool_rota` DISABLE KEYS */;
/*!40000 ALTER TABLE `vivo_pool_rota` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vivo_postagem`
--

DROP TABLE IF EXISTS `vivo_postagem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vivo_postagem` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `track_post` char(15) DEFAULT NULL,
  `d_nome` varchar(60) DEFAULT NULL,
  `d_local` varchar(60) DEFAULT NULL,
  `d_numero` varchar(45) DEFAULT NULL,
  `d_andar` varchar(45) DEFAULT NULL,
  `d_baia` varchar(45) DEFAULT NULL,
  `d_bairro` varchar(60) DEFAULT NULL,
  `d_cidade` varchar(60) DEFAULT NULL,
  `d_uf` varchar(2) DEFAULT NULL,
  `d_cep` varchar(9) DEFAULT NULL,
  `d_ramal` varchar(45) DEFAULT NULL,
  `d_area` varchar(45) DEFAULT NULL,
  `d_email` varchar(45) DEFAULT NULL,
  `p_nome` varchar(60) DEFAULT NULL,
  `p_local` varchar(60) DEFAULT NULL,
  `p_numero` varchar(45) DEFAULT NULL,
  `p_andar` varchar(45) DEFAULT NULL,
  `p_baia` varchar(45) DEFAULT NULL,
  `p_bairro` varchar(60) DEFAULT NULL,
  `p_cidade` varchar(60) DEFAULT NULL,
  `p_cep` varchar(9) DEFAULT NULL,
  `p_uf` varchar(2) DEFAULT NULL,
  `p_ramal` varchar(45) DEFAULT NULL,
  `p_area` varchar(45) DEFAULT NULL,
  `p_email` varchar(45) DEFAULT NULL,
  `ts_impressao` timestamp NULL DEFAULT NULL,
  `ts_entrega` timestamp NULL DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `descricao` text,
  `Valor` float DEFAULT NULL,
  `c_custo` varchar(45) DEFAULT NULL,
  `os` varchar(45) DEFAULT NULL,
  `obs` text,
  `servico` varchar(45) DEFAULT NULL,
  `aprovacao` varchar(45) DEFAULT NULL,
  `recebedor` varchar(45) DEFAULT NULL,
  `local` varchar(60) DEFAULT NULL,
  `etiqueta` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vivo_postagem`
--

LOCK TABLES `vivo_postagem` WRITE;
/*!40000 ALTER TABLE `vivo_postagem` DISABLE KEYS */;
INSERT INTO `vivo_postagem` VALUES (1,'E20140204003131','sa','x',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'x','x','admin@localhost.com','x','x',NULL,'x','',NULL,NULL,NULL,NULL,'x','x','','2014-02-04 02:31:31',NULL,'Impresso','xx',0,'1','1234',NULL,'Autoboy Avulso','Não Aprovado',NULL,NULL,NULL),(2,'P20140205165735','Admin','SÃ£o Paulo',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'4569','Desenvolvimento','admin@localhost.com','admin','teste',NULL,'12','',NULL,NULL,NULL,NULL,'asd','asdadfafsd','','2014-02-05 18:57:35',NULL,'Impresso','CobranÃ§a',0,'569874',NULL,NULL,'Carta Simples',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `vivo_postagem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vivo_prot_malote`
--

DROP TABLE IF EXISTS `vivo_prot_malote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vivo_prot_malote` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `track_carta` char(15) DEFAULT NULL,
  `track_malote` char(15) DEFAULT NULL,
  `malote` char(20) DEFAULT NULL,
  `d_nome` varchar(45) DEFAULT NULL,
  `d_local` varchar(45) DEFAULT NULL,
  `d_andar` varchar(45) DEFAULT NULL,
  `d_baia` varchar(45) DEFAULT NULL,
  `d_ramal` varchar(15) DEFAULT NULL,
  `d_area` varchar(45) DEFAULT NULL,
  `d_email` varchar(45) DEFAULT NULL,
  `p_nome` varchar(45) DEFAULT NULL,
  `p_local` varchar(45) DEFAULT NULL,
  `p_andar` varchar(45) DEFAULT NULL,
  `p_baia` varchar(45) DEFAULT NULL,
  `p_ramal` varchar(15) DEFAULT NULL,
  `p_area` varchar(45) DEFAULT NULL,
  `p_email` varchar(45) DEFAULT NULL,
  `ts_impressao` timestamp NULL DEFAULT NULL,
  `ts_malote` timestamp NULL DEFAULT NULL,
  `ts_sup_inicio` timestamp NULL DEFAULT NULL,
  `ts_motoboy_1` timestamp NULL DEFAULT NULL,
  `ts_transp` timestamp NULL DEFAULT NULL,
  `ts_motoboy_2` timestamp NULL DEFAULT NULL,
  `ts_sup_fim` timestamp NULL DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `track_msg` char(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5017 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vivo_prot_malote`
--

LOCK TABLES `vivo_prot_malote` WRITE;
/*!40000 ALTER TABLE `vivo_prot_malote` DISABLE KEYS */;
INSERT INTO `vivo_prot_malote` VALUES (5015,'C20140205160250','M20140205163246','1234','Claudio Terra','Regional RJ',NULL,NULL,'12365','TI','crmt@outlook.com','Admin','SÃ£o Paulo','10','1025','4569','Desenvolvimento','admin@localhost.com','2014-02-05 18:02:50','2014-02-05 18:32:46',NULL,NULL,NULL,NULL,'2014-02-05 18:33:54','Entregue',NULL,NULL),(5016,'C20140205162458','M20140205163246','1234','Claudio Terra','Regional RJ','TI','sala 301','12365','TI','crmt@outlook.com','Admin','SÃ£o Paulo','10','1025','4569','Desenvolvimento','admin@localhost.com','2014-02-05 18:24:58','2014-02-05 18:32:46',NULL,NULL,NULL,NULL,'2014-02-05 18:33:54','Entregue',NULL,NULL);
/*!40000 ALTER TABLE `vivo_prot_malote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vivo_track`
--

DROP TABLE IF EXISTS `vivo_track`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vivo_track` (
  `chave` int(11) NOT NULL AUTO_INCREMENT,
  `tracking` varchar(15) NOT NULL,
  `remetente` varchar(60) NOT NULL,
  `destinatario` varchar(60) NOT NULL,
  `depto` varchar(40) DEFAULT NULL,
  `baia` varchar(20) DEFAULT NULL,
  `andar` varchar(20) DEFAULT NULL,
  `obs` varchar(60) DEFAULT NULL,
  `correio` varchar(30) DEFAULT NULL,
  `transp` varchar(5) DEFAULT NULL,
  `ts_impressao` timestamp NULL DEFAULT NULL,
  `ts_tt1` timestamp NULL DEFAULT NULL,
  `ts_tt2` timestamp NULL DEFAULT NULL,
  `ts_tt3` timestamp NULL DEFAULT NULL,
  `ts_assinatura` timestamp NULL DEFAULT NULL,
  `tipo` varchar(45) NOT NULL,
  PRIMARY KEY (`chave`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vivo_track`
--

LOCK TABLES `vivo_track` WRITE;
/*!40000 ALTER TABLE `vivo_track` DISABLE KEYS */;
INSERT INTO `vivo_track` VALUES (2,'20140203132559','15165','16541','15489','1984','1989','1598951','1948915','SEC','2014-02-03 15:25:59',NULL,NULL,NULL,NULL,'Impresso'),(3,'20140203133807','teste','teste','teste','teste','teste','teste','teste','SEC','2014-02-03 15:38:07',NULL,NULL,NULL,NULL,'Impresso');
/*!40000 ALTER TABLE `vivo_track` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vivo_usuario`
--

DROP TABLE IF EXISTS `vivo_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vivo_usuario` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(60) NOT NULL,
  `senha` varchar(32) NOT NULL,
  `nome` varchar(60) DEFAULT NULL,
  `local` varchar(60) DEFAULT NULL,
  `numero` varchar(45) DEFAULT NULL,
  `andar` varchar(45) DEFAULT NULL,
  `baia` varchar(45) DEFAULT NULL,
  `bairro` varchar(60) DEFAULT NULL,
  `cidade` varchar(60) DEFAULT NULL,
  `cep` varchar(9) DEFAULT NULL,
  `uf` varchar(2) DEFAULT NULL,
  `area` varchar(45) DEFAULT NULL,
  `ramal` varchar(45) DEFAULT NULL,
  `centro_custo` varchar(45) DEFAULT NULL,
  `first` int(11) NOT NULL DEFAULT '1',
  `perfil` int(2) NOT NULL DEFAULT '2',
  `ativo` tinyint(1) DEFAULT NULL,
  `confirmado` tinyint(1) DEFAULT NULL,
  `hash` char(32) DEFAULT NULL,
  `matricula` varchar(45) DEFAULT NULL,
  `nome_local` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8511 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vivo_usuario`
--

LOCK TABLES `vivo_usuario` WRITE;
/*!40000 ALTER TABLE `vivo_usuario` DISABLE KEYS */;
INSERT INTO `vivo_usuario` VALUES (1,'admin@localhost.com','admin','Admin','São Paulo','123','10','1025','Centro','São Paulo','04571-011','SP','Desenvolvimento','4569','569874',1,1,NULL,NULL,NULL,'123456','Regional SP'),(8510,'crmt@outlook.com','terr@880','Claudio Terra','Rua Copacabana','1254','12','1265','Copacabana','rio de janeiro','22020-000','RJ','TI','123654','123456',1,2,1,1,'','123456','Regional RJ');
/*!40000 ALTER TABLE `vivo_usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-02-05 19:51:25
