CREATE DATABASE  IF NOT EXISTS `vivo` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `vivo`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: vivo
-- ------------------------------------------------------
-- Server version	5.6.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `vivo_espo`
--

DROP TABLE IF EXISTS `vivo_espo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vivo_espo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `track_expo` char(15) DEFAULT NULL,
  `d_nome` varchar(45) DEFAULT NULL,
  `d_local` varchar(45) DEFAULT NULL,
  `d_andar` varchar(45) DEFAULT NULL,
  `d_baia` varchar(45) DEFAULT NULL,
  `d_ramal` varchar(15) DEFAULT NULL,
  `d_area` varchar(45) DEFAULT NULL,
  `d_email` varchar(45) DEFAULT NULL,
  `p_nome` varchar(45) DEFAULT NULL,
  `p_local` varchar(45) DEFAULT NULL,
  `p_andar` varchar(45) DEFAULT NULL,
  `p_baia` varchar(45) DEFAULT NULL,
  `p_ramal` varchar(15) DEFAULT NULL,
  `p_area` varchar(45) DEFAULT NULL,
  `p_email` varchar(45) DEFAULT NULL,
  `ts_impressao` timestamp NULL DEFAULT NULL,
  `ts_entrega` timestamp NULL DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `descricao` text,
  `Valor` float DEFAULT NULL,
  `c_custo` varchar(45) DEFAULT NULL,
  `os` varchar(45) DEFAULT NULL,
  `obs` text,
  `servico` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vivo_espo`
--

LOCK TABLES `vivo_espo` WRITE;
/*!40000 ALTER TABLE `vivo_espo` DISABLE KEYS */;
INSERT INTO `vivo_espo` VALUES (26,'E20140131160027','dsf','asdfg',NULL,NULL,'sdfg','sdfag','admin@localhost.com','dfsgh','fdghj','sfdgh','','sdfgh','sdafgh','','2014-01-31 18:00:27','2014-01-09 14:56:00','Entregue','sdafgh',64,'1','12365478',' n\\zkcjbhfqouij asBDOBUIQd asLCODBsd bqdbofdnbwqesa',NULL),(27,'E20140201235851','asdaf','',NULL,NULL,'','','admin@localhost.com','sgfdhfj','','','','fdsgh','','','2014-02-02 01:58:51',NULL,'Impresso','fdasghj',64,'1',NULL,NULL,NULL),(28,'E20140202001314','asdaf','',NULL,NULL,'','','admin@localhost.com','sgfdhfj','','','','fdsgh','','','2014-02-02 02:13:14',NULL,'Impresso','fdasghj',64,'1',NULL,NULL,NULL),(29,'E20140202001341','asvcds','',NULL,NULL,'','','admin@localhost.com','sdfva','sadvfd','','','','asvfd','','2014-02-02 02:13:41',NULL,'Impresso','savcd',64,'1',NULL,NULL,NULL),(30,'E20140202002757','zdasfgbhnb','',NULL,NULL,'','','admin@localhost.com','','','','','','','','2014-02-02 02:27:57',NULL,'Impresso','sfxdgchjmk,',64,'1',NULL,NULL,NULL),(31,'E20140202005115','asdfg','asdfsdg',NULL,NULL,'dfs','sdfd','admin@localhost.com','fdzg','dsfgd','fsgdhf','','sfgdhf','sfzdgsh','','2014-02-02 02:51:15',NULL,'Impresso','sdgf',0,'1',NULL,NULL,'Motoboy Avulso');
/*!40000 ALTER TABLE `vivo_espo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vivo_local`
--

DROP TABLE IF EXISTS `vivo_local`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vivo_local` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(45) DEFAULT NULL,
  `nome` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vivo_local`
--

LOCK TABLES `vivo_local` WRITE;
/*!40000 ALTER TABLE `vivo_local` DISABLE KEYS */;
/*!40000 ALTER TABLE `vivo_local` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vivo_perfil`
--

DROP TABLE IF EXISTS `vivo_perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vivo_perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `movimento` tinyint(4) NOT NULL DEFAULT '0',
  `criar_carta` tinyint(4) NOT NULL DEFAULT '0',
  `criar_malote` tinyint(4) NOT NULL DEFAULT '0',
  `pools` tinyint(4) NOT NULL DEFAULT '0',
  `criar_pool` tinyint(4) NOT NULL DEFAULT '0',
  `receber_pool` tinyint(4) NOT NULL DEFAULT '0',
  `rota` tinyint(4) NOT NULL DEFAULT '0',
  `criar_rota` tinyint(4) NOT NULL DEFAULT '0',
  `admin` tinyint(4) NOT NULL DEFAULT '0',
  `delete_pools` tinyint(4) NOT NULL DEFAULT '0',
  `delete_movimento` tinyint(4) NOT NULL DEFAULT '0',
  `delete_rota` tinyint(4) NOT NULL DEFAULT '0',
  `esporadico` tinyint(4) NOT NULL DEFAULT '0',
  `criar_esporadico` tinyint(4) NOT NULL DEFAULT '0',
  `modificar_esporadico` tinyint(4) NOT NULL DEFAULT '0',
  `auto_esporadico` tinyint(4) NOT NULL DEFAULT '0',
  `delete_esporadico` tinyint(4) NOT NULL DEFAULT '0',
  `transportadora` tinyint(4) NOT NULL DEFAULT '0',
  `recebe_malote` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vivo_perfil`
--

LOCK TABLES `vivo_perfil` WRITE;
/*!40000 ALTER TABLE `vivo_perfil` DISABLE KEYS */;
INSERT INTO `vivo_perfil` VALUES (1,'Administradores',1,1,1,0,0,0,0,0,1,0,1,0,1,1,1,1,1,0,1),(2,'Comun',1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0),(3,'Malote',1,1,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0),(4,'Gerencial',1,1,1,0,0,0,0,0,0,0,1,0,1,1,1,1,1,0,0),(5,'Tranportadora Esprádico',0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0),(6,'Esporádico',0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0);
/*!40000 ALTER TABLE `vivo_perfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vivo_pool_carta`
--

DROP TABLE IF EXISTS `vivo_pool_carta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vivo_pool_carta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Track_carta` char(20) NOT NULL,
  `pool` char(20) NOT NULL,
  `ts_impressao` timestamp NULL DEFAULT NULL,
  `ts_malote` varchar(45) DEFAULT NULL,
  `ts_sede` timestamp NULL DEFAULT NULL,
  `ts_transp` timestamp NULL DEFAULT NULL,
  `ts_rota` timestamp NULL DEFAULT NULL,
  `ts_entrega` timestamp NULL DEFAULT NULL,
  `devolucao` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `ts_tentativa1` timestamp NULL DEFAULT NULL,
  `ts_tentativa2` timestamp NULL DEFAULT NULL,
  `ts_tentativa3` timestamp NULL DEFAULT NULL,
  `cad_ccusto` varchar(20) DEFAULT NULL,
  `cad_num_doc` varchar(20) DEFAULT NULL,
  `cad_emitente` varchar(45) DEFAULT NULL,
  `cad_local` varchar(20) DEFAULT NULL,
  `cad_cod_cliente` varchar(20) DEFAULT NULL,
  `cad_nome` varchar(100) DEFAULT NULL,
  `cad_endereco` varchar(100) DEFAULT NULL,
  `cad_bairro` varchar(60) DEFAULT NULL,
  `cad_cidade` varchar(60) DEFAULT NULL,
  `cad_UF` varchar(2) DEFAULT NULL,
  `cad_cep` varchar(9) DEFAULT NULL,
  `cad_obs` varchar(100) DEFAULT NULL,
  `cad_telefone` varchar(20) DEFAULT NULL,
  `cad_cod_entrega` varchar(45) DEFAULT NULL,
  `cad_tipo_doc` varchar(45) DEFAULT NULL,
  `cad_cod_impressao` varchar(20) DEFAULT NULL,
  `cod_instal` varchar(45) DEFAULT NULL,
  `rota` int(1) DEFAULT '0',
  `d_email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=301311 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vivo_pool_carta`
--

LOCK TABLES `vivo_pool_carta` WRITE;
/*!40000 ALTER TABLE `vivo_pool_carta` DISABLE KEYS */;
/*!40000 ALTER TABLE `vivo_pool_carta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vivo_pool_rota`
--

DROP TABLE IF EXISTS `vivo_pool_rota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vivo_pool_rota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pool` varchar(20) NOT NULL,
  `d_local` varchar(45) DEFAULT NULL,
  `p_local` varchar(45) DEFAULT NULL,
  `ts_impresso` timestamp NULL DEFAULT NULL,
  `ts_malote` timestamp NULL DEFAULT NULL,
  `ts_sede` timestamp NULL DEFAULT NULL,
  `ts_transp` timestamp NULL DEFAULT NULL,
  `ts_rota` timestamp NULL DEFAULT NULL,
  `arquivo_csv` varchar(20) DEFAULT NULL,
  `arquivo_capture` varchar(20) DEFAULT NULL,
  `pool_recebido` varchar(20) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1013 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vivo_pool_rota`
--

LOCK TABLES `vivo_pool_rota` WRITE;
/*!40000 ALTER TABLE `vivo_pool_rota` DISABLE KEYS */;
/*!40000 ALTER TABLE `vivo_pool_rota` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vivo_prot_malote`
--

DROP TABLE IF EXISTS `vivo_prot_malote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vivo_prot_malote` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `track_carta` char(15) DEFAULT NULL,
  `track_malote` char(15) DEFAULT NULL,
  `malote` char(20) DEFAULT NULL,
  `d_nome` varchar(45) DEFAULT NULL,
  `d_local` varchar(45) DEFAULT NULL,
  `d_andar` varchar(45) DEFAULT NULL,
  `d_baia` varchar(45) DEFAULT NULL,
  `d_ramal` varchar(15) DEFAULT NULL,
  `d_area` varchar(45) DEFAULT NULL,
  `d_email` varchar(45) DEFAULT NULL,
  `p_nome` varchar(45) DEFAULT NULL,
  `p_local` varchar(45) DEFAULT NULL,
  `p_andar` varchar(45) DEFAULT NULL,
  `p_baia` varchar(45) DEFAULT NULL,
  `p_ramal` varchar(15) DEFAULT NULL,
  `p_area` varchar(45) DEFAULT NULL,
  `p_email` varchar(45) DEFAULT NULL,
  `ts_impressao` timestamp NULL DEFAULT NULL,
  `ts_malote` timestamp NULL DEFAULT NULL,
  `ts_sup_inicio` timestamp NULL DEFAULT NULL,
  `ts_motoboy_1` timestamp NULL DEFAULT NULL,
  `ts_transp` timestamp NULL DEFAULT NULL,
  `ts_motoboy_2` timestamp NULL DEFAULT NULL,
  `ts_sup_fim` timestamp NULL DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5012 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vivo_prot_malote`
--

LOCK TABLES `vivo_prot_malote` WRITE;
/*!40000 ALTER TABLE `vivo_prot_malote` DISABLE KEYS */;
INSERT INTO `vivo_prot_malote` VALUES (5010,'M20140131154222','20140131154654','12345','Admin','1',NULL,NULL,'wer','1','admin@localhost.com','werty','wert','wert','wert','wert','wert','wert','2014-01-31 17:42:22','2014-01-31 17:46:54','2014-01-31 17:50:46','2014-01-31 17:57:46','2014-01-31 17:58:15','2014-01-31 17:58:44','2014-01-31 17:59:32','Entregue',NULL),(5011,'M20140131154505','M20140201160107','969897','Admin','1',NULL,NULL,'asdf','1','admin@localhost.com','dsfa','sd','f','sd','a','sd','asd','2014-01-31 17:45:05','2014-02-01 18:01:07',NULL,NULL,NULL,NULL,'2014-02-01 18:40:29','Entregue',NULL);
/*!40000 ALTER TABLE `vivo_prot_malote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vivo_track`
--

DROP TABLE IF EXISTS `vivo_track`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vivo_track` (
  `chave` int(11) NOT NULL AUTO_INCREMENT,
  `tracking` varchar(15) NOT NULL,
  `remetente` varchar(60) NOT NULL,
  `destinatario` varchar(60) NOT NULL,
  `depto` varchar(40) DEFAULT NULL,
  `baia` varchar(20) DEFAULT NULL,
  `andar` varchar(20) DEFAULT NULL,
  `obs` varchar(60) DEFAULT NULL,
  `correio` varchar(30) DEFAULT NULL,
  `transp` varchar(5) DEFAULT NULL,
  `ts_impressao` timestamp NULL DEFAULT NULL,
  `ts_tt1` timestamp NULL DEFAULT NULL,
  `ts_tt2` timestamp NULL DEFAULT NULL,
  `ts_tt3` timestamp NULL DEFAULT NULL,
  `ts_assinatura` timestamp NULL DEFAULT NULL,
  `tipo` varchar(45) NOT NULL,
  PRIMARY KEY (`chave`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vivo_track`
--

LOCK TABLES `vivo_track` WRITE;
/*!40000 ALTER TABLE `vivo_track` DISABLE KEYS */;
INSERT INTO `vivo_track` VALUES (1,'20140131162344','teste','teste','teste','teste','teste','teste','teste','CRJ','2014-01-31 18:23:44',NULL,NULL,NULL,NULL,'Impresso');
/*!40000 ALTER TABLE `vivo_track` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vivo_usuario`
--

DROP TABLE IF EXISTS `vivo_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vivo_usuario` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(60) NOT NULL,
  `senha` varchar(32) NOT NULL,
  `nome` varchar(60) DEFAULT NULL,
  `local` varchar(60) DEFAULT NULL,
  `andar` varchar(45) DEFAULT NULL,
  `baia` varchar(45) DEFAULT NULL,
  `ramal` varchar(45) DEFAULT NULL,
  `area` varchar(45) DEFAULT NULL,
  `centro_custo` varchar(45) DEFAULT NULL,
  `first` int(11) DEFAULT NULL,
  `perfil` int(2) DEFAULT '2',
  `ativo` tinyint(1) DEFAULT NULL,
  `confirmado` tinyint(1) DEFAULT NULL,
  `hash` char(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8508 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vivo_usuario`
--

LOCK TABLES `vivo_usuario` WRITE;
/*!40000 ALTER TABLE `vivo_usuario` DISABLE KEYS */;
INSERT INTO `vivo_usuario` VALUES (1,'admin@localhost.com','admin','Admin','1','1','1','','1','1',1,1,NULL,NULL,NULL),(8507,'danielbastos@danielbastos.kinghost.net','sasdasd','teste','asd',NULL,NULL,'asd','asd','asd',NULL,2,1,0,'f2f2a3fb9b6f3a44147c9bb25de755f3');
/*!40000 ALTER TABLE `vivo_usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-02-02  5:28:09
