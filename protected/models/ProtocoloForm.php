<?php

class ProtocoloForm extends CFormModel {

	public $track;
	public $d_nome;
	public $p_nome;
	public $p_area;
	public $p_baia;
	public $p_andar;
	public $d_email;
	public $u_empresa;
	public $obs;
	public $codCorreios;
	public $transp;
	
	public function init(){
		$this->setUsuarioOrigem();
		} 
	
	public function setUsuarioOrigem(){
		$model = Usuario::model()->findByPk(Yii::app()->user->id);
		$this->d_email = $model->email;
		$this->u_empresa = $model->empresa;
		
	}

	public function rules() {
		return array(
			array('track, d_nome, p_nome, p_area, p_baia, p_andar, p_email, obs, codCorreios, transp, u_empresa,', 'safe'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
			'track'           => 'Carta',
			'd_nome'          => 'De',
			'p_nome'          => 'Para',
			'u_empresa'       => 'Empresa',
			'p_area'          => 'Área',
			'p_baia'          => 'Baia \ Complemento',
			'p_andar'         => 'Andar',
			'obs'             => 'Observação',
			'codCorreios'     => 'Código dos correios',
			'transp'          => 'Transp'
			);
	}
}
