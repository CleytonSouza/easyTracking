<?php

/**
 * @property string $d_nome
 * @property string $d_local
 * @property string $d_andar
 * @property string $d_baia
 * @property string $d_ramal
 * @property string $d_area
 * @property string $d_email
 * @property string $p_nome
 * @property string $p_local
 * @property string $p_andar
 * @property string $p_baia
 * @property string $p_ramal
 * @property string $p_area
 * @property string $p_email
 */

class Carta extends CFormModel {

	public $d_nome;
	public $d_local;
	public $d_andar;
	public $d_baia;
	public $d_ramal;
	public $d_area;
	public $d_email;
	public $p_nome;
	public $p_local;
	public $p_andar;
	public $p_baia;
	public $p_ramal;
	public $p_area;
	public $p_email;
	public $texto;
	public $u_empresa;


	public function init(){
		$this->setUsuarioOrigem();
	}
	
	public function setUsuarioOrigem(){
		$model = Usuario::model()->findByPk(Yii::app()->user->id);
		$this->d_nome  = $model->nome;
		$this->d_local = $model->nome_local;
		$this->d_andar = $model->andar;
		$this->d_baia  = $model->baia;
		$this->d_ramal = $model->ramal;
		$this->d_area  = $model->area;
		$this->d_email = $model->email;
		$this->u_empresa = $model->empresa;

	}
	
	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules() {
		return array(
//		array('d_ramal, d_area, p_ramal, p_area', 'required'),
		array('d_nome, d_local, d_andar, d_baia, d_ramal, d_area, p_nome, p_local, p_andar, p_baia, p_ramal, p_area, d_email, p_email, texto, u_empresa', 'safe'),
			array('d_nome, d_local, d_andar, d_baia, d_area, p_nome, p_local, p_andar, p_baia, p_area, d_email, p_email,, u_empresa', 'length', 'max' => 45),
			array('d_area, p_area', 'length', 'max' => 150),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
			'd_email'         => 'Email',
			'd_nome'          => 'Nome',
			'd_local'         => 'Localidade Usuário',
			'd_andar'         => 'Andar',
			'd_baia'          => 'Baia \ Complemento',
			'd_ramal'         => 'Ramal (DD 9999-99999)',
			'd_area'          => 'Area',
			
			'p_email'         => 'Email',
			'p_nome'          => 'Nome',
			'p_local'         => 'Localidade Destino',
			'p_andar'         => 'Andar',
			'p_baia'          => 'Baia \ Complemento',
			'p_ramal'         => 'Ramal (DD 9999-99999)',
			'p_area'          => 'Area',
			'texto'           => 'Observação',
			'u_empresa'       => 'Empresa do usuário'
			);
	}
}
