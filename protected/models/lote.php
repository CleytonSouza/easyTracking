<?php




class Lote extends CFormModel {
   
   //Declaração de Variáveis
	
	public $local_origem;
	public $local_destino;
	public $de;
	public $para;
	public $tracks;
	public $obs;
    public $transporte;
    public $data_chegada;
	public $d_email;
	public $u_empresa;
	
	public function init(){
		$this->setUsuarioOrigem();
		} 
	
	public function setUsuarioOrigem(){
		$model = Usuario::model()->findByPk(Yii::app()->user->id);
		$this->d_email = $model->email;
		$this->u_empresa = $model->empresa;
		
	}

	public function rules() {
		return array(
			
            /* Campo Obrigatório */
            array('de, para', 'required'),
			array('local_origem, transporte, local_destino, de, para, tracks, obs, data_chegada, d_email, u_empresa', 'safe'),
			
			//Local
			array('local_origem', 'length'),
			array('local_destino', 'length'),
			
			//de -> para
			array('de, para', 'length', 'max' => 150),
			array('obs','length', 'max'=>300),
			
			array('data_chegada', 'length'),

			//tracks
			array('tracks', 'length', 'max' => 15),

		);
	}
        /**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
            'id'            => 'ID', 
			'local_origem'  => 'Local de Origem',
			'local_destino' => 'Local de Destino',
			'de'            => 'De',
			'para'          => 'Para',
			'tracks'        => 'Código de barras',
			'obs'           => 'Observação',
			'transporte'    => 'Transporte',
			'data_chegada'  => 'Data Chegada',
			'd_email'       => 'E-mail Solicitante',
			'u_empresa'     => 'Empresa do usuário',
		);
	}


public function search() {
  // @todo Please modify the following code to remove attributes that should not be searched.

	$criteria = new CDbCriteria;
               
        $criteria->compare('id', $this->id, true);
		$criteria->compare('local_destino', $this->local_destino, true);
		$criteria->compare('local_origem', $this->local_origem, true);
		$criteria->compare('de', $this->de, true);
		$criteria->compare('para', $this->para, true);
		$criteria->compare('tracks', $this->tracks, true);
		$criteria->compare('obs', $this->obs, true);
		$criteria->compare('transporte', $this->transporte, true);
		$criteria->compare('data_chegada', $this->data_chegada, true);
		$criteria->compare('d_email', $this->d_email, true);
		$criteria->compare('u_empresa', $this->u_empresa, true);
                
                return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
}	           
/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Esporadico the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
}