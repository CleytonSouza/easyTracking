<?php

/**
 * @property string $lacre
 * @property string $de
 * @property string $para
 * @property array $tracks
 * @property array $quantidade
 * @property array $p_andar
 * @property array $obs
 * @property array $identificador
 */


class Malote extends CFormModel {
   
   //Declaração de Variáveis
	
	public $lacre;
	public $de;
	public $para;
	public $tracks;
	public $quantidade;
	public $p_andar;
	public $doc_sem_n;
    public $servico;
	public $obs;
	public $u_empresa;
	public $d_email;

	public function init(){
		$this->setUsuarioOrigem();
		} 
	
	public function setUsuarioOrigem(){
		$model = Usuario::model()->findByPk(Yii::app()->user->id);
		$this->d_email = $model->email;
		$this->u_empresa = $model->empresa;
		
	}
		

	public function rules() {
		return array(
			
            /* Campo Obrigatório */
             array('lacre, de, para, servico', 'required'),
			array('lacre, de, para, tracks, quantidade, p_andar, doc_sem_n, obs, servico, u_empresa, d_email', 'safe'),
			//Lacre
			array('lacre', 'length', 'max' => 22),
			array('quantidade','length', 'max'=>8),
			array('p_andar', 'length', 'max'=> 8),
			
			//de -> para
			array('de, para', 'length', 'max' => 60),
			array('obs','length', 'max'=>150),
			array('d_email', 'length', 'max'=> 40),
			array('u_empresa', 'length', 'max'=> 40),
			

			//tracks
			array('tracks', 'length', 'max' => 100),

		);
	}
        /**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
                        'id'            => 'ID', 
			'lacre'         => 'Lacre',
			'de'            => 'De',
			'para'          => 'Para',
			'tracks'        => 'Tracking Cartas',
			'quantidade'    => 'Quantidade',
			'p_andar'       => 'Andar',
			'doc_sem_n'     => 'Documentos sem numeração',
			'servico'       => 'Servico',
			'obs'           => 'Observação',
			'd_email'       => 'E-mail Solicitante',
			'u_empresa'     => 'Empresa do usuário',
		);
	}


public function search() {
  // @todo Please modify the following code to remove attributes that should not be searched.

	$criteria = new CDbCriteria;
               
        $criteria->compare('id', $this->id, true);
		$criteria->compare('lacre', $this->lacre, true);
		$criteria->compare('de', $this->de, true);
		$criteria->compare('para', $this->para, true);
		$criteria->compare('tracks', $this->tracks, true);
		$criteria->compare('quantidade', $this->quantidade, true);
		$criteria->compare('p_andar', $this->p_andar, true);
		$criteria->compare('doc_sem_n', $this->doc_sem_n, true);
		$criteria->compare('servico', $this->servico, true);
		$criteria->compare('obs', $this->obs, true);
		$criteria->compare('d_email', $this->d_email, true);
		$criteria->compare('u_empresa', $this->u_empresa, true);
                
                return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
}	           
/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Esporadico the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
}