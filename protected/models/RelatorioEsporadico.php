<?php

/**
 * This is the model class for table "{{aes_espo}}".
 *
 * The followings are the available columns in table '{{aes_espo}}':
 * @property string $id
 * @property string $track_expo
 * @property string $d_nome
 * @property string $d_local
 * @property string $d_andar
 * @property string $d_baia
 * @property string $d_ramal
 * @property string $d_area
 * @property string $d_email
 * @property string $p_nome
 * @property string $p_local
 * @property string $p_andar
 * @property string $p_baia
 * @property string $p_ramal
 * @property string $p_area
 * @property string $p_email
 * @property string $ts_impressao
 * @property string $ts_entrega
 * @property string $status
 * @property string $descricao
 * @property string $Valor
 * @property string $c_custo
 * @property string $os
 * @property string $obs
 * @property string $b_area
 */

class RelatorioEsporadico extends ActiveRecord {

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{espo}}';
	}
    
	public function getCartaLink(){
		return CHtml::link($this->track_expo, Yii::app()->params["Esporadico_Arquivos"].$this->track_expo.".pdf");
	}
	
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
        //array('d_ramal, d_area, p_ramal, p_area', 'required'),
		array('Valor','numerical'),
			array('track_expo', 'length', 'max' => 15),
			array('track_expo, d_nome, ts_impressao, ts_entrega, descricao, obs, d_local, d_andar, d_baia, d_ramal, d_area, d_email, p_nome, p_local, p_andar, p_baia, p_ramal, p_area, p_email, c_custo, Valor', 'safe', 'on' => 'search'),
			array('d_numero, d_andar, d_baia, d_ramal, d_area, p_andar, p_baia, p_ramal, p_area, p_email', 'length', 'max' => 45),
            array('d_local, p_local', 'length', 'max' => 60),
            array('status, os', 'length', 'max' => 45),
			array('d_uf, p_uf', 'length', 'max' => 2),
			array('d_cep, p_cep', 'length', 'max' => 9),
			array('ts_impressao, ts_entrega', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id'                       => 'ID',
			'track_expo'               => 'track_expo',
			'd_nome'                   => 'd_nome',
            'd_local'                  => 'd_local',
            'd_andar'                  => 'd_andar',
			'd_baia'                   => 'd_baia',
			'd_ramal'                  => 'd_ramal',
			'd_area'                   => 'd_area',
			'd_email'                  => 'd_email',
			'p_nome'                   => 'p_nome',
			'p_local'                  => 'p_local',
			'p_andar'                  => 'p_andar',
			'p_baia'                   => 'p_baia',
			'p_ramal'                  => 'p_ramal',
			'p_area'                   => 'p_area',
			'p_email'                  => 'p_email',
			'ts_impressao'             => 'ts_impressao',
			'ts_entrega'               => 'ts_entrega',
			'status'                   => 'status',
			'descricao'                => 'descricao',
			'Valor'                    => 'Valor',
			'c_custo'                  => 'c_custo',
			'os'                       => 'os',
			'obs'                      => 'obs',
			'b_area'                   => 'b_area',
						
		);
	}
	
	//	public function getCartaLink(){
	//	return CHtml::link($this->track_expo, Yii::app()->params["esporadicoArquivos"].$this->track_expo.".pdf");		
	//}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		
	
		$criteria->compare('id', $this->id, true);
		$criteria->compare('track_expo', $this->track_expo, true);
        $criteria->compare('d_nome', $this->d_nome, true);
		$criteria->compare('d_local', $this->d_local, true);
		$criteria->compare('d_andar', $this->d_andar, true);
		$criteria->compare('d_baia', $this->d_baia, true);
		$criteria->compare('d_ramal', $this->d_local, true);
		$criteria->compare('d_area', $this->d_local, true);
		$criteria->compare('d_email', $this->d_local, true);
		$criteria->compare('p_nome', $this->d_local, true);
		$criteria->compare('p_local', $this->d_local, true);
		$criteria->compare('p_andar', $this->d_local, true);
		$criteria->compare('p_baia', $this->d_local, true);
		$criteria->compare('p_ramal', $this->d_local, true);
		$criteria->compare('p_area', $this->d_local, true);
		$criteria->compare('p_email', $this->d_local, true);
        $criteria->compare('ts_impressao', $this->convertToFindDate($this->ts_impressao), true);
        $criteria->compare('ts_entrega', $this->convertToFindDate($this->ts_entrega), true);
        $criteria->compare('status', $this->d_local, true);
        $criteria->compare('descricao', $this->d_local, true);
        $criteria->compare('Valor', $this->d_local, true);
        $criteria->compare('c_custo', $this->d_local, true);
		$criteria->compare('os', $this->d_local, true);
        $criteria->compare('obs', $this->d_local, true);
        $criteria->compare('b_area', $this->d_local, true);
       Yii::app()->session['RelatorioEsporadico.criteria'] = $criteria;
		return new CActiveDataProvider($this, array('criteria'=>$criteria));
				
	
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Esporadico the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

}