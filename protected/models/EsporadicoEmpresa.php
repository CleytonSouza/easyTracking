<?php

/**
 * This is the model class for table "{{expo}}".
 *
 * The followings are the available columns in table '{{expo}}':
 * @property string $id
 * @property string $track_expo
 * @property string $d_nome
 * @property string $d_local
 * @property string $d_andar
 * @property string $d_baia
 * @property string $d_ramal
 * @property string $d_area
 * @property string $d_email
 * @property string $p_nome
 * @property string $p_local
 * @property string $p_andar
 * @property string $p_baia
 * @property string $p_ramal
 * @property string $p_area
 * @property string $p_email
 * @property string $ts_impressao
 * @property string $ts_entrega
 * @property string $status
 * @property string $descricao
 * @property double $valor
 * @property string $c_custo
 * @property string $os
 * @property text $obs
 * @property text $centro_custo2
 * @property String $km
 * @property String $eventos
 */
class EsporadicoEmpresa extends ActiveRecord {


	public $d_nome;
	public $d_local;
	public $d_numero;
	public $d_andar;
	public $d_baia;
	public $d_bairro;
	public $d_cidade;
	public $d_uf;
	public $d_cep;
	public $d_ramal;
	public $d_area;
	public $d_email;
	public $d_ccusto;
	public $texto;
	public $servico;
	public $recebedor;
	public $c_custo_cobranca;
	public $km;
	public $path_Assinatura;
	public $empresa;
	public $filial;
	public $eventos;
	public $obs;
	public $endereco_entrega;
	public $motos;
	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{espo}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
//		array('d_ramal, d_area, p_ramal, p_area', 'required'),
			array('track_expo, path_Assinatura', 'length', 'max' => 22),
			array('track_expo, path_Assinatura, motos, endereco_entrega, ts_impressao, ts_entrega, obs, d_nome, d_local, d_numero, d_andar, d_baia, d_bairro, d_cidade, d_uf, d_cep, d_ramal, d_area, d_email, c_custo, servico, aprovacao, local, recebedor,  c_custo_cobranca, km, eventos, empresa, filial', 'safe', 'on' => 'search'),
			array('d_numero, d_andar, d_baia, d_ramal, d_area, c_custo, servico, recebedor, c_custo_cobranca, km, eventos', 'length', 'max' => 45),
            array('d_nome, d_local, d_bairro, d_cidade, endereco_entrega, d_email, recebedor, km, eventos', 'length', 'max' => 60),
            array('status', 'length', 'max' => 45),
			array('motos', 'length', 'max' => 50),
			array('empresa', 'length', 'max' => 40),
			array('d_uf', 'length', 'max' => 2),
			array('d_cep', 'length', 'max' => 9),
			array('ts_impressao, ts_entrega, obs, recebedor, km, eventos, empresa', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'track_expo'      => 'Tracking',
			'path_Assinatura' => 'Tracking Assinatura',
			'd_nome'          => 'Nome',
			'd_email'         => 'Email',
			'd_nome'          => 'Nome',
			'd_local'         => 'Logradouro',
			'd_numero'        => 'Número',
			'd_bairro'        => 'Bairro',
			'd_cidade'        => 'Cidade',
			'd_uf'            => 'Estado',
			'd_cep'           => 'CEP',
			'd_andar'         => 'Andar',
			'd_baia'          => 'Baia',
			'd_ramal'         => 'Ramal (DD 9999-99999)',
			'd_area'          => 'Area',
			'ts_impressao'    => 'Impressão',
			'ts_entrega'      => 'Entrega',
			'status'          => 'Status',
			'c_custo'         => 'C Custo',
			'obs'             => 'Observação',
			'servico'         => 'Tipo de Serviço',
			'recebedor'       => 'Nome Recebedor',
			'c_custo_cobranca' => 'C.Custo Cobrança',
			'km' => 'km',
			'eventos' => 'eventos',
			'empresa' => 'Empresa',
			'filial' => 'Filial',
			'endereco_entrega' => 'Entrega',
			'motos' => 'motos'

		);
	}
	
	public function getTrackLink(){
		return CHtml::link($this->track_expo, Yii::app()->params["esporadicoArquivos"].$this->track_expo.".pdf");
		//return CHtml::link($this->path_Assinatura, Yii::app()->params["esporadicoArquivos1"].$this->path_Assinatura.".jpg");
				
	}

    public function getTrackLink1(){
    	
		//return CHtml::link($this->path_Assinatura, Yii::app()->params["esporadicoArquivos1"].$this->path_Assinatura.".jpg");	
	}
 // $link = Yii::app()->params['esporadicoArquivos'].$filename;


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		
         if ((!Yii::app()->user->isAdmin) && (!Yii::app()->user->isGeren) && (!Yii::app()->user->isTransp)){
			$criteria->compare('d_email', Yii::app()->user->email, false, 'AND');
			$criteria->compare('u_empresa', Yii::app()->user->empresa, false);
		}
		
		else if ((Yii::app()->user->isGeren)) {
			$criteria->compare('u_empresa', Yii::app()->user->empresa, false);
		}
		
		else if ((Yii::app()->user->isTransp)) {
			$criteria->compare('empresa', Yii::app()->user->empresa, false);
		}

        $criteria->compare('id', $this->id, true);
		$criteria->compare('track_expo', $this->track_expo, true);
		$criteria->compare('path_Assinatura', $this->path_Assinatura, true);
		$criteria->compare('d_nome', $this->d_nome, true);
		$criteria->compare('d_local', $this->d_local, true);
		$criteria->compare('d_andar', $this->d_andar, true);
		$criteria->compare('d_baia', $this->d_baia, true);
		$criteria->compare('d_ramal', $this->d_ramal, true);
		$criteria->compare('d_area', $this->d_area, true);
		$criteria->compare('d_email', $this->d_email, true);
		$criteria->compare('ts_impressao', $this->convertToFindDate($this->ts_impressao), true);
		$criteria->compare('ts_entrega', $this->convertToFindDate($this->ts_entrega), true);
		$criteria->compare('status', $this->status, true);
		$criteria->compare('c_custo', $this->c_custo, true);
		$criteria->compare('d_numero', $this->d_numero, true);		
		$criteria->compare('d_bairro', $this->d_bairro, true);
		$criteria->compare('d_cidade', $this->d_cidade, true);
		$criteria->compare('d_uf', $this->d_uf, true);
		$criteria->compare('d_cep', $this->d_cep, true);	
		$criteria->compare('servico', $this->servico);
		$criteria->compare('recebedor', $this->recebedor);
		$criteria->compare('obs', $this->obs);
		$criteria->compare('c_custo_cobranca', $this->c_custo_cobranca, true);	
		$criteria->compare('km', $this->km, true);
		$criteria->compare('eventos', $this->eventos, true);
		$criteria->compare('empresa', $this->empresa, true);
		$criteria->compare('filial', $this->filial, true);
		$criteria->compare('motos', $this->motos, true);
		$criteria->compare('endereco_entrega', $this->endereco_entrega, true);
		Yii::app()->session['espo.criteria'] = $criteria;
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => array('ts_impressao' => true),
			)	
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Esporadico the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

}
