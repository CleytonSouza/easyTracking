<?php

class ProtocoloMobileForm extends CFormModel {

	public $etiqueta;
	public $track_v;
	public $nome_d;
	public $d_email;
	public $u_empresa;
	public $d_nome;
	public $p_nome;
	public $p_area;
	public $p_baia;
	public $p_andar;
	public $obs;
	public $codCorreios;
	public $transp;
	
	public function init(){
		$this->setUsuarioOrigem();
		} 
	
	public function setUsuarioOrigem(){
		$model = Usuario::model()->findByPk(Yii::app()->user->id);
		$this->d_email = $model->email;
		$this->u_empresa = $model->empresa;
		
	} 

	public function rules() {
		return array(
			array('p_nome, etiqueta', 'required'),
			array('etiqueta, u_empresa, track_v, nome_d, email_d, d_nome, p_nome, p_area, p_baia, p_andar, p_email, obs, codCorreios, transp', 'safe'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
			'track_v'         => 'Carta',
			'etiqueta'        => 'Código da Etiqueta',
			'u_empresa'       => 'Empresa',
			'nome_d'          => 'Nome do Destinatário',
			'email_d'         => 'E-mail do Destinatário ',
			'd_nome'          => 'De',
			'p_nome'          => 'Para',
			'p_area'          => 'Área',
			'p_baia'          => 'Baia \ Complemento',
			'p_andar'         => 'Andar',
			'obs'             => 'Observação',
			'codCorreios'     => 'Código dos correios',
			'transp'          => 'Transp'
			);
	}
}
