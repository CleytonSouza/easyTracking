<?php

class EsporadicoForm extends CFormModel {

	public $d_nome;
	public $d_local;
	public $d_numero;
	public $d_andar;
	public $d_baia;
	public $d_bairro;
	public $d_cidade;
	public $d_uf;
	public $d_cep;
	public $d_ramal;
	public $d_area;
	public $d_email;
	public $d_ccusto;
	public $texto;
	public $valor;
	public $servico;
	public $aprovacao;
	public $local;
	public $recebedor;
	public $os;
	public $opcionalcentrocusto;
	public $centro_custo2;
	public $atendimento;
	public $empresa;
	public $dispositivos;
	public $km;
	public $eventos;

	public function init(){
		$this->setUsuarioOrigem();
		$this->valor = '0,00';
		} 
	
	public function setUsuarioOrigem(){
		$model = Usuario::model()->findByPk(Yii::app()->user->id);
		//$this->d_nome  = $model->nome;
		//$this->d_local = $model->local;
		//$this->d_andar = $model->andar;
		//$this->d_baia  = $model->baia;
		//$this->d_ramal = $model->ramal; 
		$this->d_area  = $model->area;
		$this->d_email = $model->email;
		$this->d_ccusto = $model->centro_custo;
		//$this->d_numero = $model->numero;
		//$this->d_andar = $model->andar;
		//$this->d_bairro = $model->bairro;
		//$this->d_cidade = $model->cidade;
		$this->d_uf = $model->uf;
		//$this->d_cep = $model->cep;
		//$this->d_ramal = $model->ramal;
		$this->d_area = $model->area;
		$this->local = $model->nome_local;
		
	} 

	/**
    * @return array relational rules.
    */
    public function relations() {
        return array(
            'CentroCusto2' => array(self::BELONGS_TO, 'CentroCusto2', 'centrocusto2'),
        );
    }
	
	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules() {
		return array(
			array('d_nome, d_email, d_uf, d_ccusto, servico, d_cep, d_local', 'required'),
			array('d_nome, d_local, d_numero, d_andar, d_baia, d_bairro, d_cidade, d_uf, d_cep, d_ramal, d_area, d_email, d_ccusto, texto, valor, servico, aprovacao, local, recebedor, os, opcionalcentrocusto, centro_custo2, atendimento, empresa, dispositivos, km, eventos', 'safe'),
			array('d_numero, d_andar, d_baia, d_ramal, d_area, d_ccusto, valor, servico, aprovacao, recebedor, opcionalcentrocusto, km, eventos, empresa', 'length', 'max' => 45),
			array('d_nome, d_local, d_bairro, d_cidade, d_email, local, recebedor, dispositivos, km, eventos, empresa', 'length', 'max' => 60),
			array('d_uf', 'length', 'max' => 2),
			array('d_cep', 'length', 'max' => 9),
			);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
			'd_email'         => 'Email',
			'd_nome'          => 'Nome',
			'd_local'         => 'Logradouro',
			'd_numero'        => 'Número',
			'd_bairro'        => 'Bairro',
			'd_cidade'        => 'Cidade',
			'd_uf'            => 'Estado',
			'd_cep'           => 'CEP',
			'd_andar'         => 'Andar',
			'd_baia'          => 'Complemento',
			'd_ramal'         => 'Ramal (DD 9999-99999)',
			'd_area'          => 'Area',

			'texto'           => 'Descrição do serviço',
			'd_ccusto'        => 'C. Custo',
			'servico'         => 'Tipo de Serviço',
			'local'		      => 'Localidade usuário',
			'aprovacao'		  => 'Aprovação',
			'recebedor'		  => 'Nome Recebedor',
			'valor'           => 'Valor (Sujeito a alteração pela transportadora)',
			'os'              => 'os',
			'opcionalcentrocusto' => 'C.Custo Cobrança',
			'centro_custo2' =>'Centro Custo Cobrança',
			'atendimento' =>'Atendimento',
			'dispositivos' =>'dispositivos',
			'empresa' =>'Empresa',
			'km' =>'km',
			'eventos' =>'eventos'
		);
	}
}
