<?php

/**
 * This is the model class for table "aes_prot_malote".
 *
 * The followings are the available columns in table 'aes_prot_malote':
 * @property string $id
 * @property string $track_carta
 * @property string $malote
 * @property string $de_usuario_id
 * @property string $de_local_id
 * @property string $para_usuario_id
 * @property string $para_local_id
 * @property string $d_nome
 * @property string $d_local
 * @property string $d_andar
 * @property string $d_baia
 * @property string $d_ramal
 * @property string $d_area
 * @property string $d_email
 * @property string $p_nome
 * @property string $p_local
 * @property string $p_andar
 * @property string $p_baia
 * @property string $p_ramal
 * @property string $p_area
 * @property string $p_email
 * @property string $ts_impressao
 * @property string $ts_sup_inicio
 * @property string $ts_motoboy_1
 * @property string $ts_transp
 * @property string $ts_motoboy_2
 * @property string $ts_sup_fim
 * @property string $status
 * @property integer $tipo
 */
class Movimento extends ActiveRecord {
	const TIPO_CARTA = 1;
	const TIPO_MALOTE = 2;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{prot_malote}}';
	}
	
	public function init(){
		$this->track_carta = substr(md5(rand()), 1, 15);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
//			array('track_carta, malote', 'required'),
//			array('track_carta, malote', 'unique'),
			array('tipo', 'numerical', 'integerOnly' => true),
			array('track_carta, track_malote, d_ramal, p_ramal, track_msg', 'length', 'max' => 22),
			array('malote', 'length', 'max' => 20),
			array('d_nome, d_local, d_andar, d_baia, p_nome, p_local, p_andar, p_baia, status, d_email, p_email, u_empresa', 'length', 'max' => 45),
			array('d_area, p_area', 'length', 'max' => 150),
			array('ts_impressao, ts_sup_inicio, ts_malote, ts_motoboy_1, ts_transp, ts_motoboy_2, ts_sup_fim', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, u_empresa, track_carta, track_malote, malote, d_nome, d_local, d_andar, d_baia, d_ramal, d_area, p_nome, p_local, p_andar, p_baia, p_ramal, p_area, ts_impressao, ts_malote, ts_sup_inicio, ts_motoboy_1, ts_transp, ts_motoboy_2, ts_sup_fim, status, tipo, d_email, p_email, track_msg', 'safe', 'on' => 'search'),
		);
	}

/*	
	public function getCartaLink(){
		return CHtml::link($this->track_carta, Yii::app()->params["cartaArquivos"].$this->track_carta.".pdf");
	}
*/
	public function getMaloteLink(){
		return CHtml::link($this->track_malote, Yii::app()->params["maloteArquivos"].$this->track_malote.".pdf");
	}
/*	
		public function getMsgLink(){
		return CHtml::link($this->track_msg, Yii::app()->params["msgArquivos"].$this->track_msg.".pdf");
	}
*/
	public function attributeLabels() {
		return array (
			'id'              => 'ID',
			'track_carta'     => 'Carta',
			'track_malote'    => 'Malote',
			'u_empresa'       => 'Empresa',
			'malote'          => 'Lacre',
			'd_email'         => 'Email',
			'd_nome'          => 'Nome',
			'd_local'         => 'Local',
			'd_andar'         => 'Andar',
			'd_baia'          => 'Baia',
			'd_ramal'         => 'Ramal',
			'd_area'          => 'Area',
			'p_email'         => 'Email',
			'p_nome'          => 'Nome',
			'p_local'         => 'Local',
			'p_andar'         => 'Andar',
			'p_baia'          => 'Baia',
			'p_ramal'         => 'Ramal',
			'p_area'          => 'Area',
			'ts_impressao'    => 'Impressao',
			'ts_malote'       => 'Ts Malote',
			'ts_sup_inicio'   => 'Sup Inicio',
			'ts_motoboy_1'    => 'Motoboy 1',
			'ts_transp'       => 'Transp',
			'ts_motoboy_2'    => 'Motoboy 2',
			'ts_sup_fim'      => 'Sup Fim',
			'status'          => 'Status',
			'tipo'            => 'Tipo',
			'track_msg'       => 'Etiqueta',
		);
	}
	public function getLimit()
	{
		return isset($this->_query['limit']) ? $this->_query['limit'] : 1000;
	}

	
	public function where($conditions, $params=array())
	{
		$this->_query['where']=$this->processConditions($conditions);
		foreach($params as $name=>$value)
			$this->params[$name]=$value;
		return $this;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
        
		if ((!Yii::app()->user->isAdmin) && (!Yii::app()->user->isGeren) && (!Yii::app()->user->isTransp)){
			$criteria->compare('d_email', Yii::app()->user->email, false, 'AND');
			$criteria->compare('u_empresa', Yii::app()->user->empresa, false);
		}
		
		else if ((Yii::app()->user->isGeren) || Yii::app()->user->isTransp) {
			$criteria->compare('u_empresa', Yii::app()->user->empresa, false);
		}

		$criteria->compare('id', $this->id, true);
		$criteria->compare('track_carta', $this->track_carta, true);
		$criteria->compare('track_malote', $this->track_malote, true);
		$criteria->compare('u_empresa', $this->u_empresa, true);
		$criteria->compare('malote', $this->malote, true);
		$criteria->compare('track_msg', $this->track_msg);
		
		$criteria->compare('d_nome', $this->d_nome);
		$criteria->compare('d_email', $this->d_nome, true, 'OR');
		
		$criteria->compare('p_nome', $this->p_nome);
		$criteria->compare('p_email', $this->p_nome, true, 'OR');
		
		$criteria->compare('ts_impressao',  $this->convertToFindDate($this->ts_impressao), true);
		$criteria->compare('ts_malote',     $this->convertToFindDate($this->ts_malote), true);
		$criteria->compare('ts_sup_inicio', $this->convertToFindDate($this->ts_sup_inicio), true);
		$criteria->compare('ts_motoboy_1',  $this->convertToFindDate($this->ts_motoboy_1), true);
		$criteria->compare('ts_transp',     $this->convertToFindDate($this->ts_transp), true);
		$criteria->compare('ts_motoboy_2',  $this->convertToFindDate($this->ts_motoboy_2), true);
		$criteria->compare('ts_sup_fim',    $this->convertToFindDate($this->ts_sup_fim), true);
		$criteria->compare('status', $this->status, true);
		 //$criteria->addCondition("ts_impressao >= '2016-04-01'");
	    
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
				'sort' => array(
				'defaultOrder' => array('ts_impressao' => true),
			) 			
	   	 )
		);
	
        #$criteria->condition="track_carta >= 'C20151001073630'";
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Movimento the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

}
