<?php

/**
 * This is the model class for table "{{track}}".
 *
 * The followings are the available columns in table '{{procter_track}}':
 * @property string $chave
 * @property string $tracking
 * @property string $remetente
 * @property string $destinatario
 * @property string $depto
 * @property string $baia
 * @property string $andar
 * @property string $obs
 * @property string $correio
 * @property string $transp
 * @property string $ts_impressao
 * @property string $ts_tt1
 * @property string $ts_tt2
 * @property string $ts_tt3
 * @property string $ts_assinatura
 * @property string $tipo
  */

class RelatorioProtocolo extends ActiveRecord {

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{track}}';
	}
    
	public function getCartaLink(){
		return CHtml::link($this->tracking, Yii::app()->params["ProtocoloArquivos"].$this->tracking.".pdf");
	}
	
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
//		array('d_ramal, d_area, p_ramal, p_area', 'required'),
		//array('Valor', 'numerical'),
			//array('track_carta', 'length', 'max' => 15),
			//array('track_carta, ts_impressao, ts_entrega, descricao, obs, d_nome, d_local, d_numero, d_andar, d_baia, d_bairro, d_cidade, d_uf, d_cep, d_ramal, d_area, d_email, c_custo, p_nome, p_local, p_numero, p_andar, p_baia, p_bairro, p_cidade, p_uf, p_cep, p_ramal, p_area, texto, valor, servico, aprovacao, local, area_portador, recebedor, os', 'safe', 'on' => 'search'),
		//	array('d_numero, d_andar, d_baia, d_ramal, d_area, p_andar, p_baia, p_ramal, p_area, p_email', 'length', 'max' => 45),
            array('remetente, destinatario, depto', 'length', 'max' => 60),
            array('tipo, obs ', 'length', 'max' => 45),
			//array('d_uf, p_uf', 'length', 'max' => 2),
			//array('d_cep, p_cep', 'length', 'max' => 9),
			array('ts_impressao, ts_assinatura, ts_tt1, ts_tt2, ts_tt3', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'chave'              => 'Chave',
			'tracking'           => 'Tracking',
			'remetente'          => 'Nome',
			'destinatario'       => 'Nome Destino',
			'depto'              => 'Número',
            'baia'               => 'Baia',
            'andar'              => 'Andar',
			'obs'                => 'Observação',
			'correio'            => 'Correio',
			'transp'             => 'Transporte',
			'tipo'               => 'Tipo',
			
			
		);
	}
	
	//	public function getCartaLink(){
	//	return CHtml::link($this->track_expo, Yii::app()->params["esporadicoArquivos"].$this->track_expo.".pdf");		
	//}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		
	
		$criteria->compare('chave', $this->chave, true);
		$criteria->compare('tracking', $this->tracking, true);
		
		$criteria->compare('remetente', $this->remetente, true);
		$criteria->compare('destinatario', $this->destinatario, true);
		$criteria->compare('depto', $this->depto, true);
		$criteria->compare('baia', $this->baia, true);
		$criteria->compare('andar', $this->andar, true);
		$criteria->compare('obs', $this->obs, true);
		$criteria->compare('correio', $this->correio, true);
		$criteria->compare('transp', $this->transp, true);
		
		
		$criteria->compare('tipo', $this->tipo, true);
		
		$criteria->compare('ts_impressao', $this->convertToFindDate($this->ts_impressao), true);
		$criteria->compare('ts_assinatura', $this->convertToFindDate($this->ts_assinatura), true);
		$criteria->compare('ts_tt1', $this->convertToFindDate($this->ts_tt1), true);
		$criteria->compare('ts_tt2', $this->convertToFindDate($this->ts_tt2), true);
		$criteria->compare('ts_tt3', $this->convertToFindDate($this->ts_tt3), true);	
		
        Yii::app()->session['RelatorioProtocolo.criteria'] = $criteria;
		return new CActiveDataProvider($this, array('criteria'=>$criteria));
				
	
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Esporadico the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

}