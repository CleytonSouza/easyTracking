<?php

/**
 * This is the model class for table "aes_prot_malote".
 *
 * The followings are the available columns in table 'aes_prot_malote':
 * @property string $id
 * @property string $track_carta
 * @property string $malote
 * @property string $de_usuario_id
 * @property string $de_local_id
 * @property string $para_usuario_id
 * @property string $para_local_id
 * @property string $d_nome
 * @property string $d_local
 * @property string $d_andar
 * @property string $d_baia
 * @property string $d_ramal
 * @property string $d_area
 * @property string $d_email
 * @property string $p_nome
 * @property string $p_local
 * @property string $p_andar
 * @property string $p_baia
 * @property string $p_ramal
 * @property string $p_area
 * @property string $p_email
 * @property string $ts_impressao
 * @property string $ts_sup_inicio
 * @property string $ts_motoboy_1
 * @property string $ts_transp
 * @property string $ts_motoboy_2
 * @property string $ts_sup_fim
 * @property string $status
 * @property integer $tipo
 */
class PesquisaUnitario extends ActiveRecord {
	const TIPO_CARTA = 1;
	const TIPO_MALOTE = 2;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{track}}';
	}
	
	#public function init(){
	#	$this->track_lote = substr(md5(rand()), 1, 15);
	#}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
//			array('track_carta, malote', 'required'),
//			array('track_carta, malote', 'unique'),
			array('tipo', 'numerical', 'integerOnly' => true),
			array('track_lote, track_cartas, status, quantidade', 'length', 'max' => 15),
			//array('malote', 'length', 'max' => 20),
			//array('d_nome, d_local, d_andar, d_baia, p_nome, p_local, p_andar, p_baia, status, d_email, p_email', 'length', 'max' => 45),
			array('servico, obs', 'length', 'max' => 200),
			array('ts_impressao, ts_tt1, ts_tt2, ts_tt3, ts_assinatura', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, local_origem, local_destino, track_lote, quantidade, transporte, obs, ts_impressao, ts_assinatura', 'safe', 'on' => 'search'),
		);
	}
	
	/*public function getCartaLink(){
		return CHtml::link($this->track_carta, Yii::app()->params["cartaArquivos"].$this->track_carta.".pdf");
	}*/
	
	

   # public function getLoteLink(){
	#	return CHtml::link($this->track_lote, Yii::app()->params["loteArquivos"].$this->track_lote.".pdf");
	#}
	
	/*	public function getMsgLink(){
		return CHtml::link($this->track_msg, Yii::app()->params["msgArquivos"].$this->track_msg.".pdf");
	}*/

	public function attributeLabels() {
		return array (
			'id'               => 'ID',
			'track_lote'       => 'Lote',
			'quantidade'       => 'Quantidade',
			'transporte'       => 'Transporte',
			'obs'              => 'Observação',
            'status'           => 'Status',
			//'track_cartas'     => 'Cartas',
			'ts_impressao'     => 'Impressao',
			'ts_assinatura'    => 'Assinatura',
			'local_origem'     => 'Local de Origem',
			'local_destino'    => 'Local de destino',
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		if ((!Yii::app()->user->isAdmin)&& (!Yii::app()->user->isTransp) )	{
			//$criteria->compare('d_email', Yii::app()->user->email, false, 'OR');
			//$criteria->compare('p_email', Yii::app()->user->email, false, 'OR');
		}

		$criteria->compare('id', $this->id, true);
		$criteria->compare('track_lote', $this->track_lote, true);
		//$criteria->compare('track_cartas', $this->track_cartas, true);
		$criteria->compare('obs', $this->obs, true);
		$criteria->compare('local_origem', $this->local_origem, true);
		$criteria->compare('local_destino', $this->local_destino, true);
		
		$criteria->compare('quantidade', $this->quantidade, true);
		$criteria->compare('transporte', $this->transporte, true);
		
		$criteria->compare('ts_impressao',  $this->convertToFindDate($this->ts_impressao), true);
		$criteria->compare('ts_assinatura',  $this->convertToFindDate($this->ts_assinatura), true);
		
		
		$criteria->compare('status', $this->status, true);
	
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => array('ts_impressao' => false),
			)			
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Movimento the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

}
