<?php

/**
 * This is the model class for table "aes_usuario".
 *
 * The followings are the available columns in table 'aes_usuario':
 * @property string $id
 * @property string $email
 * @property string $senha
 * @property string $nome
 * @property string $local
 * @property string $andar
 * @property string $baia
 * @property string $ramal
 * @property integer $perfil
 * @property string $area
 * @property string $centro_custo
 * @property string $objeto_custo
 * @property string $divisao
 */
class Usuario extends CActiveRecord {

    private $_ultimaSenhaPesquisada = null;
    public $confirmaSenha = '';

    public function onAfterFind($event) {
        parent::onAfterFind($event);
        $this->_ultimaSenhaPesquisada = $this->senha;
    }

    public function onAfterSave($event) {
		parent::onAfterSave($event);
        var_dump($this->_ultimaSenhaPesquisada);
        var_dump($this->senha);
        die();
        if ($this->_ultimaSenhaPesquisada != $this->senha) {
            $hs = new HistoricoSenha();
            $hs->usuario = Yii::app()->user->id;
            $hs->hash = $this->_ultimaSenhaPesquisada;
            $hs->date = date('Y-m-d');
            $hs->save();
        }
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{usuario}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
	
    public function rules() {
        return array(
            array('email', 'unique'),
           // array('email', 'match', 'pattern' => '/@telefonica.com$/', 'message' => 'Este e-mail não tem permissão para ser cadastrado.', 'on' => 'register'),
            #array('email', 'match', 'pattern' => '/.ext@telefonica.com$/', 'not' => true, 'message' => 'Este e-mail é um e-mail externo e não pode ser cadastrado.', 'on' => 'register'),
            array('email, senha, nome, local, numero, cidade, cep, uf, ramal, centro_custo, empresa', 'required'),
            array('senha', 'validarSenha', 'on' => array('mod_senha', 'register')),
            //array('centro_custo', 'validarCentroCusto'),
           // array('objeto_custo', 'validarObjetoCusto'),
            array('senha', 'validarHistoricoSenha', 'on' => 'mod_senha'),
            array('confirmaSenha', 'required', 'on' => 'register'),
            array('confirmaSenha', 'compare', 'compareAttribute' => 'senha', 'on' => 'register', 'message' => 'As senhas não conferem.'),
            array('confirmaSenha', 'compare', 'compareAttribute' => 'senha', 'on' => 'mod_senha', 'message' => 'As senhas não conferem.'),
            array('confirmaSenha', 'required', 'on' => 'mod_senha'),
            array('email, nome,local, bairro, cidade, nome_local, empresa', 'length', 'max' => 60),
            array('senha', 'length', 'max' => 32),
            array('numero, andar, matricula, baia, ramal, perfil, area, centro_custo, objeto_custo', 'length', 'max' => 45),
            array('cep', 'length', 'max' => 9),
            array('uf', 'length', 'max' => 2),
            array('id, email, senha, empresa, nome, matricula, nome_local, local, numero, andar, baia, bairro, cidade, cep, uf, ramal, perfil, area, centro_custo, first, ativo, confirmado, objeto_custo', 'safe', 'on' => 'search'),
        );
    }
	
    public function validarCentroCusto($attribute, $params) {
        if (empty($this->$attribute)) return false;
        $val = $this->$attribute;
		
		if (!CentroCusto::model()->exists('centro_custo = :centro_custo', array(':centro_custo'=>$val)))
	        $this->addError($attribute, 'O centro de custo é inválido');
    }
	
    public function validarObjetoCusto($attribute, $params) {
        if (empty($this->$attribute)) return false;
        $val = $this->$attribute;
		
		if (!CentroCusto::model()->exists('centro_custo = :centro_custo and objeto_custo = :objeto_custo', array(':centro_custo'=>$this->centro_custo, ':objeto_custo'=>$val)))
	        $this->addError($attribute, 'O objeto de custo é inválido ou não corresponde ao centro de custo');
    }
	
	    public function validarEmpresa($attribute, $params) {
        if (empty($this->$attribute)) return false;
        $val = $this->$attribute;
		
		if (!Empresa::model()->exists('empresa = :empresa', array(':empresa'=>$val)))
	        $this->addError($attribute, 'Essa empresa não está cadastrada');
    }

    public function validarSenha($attribute, $params) {
        if (empty($this->$attribute))
            return false;
        $val = $this->$attribute;
        $a = preg_match('/[[:alpha:]]/', $val);
        $d = preg_match('/[[:digit:]]/', $val);
        $p = preg_match('/[[:punct:]]/', $val);

        if ($a && $d && $p) {
            if (strlen($val) < 6 || strlen($val) > 12)
                $this->addError($attribute, 'A senha deve conter entre 6 e 12 caracteres.');
            return;
        }
        $this->addError($attribute, 'A senha deve conter letras, números e caracteres de pontuação.');
    }

    public function validarHistoricoSenha($attribute, $params) {
        if (empty($this->$attribute)) return false;
        $val = $this->$attribute;
        $criteria = new CDbCriteria();
        $criteria->condition = 'usuario=:usuario and hash=:hash';
        $criteria->params = array(
            ':usuario' => Yii::app()->user->id,
            ':hash' => $val,
        );
        $hs = HistoricoSenha::model()->find($criteria);
        if (!empty($hs)) {
            $this->addError($attribute, 'Esta senha já foi usada anteriormente. É necessário escolher outra senha.');
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'Perfil' => array(self::BELONGS_TO, 'Perfil', 'perfil'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'email' => 'Email',
            'senha' => 'Senha',
            'nome' => 'Nome',
            'local' => 'Logradouro',
            'andar' => 'Andar',
            'baia' => 'Complemento',
            'ramal' => 'Ramal (DD 9999-99999)',
            'perfil' => 'Perfil',
            'area' => 'Área',
            'centro_custo' => 'Centro de custo',
            'confirmaSenha' => 'Confirmação de senha',
            'numero' => 'Número',
            'bairro' => 'Bairro',
            'cidade' => 'Cidade',
            'cep' => 'CEP',
            'uf' => 'UF',
            'ativo' => 'Ativo',
            'matricula' => 'Matricula',
            'nome_local' => 'Local de Trabalho',
			'empresa' => 'Empresa',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('senha', $this->senha, true);
        $criteria->compare('nome', $this->nome, true);
        $criteria->compare('local', $this->local, true);
        $criteria->compare('andar', $this->andar, true);
        $criteria->compare('baia', $this->baia, true);
        $criteria->compare('ramal', $this->ramal, true);
        $criteria->compare('perfil', $this->perfil, true);
        $criteria->compare('area', $this->area, true);
        $criteria->compare('centro_custo', $this->centro_custo, true);
        $criteria->compare('numero', $this->numero, true);
        $criteria->compare('cidade', $this->cidade, true);
        $criteria->compare('cep', $this->cep, true);
        $criteria->compare('uf', $this->uf, true);
        $criteria->compare('ativo', $this->ativo, true);
        $criteria->compare('matricula', $this->matricula, true);
        $criteria->compare('nome_local', $this->nome_local, true);
		$criteria->compare('empresa', $this->empresa, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Usuario the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
