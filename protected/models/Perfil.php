<?php

/**
 * This is the model class for table "{{perfil}}".
 *
 * The followings are the available columns in table '{{perfil}}':
 * @property integer $id
 * @property string $nome
 * @property integer $movimento
 * @property integer $criar_carta
 * @property integer $criar_malote
 * @property integer $pools
 * @property integer $criar_pool
 * @property integer $receber_pool
 * @property integer $rota
 * @property integer $criar_rota
 * @property integer $delete_pools
 * @property integer $delete_movimento
 * @property integer $delete_rota
 * @property integer $admin
 * @property integer $esporadico
 * @property integer $criar_esporadico
 * @property integer $modificar_esporadico
 * @property integer $auto_esporadico
 * @property integer $delete_esporadico
 * @property integer $transportadora
 * @property integer $sedex10
 * @property integer $protocolo
 * @property integer $protocolo_criar
 * @property integer rel_cr
 * @property integer rel_ce
 * @property integer rel_motoboy
 */
class Perfil extends ActiveRecord {

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{perfil}}';
	}
	
	public function getMovimentoTextPerm(){
		$res = array();
		if ($this->movimento) $res[] = $this->getAttributeLabel('movimento');
		if ($this->criar_carta) $res[] = $this->getAttributeLabel('criar_carta');
		if ($this->criar_malote) $res[] = $this->getAttributeLabel('criar_malote');
		if ($this->delete_movimento) $res[] = $this->getAttributeLabel('delete_movimento');
		if ($this->recebe_malote) $res[] = $this->getAttributeLabel('recebe_malote');
		if (count($res) == 5) return 'Todas';
		return implode(' / ', $res);
	}
	
	public function getPoolTextPerm(){
		$res = array();
		if ($this->pools) $res[] = $this->getAttributeLabel('pools');
		if ($this->criar_pool) $res[] = $this->getAttributeLabel('criar_pool');
		if ($this->receber_pool) $res[] = $this->getAttributeLabel('receber_pool');
		if ($this->delete_pools) $res[] = $this->getAttributeLabel('delete_pools');
		if (count($res) == 4) return 'Todas';
		return implode(' / ', $res);
	}
	
	public function getRotaTextPerm(){
		$res = array();
		if ($this->rota) $res[] = $this->getAttributeLabel('rota');
		if ($this->criar_rota) $res[] = $this->getAttributeLabel('criar_rota');
	//	if ($this->delete_rota) $res[] = $this->getAttributeLabel('delete_rota');
		if (count($res) == 3) return 'Todas';
		return implode(' / ', $res);
	}
	
	public function getEsporadicoTextPerm(){
		$res = array();
		if ($this->esporadico) $res[] = $this->getAttributeLabel('esporadico');
		if ($this->criar_esporadico) $res[] = $this->getAttributeLabel('criar_esporadico');
		if ($this->modificar_esporadico) $res[] = $this->getAttributeLabel('modificar_esporadico');
		if ($this->delete_esporadico) $res[] = $this->getAttributeLabel('delete_esporadico');
		if ($this->auto_esporadico) $res[] = $this->getAttributeLabel('auto_esporadico');
		if ($this->aprova_esporadico) $res[] = $this->getAttributeLabel('aprova_esporadico');
		if (count($res) == 6) return 'Todas';
		return implode(' / ', $res);
	}
	
	public function getPostagemTextPerm(){
		$res = array();
		if ($this->postagem) $res[] = $this->getAttributeLabel('postagem');
		if ($this->criar_postagem) $res[] = $this->getAttributeLabel('criar_postagem');
		if ($this->modificar_postagem) $res[] = $this->getAttributeLabel('modificar_postagem');
		if ($this->delete_postagem) $res[] = $this->getAttributeLabel('delete_postagem');
		if ($this->auto_postagem) $res[] = $this->getAttributeLabel('auto_postagem');
		if ($this->aprova_postagem) $res[] = $this->getAttributeLabel('aprova_postagem');
		if ($this->recebe_postagem) $res[] = $this->getAttributeLabel('recebe_postagem');
		if ($this->juridico_postagem) $res[] = $this->getAttributeLabel('juridico_postagem');
		if (count($res) == 8) return 'Todas';
		return implode(' / ', $res);
	}
	
	public function getProtocoloTextPerm(){
		$res = array();
		if ($this->protocolo) $res[] = $this->getAttributeLabel('protocolo');
		if ($this->protocolo_criar) $res[] = $this->getAttributeLabel('protocolo_criar');
		if (count($res) == 2) return 'Todas';
		return implode(' / ', $res);
	}
	
	public function getProtocoloRelatorios(){
		$res = array();
		if ($this->rel_cr) $res[] = $this->getAttributeLabel('rel_cr');
		if ($this->rel_ce) $res[] = $this->getAttributeLabel('rel_ce');
		if ($this->rel_motoboy) $res[] = $this->getAttributeLabel('rel_motoboy');
		if (count($res) == 3) return 'Todas';
		return implode(' / ', $res);
	}
	
	public function getBadgeNome(){
		$res = $this->nome;
		if ($this->admin)
			$res = Yii::app()->controller->widget('bootstrap.widgets.TbBadge', array(
				'type'=>'important', // 'success', 'warning', 'important', 'info' or 'inverse'
				'label'=>'ADM',
			), true).' '.$res; 
		
		if ($this->transp)
			$res = Yii::app()->controller->widget('bootstrap.widgets.TbBadge', array(
				'type'=>'inverse', // 'success', 'warning', 'important', 'info' or 'inverse'
				'label'=>'TRANSP',
			), true).' '.$res; 
			
	    if ($this->geren)
			$res = Yii::app()->controller->widget('bootstrap.widgets.TbBadge', array(
				'type'=>'inverse', // 'success', 'warning', 'important', 'info' or 'inverse'
				'label'=>'GEREN',
			), true).' '.$res; 
		
		return $res;
	}
			
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('transp, geren, nome, admin', 'required'),
			array('movimento, geren, transp, criar_carta, criar_malote, pools, criar_pool, receber_pool, rota, criar_rota, admin, delete_pools, delete_movimento, esporadico, criar_esporadico, modificar_esporadico, auto_esporadico, delete_esporadico, recebe_malote, aprova_esporadico, postagem, criar_postagem, modificar_postagem, auto_postagem, delete_postagem, aprova_postagem, recebe_postagem, juridico_postagem, sedex10, protocolo, protocolo_criar, rel_cr, rel_ce, rel_motoboy', 'numerical', 'integerOnly' => true),
			array('nome', 'length', 'max' => 45),
			array('id, trasnp, nome, movimento, geren, criar_carta, criar_malote, pools, criar_pool, receber_pool, rota, criar_rota, admin, delete_pools, delete_movimento, esporadico, criar_esporadico, modificar_esporadico, auto_esporadico, delete_esporadico, recebe_malote, aprova_esporadico, postagem, criar_postagem, modificar_postagem, auto_postagem, delete_postagem, aprova_postagem, recebe_postagem, juridico_postagem', 'safe', 'on' => 'search'),
		);
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'nome' => 'Nome',
			'admin' => 'Administrar',
			'geren' => 'Gerenciar',
			'transp' => 'Transportadora',
			
			/* Movimento */
			'movimento' => 'Pesquisar',
			'criar_carta' => 'Criar Carta',
			'criar_malote' => 'Criar Malote',
			'delete_movimento' => 'Remover Carta',
			'recebe_malote' => 'Recebe Malote',
			
			/* Pools */
			'pools' => 'Pesquisar',
			'criar_pool' => 'Criar',
			'receber_pool' => 'Receber',
			'delete_pools' => 'Remover',
			
			/* Rota */
			'rota' => 'Pesquisar',
			'criar_rota' => 'Criar Rota',			
		//'delete_rota' => 'Remover',
			
			/* Motoboy */
			'esporadico'=>'Pesquisar',
			'criar_esporadico'=>'Criar',
			'modificar_esporadico'=>'Modificar',
			'delete_esporadico'=>'Remover',
			'auto_esporadico'=>'Auto-atualizar',
			'aprova_esporadico'=>'Aprovação',
			
						
			/* Postagem */
			'postagem'=>'Pesquisar',
			'criar_postagem'=>'Criar',
			'modificar_postagem'=>'Modificar',
			'delete_postagem'=>'Remover',
			'auto_postagem'=>'Auto-atualizar',
			'aprova_postagem'=>'Aprovação',
			'recebe_postagem'=>'Lote Layout 1',
			'juridico_postagem'=>'Lote Layout 2',
			'sedex10'=>'Sedex 10',
			
			/* Relatórios */
			'rel_cr'=>'Correspondências recebidas', 
			'rel_ce'=>'Correspondencias enviadas', 
			'rel_motoboy'=>'Motoboy',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('nome', $this->nome, true);
		$criteria->compare('admin', $this->admin);
		$criteria->compare('transp', $this->transp);
		$criteria->compare('geren', $this->geren);
		
		$criteria->compare('movimento', $this->movimento);
		$criteria->compare('criar_carta', $this->criar_carta);
		$criteria->compare('criar_malote', $this->criar_malote);
		$criteria->compare('recebe_malote', $this->recebe_malote);
		$criteria->compare('delete_movimento', $this->delete_movimento);
		
		$criteria->compare('pools', $this->pools);
		$criteria->compare('criar_pool', $this->criar_pool);
		$criteria->compare('receber_pool', $this->receber_pool);
		$criteria->compare('delete_pools', $this->delete_pools);
		
		$criteria->compare('rota', $this->rota);
		$criteria->compare('criar_rota', $this->criar_rota);
	//	$criteria->compare('delete_rota', $this->delete_rota);

		$criteria->compare('esporadico', $this->esporadico);
		$criteria->compare('criar_esporadico', $this->criar_esporadico);
		$criteria->compare('modificar_esporadico', $this->modificar_esporadico);
		$criteria->compare('auto_esporadico', $this->auto_esporadico);
		$criteria->compare('aprova_esporadico', $this->aprova_esporadico);

		$criteria->compare('postagem', $this->postagem);
		$criteria->compare('criar_postagem', $this->criar_postagem);
		$criteria->compare('modificar_postagem', $this->modificar_postagem);
		$criteria->compare('auto_postagem', $this->auto_postagem);
		$criteria->compare('aprova_postagem', $this->aprova_postagem);
		$criteria->compare('recebe_postagem', $this->recebe_postagem);
		$criteria->compare('juridico_postagem', $this->juridico_postagem);
		
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Perfil the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	
	/*public function beforeSave() {
		if ($this->transportadora) {
			$this->esporadico = true;
			$this->criar_esporadico = true;
			$this->esporadico = true;
			$this->postagem_aprovacao = true;
			$this->auto_postagem = true;
			$this->modificar_postagem = true;
			$this->postagem = true;
			$this->postagem_aprovacao = true;
			$this->postagem_aprovacao = true; = true;			
		}
		return parent::beforeSave();
	}
*/
}
