<?php

/**
 * This is the model class for table "{{track}}".
 *
 * The followings are the available columns in table '{{track}}':
 * @property integer $chave
 * @property string $tracking
 * @property string $remetente
 * @property string $destinatario
 * @property string $depto
 * @property string $baia
 * @property string $andar
 * @property string $obs
 * @property string $correio
 * @property string $transp
 * @property string $ts_impressao
 * @property string $ts_tt1
 * @property string $ts_tt2
 * @property string $ts_tt3
 * @property string $ts_assinatura
 * @property string $tipo
 */
class Track extends ActiveRecord {
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{track}}';
	}
	

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tracking, remetente, destinatario, tipo', 'required'),
			array('tracking', 'length', 'max'=>15),
			array('remetente, destinatario, obs', 'length', 'max'=>60),
			array('depto', 'length', 'max'=>40),
			array('baia, andar', 'length', 'max'=>20),
			array('correio', 'length', 'max'=>30),
			array('transp', 'length', 'max'=>5),
			array('tipo', 'length', 'max'=>45),
			array('ts_impressao, ts_tt1, ts_tt2, ts_tt3, ts_assinatura', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('chave, tracking, remetente, destinatario, depto, baia, andar, obs, correio, transp, ts_impressao, ts_tt1, ts_tt2, ts_tt3, ts_assinatura, tipo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'chave' => 'Chave',
			'tracking' => 'Tracking',
			'remetente' => 'De',
			'destinatario' => 'Para',
			'depto' => 'Área',
			'baia' => 'Baia / Complemento',
			'andar' => 'Andar',
			'obs' => 'Obs',
			'correio' => 'Correio',
			'transp' => 'Transp',
			'ts_impressao' => 'Ts Impressão',
			'ts_tt1' => 'Tentativa 1',
			'ts_tt2' => 'Tentativa 2',
			'ts_tt3' => 'Tentativa 3',
			'ts_assinatura' => 'Ts Assinatura',
			'tipo' => 'Tipo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		
		$criteria=new CDbCriteria;
        $criteria->condition="tracking >= '20160104082011'";
        #$criteria->condition="tracking >= '20151201075241'";
		
		
		$criteria->compare('chave',          $this->chave);
		$criteria->compare('tracking',       $this->tracking,true);
		$criteria->compare('remetente',      $this->remetente,true);
		$criteria->compare('destinatario',   $this->destinatario,true);
		$criteria->compare('depto',          $this->depto,true);
		$criteria->compare('baia',           $this->baia,true);
		$criteria->compare('andar',          $this->andar,true);
		$criteria->compare('obs',            $this->obs,true);
		$criteria->compare('correio',        $this->correio,true);
		$criteria->compare('transp',         $this->transp,true);
		$criteria->compare('ts_impressao',   $this->convertToFindDate($this->ts_impressao), true);
		$criteria->compare('ts_assinatura',  $this->convertToFindDate($this->ts_assinatura), true);
		$criteria->compare('ts_tt1',         $this->ts_tt1,true);
		$criteria->compare('ts_tt2',         $this->ts_tt2,true);
		$criteria->compare('ts_tt3',         $this->ts_tt3,true);
		$criteria->compare('tipo',           $this->tipo,true);
		Yii::app()->session['protocolo.criteria'] = $criteria;
			return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
				'sort' => array(
				'defaultOrder' => array('ts_impressao' => true),
			) 			
	   	 )
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Track the static model class
	 */
	public static function model($className=__CLASS__){
		return parent::model($className);
	}
}
