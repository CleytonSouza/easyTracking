<?php

/**
 * This is the model class for table "{{usuario}}".
 *
 * The followings are the available columns in table '{{usuario}}':
 * @property string $id
 * @property string $email
 * @property string $senha
 * @property string $nome
 * @property string $local
 * @property string $numero
 * @property string $andar
 * @property string $baia
 * @property string $bairro
 * @property string $cidade
 * @property string $cep
 * @property string $uf
 * @property string $area
 * @property string $ramal
 * @property string $centro_custo
 * @property string $first
 * @property string $perfil
 * @property string $ativo
 * @property string $confirmado
 * @property string $hash
 * @property string $matricula 
 * @property string $nome_local
 * @property string $conf
 * @property string $objeto_custo
 * @property string $divisao
 */

class ModificarMeusDados extends ActiveRecord {

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{usuario}}';
	}
 	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
        //array('d_ramal, d_area, p_ramal, p_area', 'required'),
		//array('Valor','numerical'),
			array('email', 'length', 'max' => 15),
			array('email, senha, nome, local, numero, andar, baia, bairro, cidade, cep, uf, area, ramal, centro_custo, matricula, nome_local, objeto_custo', 'safe', 'on' => 'search'),
			array('d_numero, d_andar, d_baia, d_ramal, d_area, p_andar, p_baia, p_ramal, p_area, p_email', 'length', 'max' => 45),
            array('d_local, p_local', 'length', 'max' => 60),
            array('status, os', 'length', 'max' => 45),
			array('d_uf, p_uf', 'length', 'max' => 2),
			array('d_cep, p_cep', 'length', 'max' => 9),
			array('ts_impressao, ts_entrega', 'safe'),
		);
	} 

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id'                       => 'ID',
			'track_expo'               => 'track_expo',
			'd_nome'                   => 'd_nome',
            'd_local'                  => 'd_local',
            'd_andar'                  => 'd_andar',
			'd_baia'                   => 'd_baia',
			'd_ramal'                  => 'd_ramal',
			'd_area'                   => 'd_area',
			'd_email'                  => 'd_email',
			'p_nome'                   => 'p_nome',
			'p_local'                  => 'p_local',
			'p_andar'                  => 'p_andar',
			'p_baia'                   => 'p_baia',
			'p_ramal'                  => 'p_ramal',
			'p_area'                   => 'p_area',
			'p_email'                  => 'p_email',
			'ts_impressao'             => 'ts_impressao',
			'ts_entrega'               => 'ts_entrega',
			'status'                   => 'status',
			'descricao'                => 'descricao',
			'Valor'                    => 'Valor',
			'c_custo'                  => 'c_custo',
			'os'                       => 'os',
			'obs'                      => 'obs',
			'b_area'                   => 'b_area',
						
		);
	}
	
	//	public function getCartaLink(){
	//	return CHtml::link($this->track_expo, Yii::app()->params["esporadicoArquivos"].$this->track_expo.".pdf");		
	//}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		
	
		$criteria->compare('id', $this->id, true);
		$criteria->compare('email', $this->email, true);
        $criteria->compare('senha', $this->senha, true);
		$criteria->compare('nome', $this->nome, true);
		$criteria->compare('local', $this->local, true);
		$criteria->compare('numero', $this->numero, true);
		$criteria->compare('andar', $this->andar, true);
		$criteria->compare('baia', $this->baia, true);
		$criteria->compare('bairro', $this->bairro, true);
		$criteria->compare('cidade', $this->cidade, true);
		$criteria->compare('cep', $this->cep, true);
		
		$criteria->compare('uf', $this->uf, true);
		$criteria->compare('area', $this->area, true);
		$criteria->compare('ramal', $this->ramal, true);
		$criteria->compare('centro_custo', $this->centro_custo, true);
		$criteria->compare('first', $this->first, true);

        $criteria->compare('perfil', $this->perfil, true);
        $criteria->compare('ativo', $this->ativo, true);
        $criteria->compare('confirmado', $this->confirmado, true);
        $criteria->compare('hash', $this->hash, true);
        $criteria->compare('matricula', $this->matricula, true);
        $criteria->compare('nome_local', $this->nome_local, true);
		$criteria->compare('conf', $this->conf, true);
        $criteria->compare('objeto_custo', $this->objeto_custo, true);
        $criteria->compare('divisao', $this->divisao, true);
        Yii::app()->session['ModificarMeusDados.criteria'] = $criteria;
		return new CActiveDataProvider($this, array('criteria'=>$criteria));
				
	
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Esporadico the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

}