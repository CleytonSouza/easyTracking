<script type="text/javascript">
	var cbID = '#<?php echo CHtml::activeId($model, 'tracks'); ?>';
	var delLink = '<a href="#" onClick="remover(this); return false;">Remover</a>';

	
	function remover(el){
	  $(el).parent().parent().remove();
	}
</script>

<script>
function desabilita(){
document.getElementById("enviar").disabled = true; // desabilitar
}
</script>

<input id="enviar" name="enviar" type="submit" value="Salvar" class="btn btn-primary"  onclick="desabilita();"/> 
<br>
<br>

<?php 


//Malote
$this->breadcrumbs = array('Malote');
?>



<!-- Alerta -->
<?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>true, // display a larger alert block?
        'fade'=>true, // use transitions?
        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
        'alerts'=>array( // configurations per alert type
            'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
        ),
    )); ?>

 

<!-- Crud Form -->
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'crud-form',
	'enableAjaxValidation'=>false,
)); ?>

    <?php echo $form->errorSummary($model); ?>
	
	<!-- Numero do lacre -->
	<?php echo $form->textFieldRow($model,'lacre',array(
	'class'=>'span4',
	'maxlength'=>75)); ?>

   
 

     <?php echo $form->textFieldRow($model, 'p_andar',array(
       'class'=>'span4',
       'maxlength'=>75)); ?>  

   

	
<fieldset id="origem">
<legend></legend> 
    
 	<?php echo $form->labelex($model,'de'); ?>

<?php
// De*
$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
	'model'=>$model, 
	'attribute'=>'de',
	'sourceUrl'=>$this->createUrl('/site/getLocal'),
    'options'=>array('minLength'=>'2'),
    'htmlOptions'=>array('class'=>'span4'),
));	
?>
	
<?php echo $form->labelex($model,'para'); ?>



<?php
$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
	'model'=>$model, 
	'attribute'=>'para',
	'sourceUrl'=>$this->createUrl('/site/getLocal'),
        'options'=>array('minLength'=>'2'),
        'htmlOptions'=>array('class'=>'span4'),
));	
?>

 <?php echo $form->dropDownListRow($model,'servico', array('Objetos Rastreáveis'=>'Objetos Rastreáveis','Carta Simples'=>'Carta Simples','Correspondência jurídica'=>'Correspondência jurídica','Outros'=>'Outros')); ?>
	
<fieldset id="origem">
<legend></legend> 

     <!-- OBS -->
     <?php echo $form->textFieldRow($model, 'obs',array(
       'class'=>'span4',
	   'cols'=>'50',
	   'rows' => '6',
	   'maxlength'=>'35')); ?> 
   
 
<fieldset id="origem">
<legend></legend> 



<?php echo $form->textFieldRow($model,'tracks',array('class'=>'span4', 'maxlength'=>'100')); ?>

<table border="0" id='cbTable'>
</table> 


<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(cbID).keyup(function(event){
		if(event.keyCode == 13){
			val = $(cbID).val();
			$(cbID).val('');
			field = val+'<input type="hidden" name="carta[]" value="'+val+'" />';
			$('#cbTable').append('<tr><td>'+delLink+'</td><td>'+field+'</td>');
		}
	});
</script>