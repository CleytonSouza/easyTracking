<style>
fieldset {
    border: 0 none;
    float: left;
    margin: 0 5px 0 0;
    padding: 0;
	width: 49%;
}	
</style>

<script type="text/javascript">
	function complete_campos(area, values){
		fields = $('#'+area+' input');	
		fields.eq(1).val(values.nome);
		fields.eq(2).val(values.local);
		fields.eq(3).val(values.andar);
		fields.eq(4).val(values.baia);
		fields.eq(5).val(values.ramal);
		fields.eq(6).val(values.area);
	}
</script>

<script>
function desabilita(){
document.getElementById("enviar").disabled = true; // desabilitar
}
</script>

<input id="enviar" name="enviar" type="submit" value="Salvar" class="btn btn-primary"  onclick="desabilita();"/> 
<br>
<br>

<?php 


$this->breadcrumbs = array('Carta');
?>

<?php
        Yii::app()->controller->widget('application.extensions.masks.Masks', array(
            'selector'=>'#tel1, #tel2',
            'options'=>array(
                'mask'=>'99 9999-99999',
                //'type'=>'reverse',
                'defaultValue'=>'',
            ),
        ));
?>
<?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>true, // display a larger alert block?
        'fade'=>true, // use transitions?
        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
        'alerts'=>array( // configurations per alert type
            'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
        ),
    )); ?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'crud-form',
	//'action'=>'http://127.0.0.1:8080/aes_prot',
	'enableAjaxValidation'=>false,
)); ?>

<fieldset id="cab1">

<?php echo $form->errorSummary($model); ?>

<?php echo $form->labelex($model,'d_email'); ?>

<?php
$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
	'model'=>$model, 
	'attribute'=>'d_email',
	'sourceUrl'=>$this->createUrl('getUsuarios'),
    'options'=>array('minLength'=>'2', 'select'=> 'js:function( event, ui ) {complete_campos("origem", ui.item)}'),
    'htmlOptions'=>array('class'=>'span5', 'disabled'=>!Yii::app()->user->isAdmin),
));	
?>

</fieldset>	

<fieldset id="cab2">

<?php echo $form->textFieldRow($model,'d_nome',array('class'=>'span5','maxlength'=>45, 'disabled'=>!Yii::app()->user->isAdmin)); ?>

</fieldset>


<div>	
<fieldset id="origem">
 
    <legend>Origem</legend>
		
	
	<span id="d_details">
	
	<?php echo $form->textFieldRow($model,'d_local',array('class'=>'span5','maxlength'=>45, /*'disabled'=>!Yii::app()->user->isAdmin*/)); ?>
	<?php echo $form->textFieldRow($model,'d_andar',array('class'=>'span1','maxlength'=>45, /*'disabled'=>!Yii::app()->user->isAdmin*/)); ?>
	<?php echo $form->textFieldRow($model,'d_baia',array('class'=>'span2','maxlength'=>45,/*'disabled'=>!Yii::app()->user->isAdmin*/)); ?>
	<?php echo $form->textFieldRow($model,'d_ramal',array('id'=>'tel1', 'class'=>'span3','maxlength'=>15,/*'disabled'=>!Yii::app()->user->isAdmin*/)); ?>
	<?php echo $form->textFieldRow($model,'d_area',array('class'=>'span5', /*'disabled'=>!Yii::app()->user->isAdmin*/)); ?>
	<?php echo $form->textFieldRow($model,'texto',array('class'=>'span5')); ?>
	</span>

</fieldset>

<fieldset id="destino">
    <legend>Destino</legend>
	
	
		<?php echo $form->labelex($model,'p_email'); ?>

<?php
$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
	'model'=>$model, 
	'attribute'=>'p_email',
	'sourceUrl'=>$this->createUrl('getUsuarios'),
    'options'=>array('minLength'=>'2', 'select'=> 'js:function( event, ui ) {complete_campos("destino", ui.item)}'),
    'htmlOptions'=>array('class'=>'span5'),
));	
?>	
	<?php echo $form->textFieldRow($model,'p_nome',array('class'=>'span5','maxlength'=>45)); ?>
	<?php echo $form->textFieldRow($model,'p_local',array('class'=>'span5','maxlength'=>45)); ?>
	<?php echo $form->textFieldRow($model,'p_andar',array('class'=>'span1','maxlength'=>45)); ?>
	<?php echo $form->textFieldRow($model,'p_baia',array('class'=>'span2','maxlength'=>45)); ?>
	<?php echo $form->textFieldRow($model,'p_ramal',array('id'=>'tel2', 'class'=>'span3','maxlength'=>15)); ?>
	<?php echo $form->textFieldRow($model,'p_area',array('class'=>'span5')); ?>
	</br>
	</fieldset>
	</div>
	

	
<?php $this->endWidget(); ?>