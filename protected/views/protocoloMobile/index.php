<?php

$templateView = '{view}{delete}';
if (Yii::app()->user->isAdmin || Yii::app()->user->isGeren) 
	$templateView = '{view}';
elseif (Yii::app()->user->perms["delete_movimento"]) $templateView .= '{delete}';
?>

<?php
$this->breadcrumbs=array(
	'Protocolo',
);


$this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'movimento-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'afterAjaxUpdate' => 'reinstallDatePicker',
	'columns'=>array(
		array(
			'name'=>'tracking',
			'type'=>'raw',
			'htmlOptions'=>array('width'=>'110px'),
		),
		
		array(
			'header'=>'E-mail do responsável',
			'name'=>'d_email',
			'htmlOptions'=>array('width'=>'110px'),
		),
		array(
			'header'=>'De',
			'name'=>'remetente',
		),
		array(
			'header'=>'Para',
			'name'=>'destinatario',
		),
		array(
			'header'=>'Correios',
			'name'=>'correio',
			'htmlOptions'=>array('width'=>'110px'),
		),
		array(
			'header'=>'Transp',
			'name'=>'transp',
			'htmlOptions'=>array('width'=>'60px'),
		),
		array(
			'name'=>'obs',
			'htmlOptions'=>array('width'=>'110px'),
		),
		array(
			'name'=>'ts_impressao',
			'value'=>'Converter::formatDateTime($data->ts_impressao)',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model, 
                'attribute'=>'ts_impressao', 
			), true),
			'htmlOptions'=>array('width'=>'100px'),
		),
		array(
			'name'=>'ts_assinatura',
			'value'=>'Converter::formatDateTime($data->ts_assinatura)',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model, 
                'attribute'=>'ts_assinatura', 
			), true),
			'htmlOptions'=>array('width'=>'100px'),
		),
		
		array(
		    'header'=>'Status',
			'name'=>'tipo',
			'htmlOptions'=>array('width'=>'110px'),
		),
		
		array(
			'header'=>'Empresa',
			'name'=>'u_empresa',
			'htmlOptions'=>array('width'=>'90px'),			
		),

		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{view}',
            'htmlOptions'=>array('width'=>'60px'),
		),
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{delete}',
			'visible'=>Yii::app()->user->isAdmin || Yii::app()->user->perms["delete_movimento"],			
		),
		
	),
));

Yii::app()->clientScript->registerScript('re-install-date-picker', "
function reinstallDatePicker(id, data) {
    $('.CJuiDatePicker').datepicker(jQuery.extend({showMonthAfterYear:false},jQuery.datepicker.regional['pt-BR'],[]));
$('a.view').click(function(){
	jQuery.get($(this).attr('href'), function( data ){
		$('#viewBody').html(data);
		$('#dlgView').dialog('open');
	});
  return false;
});
}
");

Yii::app()->clientScript->registerScript('request-view', "
$('a.view').click(function(){
	jQuery.get($(this).attr('href'), function( data ){
		$('#viewBody').html(data);
		$('#dlgView').dialog('open');
	});
  return false;
});
");

$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'dlgView',
    'options'=>array(
        'title'=>'Detalhes da postagem',
        'autoOpen'=>false,
		'modal'=>true,
		'width'=>'90%',
		'position'=>'center',
		'resizable'=>false,
		'buttons'=>array(
			array(
				'text'=>'Fechar',
				'click'=> new CJavaScriptExpression('function() { $( this ).dialog( "close" )}'),
			),
		),
    ),
));

echo '<div id="viewBody"> </div>';

$this->endWidget('zii.widgets.jui.CJuiDialog');

?>