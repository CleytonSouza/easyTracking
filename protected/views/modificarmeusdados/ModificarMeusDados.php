<?php
$usuario = Usuario::model()->findByPk(Yii::app()->user->id);
//echo '<span id="dialogArea">';
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'dlgUserEdit',
    'options'=>array(
        'title'=>'Modificar meus dados',
        'autoOpen'=>false,
        'width'=>'90%',
        'height'=>'580',
        'modal'=>true,
        'buttons' => array
        (
            'Salvar'=>'js:function(){$.post("'.$this->createAbsoluteUrl('/site/moduser').'", $("#modUserForm").serialize(), function(data, textStatus, jqXHR){if (data == "") {$("#dlgUserEdit").dialog("close"); location.reload(); } else $("#modUserArea").html(data);})}',
            'Cancelar'=>'js:function(){$("#dlgUserEdit").dialog("close");}',
        ),
    ),
));

//echo '<span id="modUserArea">';
$this->renderPartial('/usuario/userEdit', array('model'=>$usuario));
//echo '</span>';

$this->endWidget('zii.widgets.jui.CJuiDialog');

  
//echo '</span>';
?>