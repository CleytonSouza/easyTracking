<?php
$this->breadcrumbs=array(
	'Usuarios'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->toolbar=array(
	array('label'=>'Voltar','url'=>array('/usuario')),
	array('label'=>'Salvar','url'=>array('/usuario'), 'type'=>'primary', 'buttonType'=>'submit'),
);
?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>