<?php
$this->breadcrumbs=array(
	'Usuarios'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Usuario','url'=>array('index')),
	array('label'=>'Create Usuario','url'=>array('create')),
	array('label'=>'Update Usuario','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Usuario','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Usuario','url'=>array('admin')),
);
?>


<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'track_carta',
		'malote',
		'de_usuario_id',
		'de_local_id',
		'para_usuario_id',
		'para_local_id',
		'd_nome',
		'd_matricula',
		'd_local',
		'd_andar',
		'd_baia',
		'd_ramal',
		'd_area',
		'p_nome',
		'p_matricula',
		'p_local',
		'p_andar',
		'p_baia',
		'p_ramal',
		'p_area',
		'ts_impressao',
		'pen_impressao',
		'ts_sup_inicio',
		'ts_motoboy_1',
		'ts_transp',
		'ts_motoboy_2',
		'ts_sup_fim',
		'status',
		'tipo',
	),
)); ?>
