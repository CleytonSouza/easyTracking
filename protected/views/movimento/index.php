<?php


$this->breadcrumbs=array(
	'Malote',
);

$this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'movimento-grid',
	//'type'=>'striped bordered condensed',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'afterAjaxUpdate' => 'reinstallDatePicker',
	'columns'=>array(
		
		array(
			'name'=>'track_carta',
			'type'=>'raw',
		//	'value' => '$data->getCartaLink()',
			'htmlOptions'=>array('width'=>'110px'),
		),
		array(
			'name'=>'track_malote',
			'type'=>'raw',
			'value' => '$data->getMaloteLink()',
			'htmlOptions'=>array('width'=>'110px'),
		),
		array(
			'name'=>'malote',
			'type'=>'raw',
		//	'value' => '$data->getMaloteLink()',
			'htmlOptions'=>array('width'=>'60px'),
		),
		array(
			'header'=>'Etiqueta',
			'name'=>'track_msg',
			'type'=>'raw',
		//	'value' => '$data->getMsgLink()',
			'htmlOptions'=>array('width'=>'110px'),
		),
		
		array(
			'header'=>'Origem',
			'name'=>'d_nome',
			//'filter'=>Yii::app()->user->isAdmin ? null : false,
		//	'value'=>'$data->d_nome. (!empty($data->d_email) ? " <".$data->d_email.">": "");'
		),
		array(
			'header'=>'Destino',
			'name'=>'p_nome',
		//	'value'=>'$data->p_nome. (!empty($data->p_email) ? " <".$data->p_email.">": "");'
		),

		array(
			'name'=>'ts_impressao',
			'value'=>'Converter::formatDateTime($data->ts_impressao)',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model, 
                'attribute'=>'ts_impressao', 
			), true),
			'htmlOptions'=>array('width'=>'80px'),
		),

		array(
			'name'=>'ts_malote',
			'value'=>'Converter::formatDateTime($data->ts_malote)',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model, 
                'attribute'=>'ts_malote', 
			), true),
			'htmlOptions'=>array('width'=>'80px'),
		),

/*
		array(
			'name'=>'ts_sup_inicio',
			'value'=>'Converter::formatDateTime($data->ts_sup_inicio)',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model, 
                'attribute'=>'ts_sup_inicio', 
			), true),
			'htmlOptions'=>array('width'=>'80px'),
		),
		array(
			'name'=>'ts_motoboy_1',
			'value'=>'Converter::formatDateTime($data->ts_motoboy_1)',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model, 
                'attribute'=>'ts_motoboy_1', 
			), true),
			'htmlOptions'=>array('width'=>'80px'),
		),
		array(
			'name'=>'ts_transp',
			'value'=>'Converter::formatDateTime($data->ts_transp)',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model, 
                'attribute'=>'ts_transp', 
			), true),
			'htmlOptions'=>array('width'=>'80px'),
		),
		array(
			'name'=>'ts_motoboy_2',
			'value'=>'Converter::formatDateTime($data->ts_motoboy_2)',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model, 
                'attribute'=>'ts_motoboy_2', 
			), true),
			'htmlOptions'=>array('width'=>'80px'),
		),
*/
		array(
			'header'=>'Entrega',
			'name'=>'ts_sup_fim',
			'value'=>'Converter::formatDateTime($data->ts_sup_fim)',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model, 
                'attribute'=>'ts_sup_fim', 
			), true),
			'htmlOptions'=>array('width'=>'80px'),
		),
		array(
			'header'=>'Status',
			'name'=>'status',
			'htmlOptions'=>array('width'=>'70px'),			
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{delete}',
			'visible'=>Yii::app()->user->isAdmin || Yii::app()->user->perms["delete_movimento"],			
		),		
	),
));

Yii::app()->clientScript->registerScript('re-install-date-picker', "
function reinstallDatePicker(id, data) {
    $('.CJuiDatePicker').datepicker(jQuery.extend({showMonthAfterYear:false},jQuery.datepicker.regional['pt-BR'],[]));
}
");
