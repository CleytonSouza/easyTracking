<style>
fieldset {
    border: 0 none;
    float: left;
    margin: 0 5px 0 0;
    padding: 0;
	 width: 49%;
}	
</style>

<script type="text/javascript">
	function complete_campos(area, values){
		fields = $('#'+area+' input');	
		fields.eq(1).val(values.nome);
		fields.eq(2).val(values.local);
		fields.eq(3).val(values.andar);
		fields.eq(4).val(values.baia);
		fields.eq(5).val(values.ramal);
		fields.eq(6).val(values.area);
	}
</script>

<?php 
$this->toolbar=array(
	array('url'=>array('/movimento'), 'icon'=>'icon-chevron-left'),
	array('label'=>'Salvar', 'type'=>'primary', 'onclick'=>'js:$("#crud-form").submit()'),
);

$this->breadcrumbs = array(
	$this->action->id == 'create' ? 'Nova carta' : 'Modificando carta',
);
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'crud-form',
	'action'=>'http://127.0.0.1:8082/aes_prot',
	'enableAjaxValidation'=>false,
)); ?>
<?php echo $form->errorSummary($model); ?>
	
<fieldset id="origem">
 
    <legend>Origem</legend>
		<?php echo $form->label($model,'d_email'); ?>

<?php
$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
	'model'=>$model, 
	'attribute'=>'d_email',
	'sourceUrl'=>$this->createUrl('getUsuarios'),
    'options'=>array('minLength'=>'2', 'select'=> 'js:function( event, ui ) {complete_campos("origem", ui.item)}'),
    'htmlOptions'=>array('class'=>'span5', 'disabled'=>!Yii::app()->user->isAdmin),
));	
?>
	
	<span id="d_details" <?php if (!Yii::app()->user->isAdmin) : ?> style="display:none" <?php endif; ?>>
	<?php echo $form->textFieldRow($model,'d_nome',array('class'=>'span5','maxlength'=>45, 'disabled'=>!Yii::app()->user->isAdmin)); ?>
	<?php echo $form->textFieldRow($model,'d_local',array('class'=>'span5','maxlength'=>45, 'disabled'=>!Yii::app()->user->isAdmin)); ?>
	<?php echo $form->textFieldRow($model,'d_andar',array('class'=>'span1','maxlength'=>45, 'disabled'=>!Yii::app()->user->isAdmin)); ?>
	<?php echo $form->textFieldRow($model,'d_baia',array('class'=>'span2','maxlength'=>45, 'disabled'=>!Yii::app()->user->isAdmin)); ?>
	<?php echo $form->textFieldRow($model,'d_ramal',array('class'=>'span3','maxlength'=>15, 'disabled'=>!Yii::app()->user->isAdmin)); ?>
	<?php echo $form->textFieldRow($model,'d_area',array('class'=>'span5','maxlength'=>45, 'disabled'=>!Yii::app()->user->isAdmin)); ?>
	</span>

</fieldset>

<fieldset id="destino">
    <legend>Destino</legend>
	
	
		<?php echo $form->label($model,'p_email'); ?>

<?php
$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
	'model'=>$model, 
	'attribute'=>'p_email',
	'sourceUrl'=>$this->createUrl('getUsuarios'),
    'options'=>array('minLength'=>'2', 'select'=> 'js:function( event, ui ) {complete_campos("destino", ui.item)}'),
    'htmlOptions'=>array('class'=>'span5'),
));	
?>	
	<?php echo $form->textFieldRow($model,'p_nome',array('class'=>'span5','maxlength'=>45)); ?>
	<?php echo $form->textFieldRow($model,'p_local',array('class'=>'span5','maxlength'=>45)); ?>
	<?php echo $form->textFieldRow($model,'p_andar',array('class'=>'span1','maxlength'=>45)); ?>
	<?php echo $form->textFieldRow($model,'p_baia',array('class'=>'span2','maxlength'=>45)); ?>
	<?php echo $form->textFieldRow($model,'p_ramal',array('class'=>'span3','maxlength'=>15)); ?>
	<?php echo $form->textFieldRow($model,'p_area',array('class'=>'span5','maxlength'=>45)); ?>
</fieldset>

<?php $this->endWidget(); ?>