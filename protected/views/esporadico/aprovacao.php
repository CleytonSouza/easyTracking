<?php 
$this->toolbar=array(
	array('url'=>array('/esporadico'), 'icon'=>'icon-chevron-left'),
	array('label'=>'Salvar', 'type'=>'primary', 'onclick'=>'js:$("#crud-form").submit()'),
);

$this->breadcrumbs = array('Aprova Motoboy');
?>

<?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>true, // display a larger alert block?
        'fade'=>true, // use transitions?
        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
        'alerts'=>array( // configurations per alert type
            'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
        ),
    )); ?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'crud-form',
	//'action'=>'http://127.0.0.1:8082/aes_prot',
	'enableAjaxValidation'=>false,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->label($model,'track_expo'); ?>
<?php echo $form->textField($model,'track_expo',array('class'=>'span2', 'disabled'=>true)); ?>

<?php echo Chtml::label('Origem', 'Esporadico_d_email') ?>
<?php echo $form->textField($model,'d_email',array('class'=>'span2', 'disabled'=>true)); ?>

<?php echo $form->labelEx($model,'aprovacao'); ?>
<?php 
    $ar = array_keys(Yii::app()->params['aprovacao']);
	$ar = array_combine($ar, $ar);
	echo $form->dropDownList($model,'aprovacao', $ar); 
?>

<?php $this->endWidget(); ?>