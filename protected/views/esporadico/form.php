<?php 
$this->breadcrumbs = array('Reversa');
?>

<?php
Yii::app()->controller->widget('application.extensions.masks.Masks', array(
  'selector'=>'#tel1, #tel2',
  'options'=>array(
    'mask'=>'99 9999-99999',
                //'type'=>'reverse',
    'defaultValue'=>'',
    ),
  ));
Yii::app()->controller->widget('application.extensions.masks.Masks', array(
  'selector'=>'.d_cep',
  'options'=>array(
    'mask'=>'99999-999',
                //'type'=>'reverse',
    'defaultValue'=>'',
    ),
  ));
Yii::app()->controller->widget('application.extensions.masks.Masks', array(
  'selector'=>'.d_numero',
  'options'=>array(
    'mask'=>'99999',
                //'type'=>'reverse',
    'defaultValue'=>'',
    ),
  ));
Yii::app()->controller->widget('application.extensions.masks.Masks', array(
  'selector'=>'.d_andar',
  'options'=>array(
    'mask'=>'99999',
                //'type'=>'reverse',
    'defaultValue'=>'',
    ),
  ));
  ?>

  <?php

  include 'conexao\conexao.php';

  $id=Yii::app()->user->id;

  $sql = "SELECT email, empresa, nome, cep, local, numero, andar, baia, bairro, cidade, uf, ramal, area, centro_custo FROM easytracking.tb_usuario WHERE id = $id";

  $resultado = mysql_query($sql, $conecta) or print_r(mysql_error());

  $query = mysql_fetch_array($resultado);

  $d_email = utf8_encode($query['0']);
  $u_empresa = $query['1'];
  $nome = utf8_encode($query['2']);
  $cep = $query['3'];
  $local = utf8_encode($query['4']);
  $numero = $query['5'];
  $andar = $query['6'];
  $baia = $query['7'];
  $bairro = utf8_encode($query['8']);
  $cidade = utf8_encode($query['9']);
  $uf = $query['10'];
  $ramal = $query['11'];
  $area = utf8_encode($query['12']);
  $centro_custo = utf8_encode($query['13']);
  ?>
  <? mysql_close($conecta); ?>
  <style>
    .desabilitado {
      background: #EEE; 
      cursor: not-allowed; 
      color: #777;
    }
  </style>

  <!doctype html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
  <!--  <div class="alert alert-danger">
     <h5>Campos com * são obrigatórios</h5>
   </div> -->
   <ul class="nav nav-tabs">
    <li class="active"><a href="#tabs-1" data-toggle="tab">Origem</a></li>
    <li><a href="#tabs-2" data-toggle="tab">Destino</a></li>
    <li><a href="#tabs-3" data-toggle="tab">Finalizar Solicitação</a></li>
  </ul>

  <form action="actions\motoboy.php" method="POST">

    <div class="tab-content">

      <div class="tab-pane active" id="tabs-1">
       <legend>Coleta</legend>
       <label class="control-label required">Nome<span class="required">*</span></label><input type="text"  id="nomeorigem" name="d_nome" value=""><br>

       <label>CEP<span class="required">*</span></label><input type="text" id="d_cep" class="d_cep" name="d_cep"  onBlur="getColeta()" ><br>
       <label>Endereço<span class="required">*</span></label><input type="text" id="d_localorigem" name="d_local" value=""> <br>
       <label>Número<span class="required">*</span></label><input type="text" name="d_numero" id="numeroorigem" class="d_numero" value=""><br>
       <label>Andar<span class="required">*</span></label><input type="text" name="d_andar" id="andarorigem" class="d_andar" value=""><br>
      <!-- <label>Baia</label><input type="text" name="d_baia" value=""><br> -->           
       <label>Bairro<span class="required">*</span></label><input type="text" id="d_bairroorigem" name="d_bairro" value=""><br>
       <label>Cidade<span class="required">*</span></label><input type="text" id="d_cidadeorigem" name="d_cidade" value=""><br>
       <label>UF<span class="required">*</span></label><input type="text"  id="d_uforigem"  name="d_uf" value=""><br>
       <label>Ramal<span class="required">*</span></label><input type="text" name="d_ramal" id="ramalorigem" value=""><br>
       <label>Area<span class="required">*</span></label><input type="text" name="d_area" id="areaorigem" value=""><br>
       <label>Observação<span class="required">*</span></label><input type="text" id="obsorigem" name="obs" value=""><br>
     </div>
     
     <div class="tab-pane" id="tabs-2">
      <legend>Entrega</legend>
      <label class="control-label required">Nome<span class="required">*</span></label><input type="text" id="nomedestino" name="endereco[]" > <br>
      <label>CEP<span class="required">*</span></label><input type="text" id="cep" class="d_cep" name="endereco[]" onBlur="getEndereco()" ><br>
      <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/"> Encontre o CEP que você procura aqui  </a><br><br>
      <label>Endereço<span class="required">*</span></label><input type="text" id="endereco" name="endereco[]"><br>
      <label>Número<span class="required">*</span></label><input type="text" id="numerodestino" class="d_numero" name="endereco[]"><br>
      <label>Andar<span class="required">*</span></label><input type="text" id="andardestino" name="endereco[]" class="d_andar"><br>
   <!--   <label>Baia</label> <input type="text" id="baiadestino" name="endereco[<? #php echo uniqid();?>][baia]"><br> -->
      <label>Bairro<span class="required">*</span></label><input type="text" id="bairro" name="endereco[]"><br>
      <label>Cidade<span class="required">*</span></label><input type="text" id="cidade" name="endereco[]"><br>
      <label>UF<span class="required">*</span></label><input type="text" id="uf" name="endereco[]" maxlength="2"><br>
      <label>Ramal<span class="required">*</span></label><input type="text" id="ramaldestino" name="endereco[]"><br>
      <label>Area<span class="required">*</span></label><input type="text" id="areadestino" name="endereco[]"><br>
      <legend></legend> 
    </div> 


    <div class="tab-pane" id="tabs-3">

     <legend>Dados do Solicitante</legend>   
     <label>E-mail do Solicitante</label><input type="email" id="campo_obrigatorio" name="email" value="<?php echo "$d_email";?>"  style="background: #EEE; cursor: not-allowed; color: #777" readonly>
     <label>C. Custo <span class="required">*</span></label><input type="text" id="campo_obrigatorio" name="c_custo" value="<?php echo "$centro_custo";?>" style="background: #EEE; cursor: not-allowed; color: #777" readonly> 
     <label>Projeto de Cobrança<span class="required">*</span></label><input type="text" name="c_custo_cobranca" > 
    <!--
      <label>Recuperável<span class="required">* </span></label>   
     <select name="recuperavel">
      <option value="sim">SIM</option>
      <option value="nao">NÃO</option>
    </select> -->


     <label>Tipo de Serviço <span class="required">* </span></label>     
     <select name="servico">
      <option value="contrato">Contrato</option>
    </select>

    <label>Atendimento</label>   
    <select name="atendimento">
     <option value="coleta">Entrega</option>
     <option value="entrega">Coleta</option>
   </select>
   <div id="mensagem"></div>
   <input id="enviar" name="enviar" type="submit" value="Salvar" class="btn btn-primary" /> 
 </div><!--Div tbs-3 -->
</div><!--tab-content -->
</form>
</body>
</html>
<!--<script src="js\completa_cep_coleta.js"></script> -->
<script src="js\completa_cep_entrega.js"></script>
<script src="js\clona.js"></script>
<script src="js\submit.js"></script>

