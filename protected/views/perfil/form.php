<?php
$this->breadcrumbs=array(
	'Usuarios'=>array('/perfil'),
	$this->action->id == 'create' ? 'Adicionando' : 'Modificando',
);

$this->toolbar=array(
	array('url'=>array('/perfil'), 'icon'=>'icon-chevron-left'),
	array('label'=>'Salvar', 'type'=>'primary', 'onclick'=>'js:$("#crud-form").submit()'),
);
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'crud-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>
	<?php echo $form->textFieldRow($model,'nome',array('class'=>'span6','maxlength'=>45)); ?>
	<?php echo $form->checkboxRow($model,'admin'); ?>
	<?php echo $form->checkboxRow($model,'transportadora'); ?>

	<fieldset>
		<legend> Movimento </legend>
		<?php echo $form->checkboxRow($model, 'movimento'); ?>
		<?php echo $form->checkboxRow($model, 'criar_carta'); ?>
		<?php echo $form->checkboxRow($model, 'criar_malote'); ?>
		<?php echo $form->checkboxRow($model, 'delete_movimento'); ?>
		<?php echo $form->checkboxRow($model, 'recebe_malote'); ?>
	</fieldset>

	<fieldset>
		<legend> Motoboy </legend>
		<?php echo $form->checkboxRow($model,'esporadico'); ?>
		<?php echo $form->checkboxRow($model,'auto_esporadico'); ?>
		<?php echo $form->checkboxRow($model,'criar_esporadico'); ?>
		<?php echo $form->checkboxRow($model,'modificar_esporadico'); ?>
		<?php echo $form->checkboxRow($model,'delete_esporadico'); ?>
		<?php echo $form->checkboxRow($model,'aprova_esporadico'); ?>
	</fieldset>
	
	<fieldset>
		<legend> Postagem </legend>
		<?php echo $form->checkboxRow($model,'postagem'); ?>
		<?php echo $form->checkboxRow($model,'auto_postagem'); ?>
		<?php echo $form->checkboxRow($model,'criar_postagem'); ?>
		<?php echo $form->checkboxRow($model,'modificar_postagem'); ?>
		<?php echo $form->checkboxRow($model,'delete_postagem'); ?>
		<?php echo $form->checkboxRow($model,'aprova_postagem'); ?>
		<?php echo $form->checkboxRow($model,'recebe_postagem'); ?>
		<?php echo $form->checkboxRow($model,'juridico_postagem'); ?>
		<?php echo $form->checkboxRow($model,'sedex10'); ?>
	</fieldset>

	<fieldset>
		<legend> Protocolo </legend>
		<?php echo $form->checkboxRow($model,'protocolo'); ?>
		<?php echo $form->checkboxRow($model,'protocolo_criar'); ?>
	</fieldset>

	<fieldset>
		<legend> Correios </legend>
		<?php echo $form->checkboxRow($model,'receber_pool'); ?>
	</fieldset>
	
	<fieldset>
		<legend> Relatórios </legend>
		<?php echo $form->checkboxRow($model,'rel_cr'); ?>
		<?php echo $form->checkboxRow($model,'rel_ce'); ?>
		<?php echo $form->checkboxRow($model,'rel_motoboy'); ?>
	</fieldset>
	


	

<?php $this->endWidget(); ?>
