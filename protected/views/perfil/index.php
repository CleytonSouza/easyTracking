<?php
$this->breadcrumbs=array(
	'Perfil de acesso',
);
/*
$this->toolbar=array(
	array('label'=>'Adicionar','url'=>array('create'), 'type'=>'primary',),
);
*/
$this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'perfil-grid',
	'type'=>'striped bordered condensed',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'name'=>'nome',
			'type'=>'raw',
			'value'=>'$data->getBadgeNome()',
		),
		//array('name'=>'admin',        'type'=>'raw', 'filter'=>$model->getCheckFilter("admin"), 'value'=>'$data->getCheck("admin")'),
		array('header'=>'Movimento', 'type'=>'raw', 'value'=>'$data->getMovimentoTextPerm()', 'htmlOptions'=>array('width'=>'180px')),
	//	array('header'=>'Correio', 'type'=>'raw', 'value'=>'$data->getPoolTextPerm()', 'htmlOptions'=>array('width'=>'180px')),
		//array('header'=>'Rota', 'type'=>'raw', 'value'=>'$data->getRotaTextPerm()', 'htmlOptions'=>array('width'=>'180px')),
		array('header'=>'Esporádico', 'type'=>'raw', 'value'=>'$data->getEsporadicoTextPerm()', 'htmlOptions'=>array('width'=>'180px')),
	//	array('header'=>'Postagem', 'type'=>'raw', 'value'=>'$data->getPostagemTextPerm()', 'htmlOptions'=>array('width'=>'180px')),		
		array('header'=>'Protocolo', 'type'=>'raw', 'value'=>'$data->getprotocoloTextPerm()', 'htmlOptions'=>array('width'=>'180px')),		
		array('header'=>'Relatórios', 'type'=>'raw', 'value'=>'$data->getProtocoloRelatorios()', 'htmlOptions'=>array('width'=>'180px')),		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); 
