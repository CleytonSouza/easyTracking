<?php
$this->breadcrumbs=array(
	'Perfil de acesso'=>array('index'),
	$model->nome,
);

$this->toolbar=array(
	array('url'=>array('/perfil'), 'icon'=>'icon-chevron-left', 'type'=>'primary'),
	array('label'=>'Adicionar','url'=>array('create')),
	array('label'=>'Modificar','url'=>array('update','id'=>$model->id)),
	array('label'=>'Apagar','url'=>'#','buttonType'=>'submit', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);

$this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nome',
		'criar_carta',
		'criar_malote',
		'pools',
		'criar_pool',
		'receber_pool',
		'rota',
		'criar_rota',
		'admin',
		'sedex10',
	),
)); 
