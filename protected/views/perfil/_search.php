<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'senha',array('class'=>'span5','maxlength'=>32)); ?>

	<?php echo $form->textFieldRow($model,'matricula',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'nome',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'local',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'andar',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'baia',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'ramal',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'perfil',array('class'=>'span5','maxlength'=>45)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
