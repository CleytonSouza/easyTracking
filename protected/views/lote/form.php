<html>

<head>
  <meta charset="UTF-8">
</head>

<script type="text/javascript">
    var cbID = '#<?php echo CHtml::activeId($model, 'tracks'); ?>';
    var delLink = '<a href="#" onClick="remover(this); return false;">Remover</a>';
    
  
    
    function remover(el){
      $(el).parent().parent().remove();
    }
</script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    
    <!--CSS Link    -->
  <link rel="stylesheet" type="text/css" href="css/estilo.css">

  <script>
  $(function() {
    $( "#datepicker1" ).datepicker({
       dateFormat : 'yy-mm-dd',
       monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
       dayNamesShort: ['Dom', 'Seg', 'Ter'  , 'Quar', 'Qui', 'Sex', 'Sab'],
       dayNamesMin: ['Dom', 'Seg', 'Ter'  , 'Quar', 'Qui', 'Sex', 'Sab']
     });
    
  });
  </script>




<script>
function desabilita(){
document.getElementById("enviar").disabled = true; // desabilitar
}
</script>

<input id="enviar" name="enviar" type="submit" value="Salvar" class="btn btn-primary"  onclick="desabilita();"/> 
<br>
<br>


<?php
//Malote
$this->breadcrumbs = array('Lote');
?>

 


<!-- Alerta -->
<?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>true, // display a larger alert block?
        'fade'=>true, // use transitions?
        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
        'alerts'=>array( // configurations per alert type
            'success'=>array('block'=>true, 'fade'=>true, 'x'=>'&times;'), // success, info, warning, error or danger
        ),
    )); ?>

 

<!-- Crud Form -->
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'crud-form',
	'enableAjaxValidation'=>false,
)); ?>

    <?php echo $form->errorSummary($model); ?>
	
	
	<?php echo $form->textFieldRow($model,'de',array(
	'class'=>'span4',
	'maxlength'=>75)); ?>

   
    <?php echo $form->textFieldRow($model, 'local_origem',array(
       'class'=>'span4',
       'maxlength'=>150)); ?>   

     <?php echo $form->textFieldRow($model, 'para',array(
       'class'=>'span4',
       'maxlength'=>75)); ?>  

    <?php echo $form->textFieldRow($model, 'local_destino',array(
       'class'=>'span4',
       'maxlength'=>150)); ?>  


 <?php echo $form->dropDownListRow($model,'transporte', array('Objetos Rastreaveis'=>'Objetos Rastreaveis','Carta Simples'=>'Carta Simples','Juridico'=>'Juridico','Outros'=>'Outros')); ?>

<?php echo $form->textFieldRow($model,'data_chegada',array(
          'id'=>'datepicker1', 
          'class'=>'span4',
          'maxlength'=> 15, )); ?>

<?php echo $form->textFieldRow($model,'obs',array(
          'class'=>'span4',
          'maxlength'=> 300, )); ?>

<?php echo $form->textFieldRow($model,'tracks',array('class'=>'span5', 'maxlength'=>'100')); ?>

<table border="0" id='cbTable'>
</table> 

<?php $this->endWidget(); ?>

<?php 
$contador = 0;?>

<script type="text/javascript">

var contador = 0;
contador++;
	
	$(cbID).keyup(function(event){
		if(event.keyCode == 13){
			val = $(cbID).val();
			$(cbID).val('');
			field = val+'<input type="hidden" name="carta[]" value="'+val+'" />';
			$('#cbTable').append('<tr><td>'+contador+++'</td><td>'+delLink+'</td><td>'+field+'</td>');
		}
	});
	

</script>


</html>