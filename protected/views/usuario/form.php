<?php
$this->breadcrumbs=array(
	'Usuarios'=>array('/usuario'),
	$this->action->id == 'create' ? 'Adicionando' : 'Modificando',
);

$this->toolbar=array(
	array('url'=>array('/usuario'), 'icon'=>'icon-chevron-left'),
	array('label'=>'Salvar', 'type'=>'primary', 'onclick'=>'js:$("#crud-form").submit()'),
);
?>
<?php
        Yii::app()->controller->widget('application.extensions.masks.Masks', array(
            'selector'=>'#tel',
            'options'=>array(
                'mask'=>'99 9999-99999',
                //'type'=>'reverse',
                'defaultValue'=>'',
            ),
        ));
?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'crud-form',
	'enableAjaxValidation'=>false,
)); ?>
	<?php echo $form->errorSummary($model); ?>
	<?php echo $form->textFieldRow($model,'nome',array('class'=>'span6','maxlength'=>60)); ?>
	<?php echo $form->textFieldRow($model,'email',array('class'=>'span6','maxlength'=>60)); ?>
	<?php echo $form->passwordFieldRow($model,'senha',array('class'=>'span2','maxlength'=>32)); ?>
	<?php echo $form->textFieldRow($model,'matricula',array('class'=>'span2','maxlength'=>45));?>
	<?php echo $form->textFieldRow($model,'nome_local',array('class'=>'span5','maxlength'=>60)); ?>
	<?php echo $form->textFieldRow($model,'local',array('class'=>'span5','maxlength'=>60)); ?>
	<?php echo $form->textFieldRow($model,'numero',array('class'=>'span2','maxlength'=>45)); ?>
	<?php echo $form->textFieldRow($model,'andar',array('class'=>'span1','maxlength'=>45)); ?>
	<?php echo $form->textFieldRow($model,'baia',array('class'=>'span3','maxlength'=>45)); ?>
	<?php echo $form->textFieldRow($model,'bairro',array('class'=>'span5','maxlength'=>60)); ?>
	<?php echo $form->textFieldRow($model,'cidade',array('class'=>'span5','maxlength'=>60)); ?>
	<?php echo $form->textFieldRow($model,'uf',array('class'=>'span1','maxlength'=>2)); ?>
	<?php echo $form->textFieldRow($model,'cep',array('class'=>'span1','maxlength'=>9)); ?>
	<?php echo $form->textFieldRow($model,'ramal',array('id'=>'tel', 'class'=>'span2','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'area',array('class'=>'span2','maxlength'=>45)); ?>

    <div class="control-group ">
        <label class="control-label required" for="Usuario_centro_custo">
            Centro de custo
            <span class="required">*</span>
        </label>
        <div class="controls">
            <?php
            $this->widget('zii.widgets.jui.CJuiAutoComplete',array(
                'model'=>$model,
                'attribute'=>'centro_custo',
                'sourceUrl'=>$this->createUrl('/site/findCC'),
				'options'=>array('minLength'=>'1'),
                'htmlOptions'=>array('class'=>'span2'),
            ));
            ?>
        </div>
    </div>
	
    <div class="control-group ">
      <!--  <label class="control-label required" for="Usuario_objeto_custo">
            Objeto de custo
            <span class="required">*</span> 
        </label> -->
        <div class="controls">
            <?php
            $this->widget('zii.widgets.jui.CJuiAutoComplete',array(
                'model'=>$model,
                'attribute'=>'objeto_custo',
                'sourceUrl'=>'js: "'.$this->createUrl('/site/findOC', array('cc'=>'')).'"+$("#Usuario_centro_custo").val()',
				'options'=>array('minLength'=>'1'),
                'htmlOptions'=>array('class'=>'span2'),
            ));
            ?>
        </div>
    </div>	

	<?php echo $form->checkboxRow($model,'ativo'); ?>
	<?php echo $form->dropDownListRow($model,'perfil', CHtml::listData(Perfil::model()->findAll(), 'id', 'nome'), array('empty'=>'Nenhum perfil associado')); ?>



<?php $this->endWidget(); ?>

<script type="text/javascript">
	function executeAutoCompleteObjetoCusto(){
		jQuery('#Usuario_objeto_custo').autocomplete({'minLength':'1','source': "<?php echo $this->createUrl('/site/findOC', array('cc'=>'')); ?>"+$("#Usuario_centro_custo").val()});
	}
	
	$().ready(function(){
		$('#Usuario_centro_custo').blur(function(){executeAutoCompleteObjetoCusto()});
	})
</script>