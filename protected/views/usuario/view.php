<?php
$this->breadcrumbs=array(
	'Usuarios'=>array('index'),
	$model->nome,
);

$this->toolbar=array(
	array('url'=>array('/usuario'), 'icon'=>'icon-chevron-left', 'type'=>'primary'),
	array('label'=>'Adicionar','url'=>array('create')),
	array('label'=>'Modificar','url'=>array('update','id'=>$model->id)),
	array('label'=>'Apagar','url'=>'#','buttonType'=>'submit', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);

$this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'email',
		'nome',
		'matricula',
                array(
                    'name'=>'senha',
                    'value'=>'*****',
                ),
		'nome_local',
		'local',
		'numero',
		'andar',
		'baia',
		'bairro',
		'cidade',
		'uf',
		'cep',
		'ramal',
		'area',
		'centro_custo',
		'perfil',
		'ativo',
	),
)); 
