<style>
    #modUserForm div {
        height: 35px;
    }

</style>


<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'modUserForm',
    'type'=>'horizontal',
	'enableAjaxValidation'=>false,
    //'hideInlineErrors'=>true,
)); ?>

	<?php echo $form->errorSummary($model); ?></span>
	<div><?php echo $form->textFieldRow($model,'nome',array('class'=>'span6','maxlength'=>60)); ?></div>
    <div><?php echo $form->textFieldRow($model,'email',array('class'=>'span6','maxlength'=>60)); ?></div>
    <div><?php echo $form->textFieldRow($model,'matricula',array('class'=>'span2','maxlength'=>45));?></div>
    <div><?php echo $form->textFieldRow($model,'nome_local',array('class'=>'span5','maxlength'=>60)); ?></div>
    <div><?php echo $form->textFieldRow($model,'local',array('class'=>'span5','maxlength'=>60)); ?></div>
    <div><?php echo $form->textFieldRow($model,'numero',array('class'=>'span2','maxlength'=>45)); ?></div>
    <div><?php echo $form->textFieldRow($model,'andar',array('class'=>'span1','maxlength'=>45)); ?></div>
    <div><?php echo $form->textFieldRow($model,'baia',array('class'=>'span3','maxlength'=>45)); ?></div>
    <div><?php echo $form->textFieldRow($model,'cep',array('class'=>'span1','maxlength'=>9)); ?></div>
    <div><?php echo $form->textFieldRow($model,'bairro',array('class'=>'span5','maxlength'=>60)); ?></div>
    <div><?php echo $form->textFieldRow($model,'cidade',array('class'=>'span5','maxlength'=>60)); ?></div>
    <div><?php echo $form->textFieldRow($model,'uf',array('class'=>'span1','maxlength'=>2)); ?></div>
    <div><?php echo $form->textFieldRow($model,'ramal',array('class'=>'span2','maxlength'=>45)); ?></div>
    <div><?php echo $form->textFieldRow($model,'area',array('class'=>'span2','maxlength'=>45)); ?></div>
    <div class="control-group ">
        <label class="control-label required" for="Usuario_centro_custo">
            Centro de custo
            <span class="required">*</span>
        </label>
        <div class="controls">
            <?php
            $this->widget('zii.widgets.jui.CJuiAutoComplete',array(
                'model'=>$model,
                'attribute'=>'centro_custo',
                'sourceUrl'=>$this->createUrl('/site/findCC'),
				'options'=>array('minLength'=>'1'),
                'htmlOptions'=>array('id'=>'Usuario_centro_custo1', 'class'=>'span2'),
            ));
            ?>
        </div>
    </div>
	
    <div class="control-group ">
        <label class="control-label required" for="Usuario_objeto_custo">
            Objeto de custo
            <span class="required">*</span>
        </label>
        <div class="controls">
            <?php
            $this->widget('zii.widgets.jui.CJuiAutoComplete',array(
                'model'=>$model,
                'attribute'=>'objeto_custo',
                'sourceUrl'=>'js: "'.$this->createUrl('/site/findOC', array('cc'=>'')).'"+$("#Usuario_centro_custo").val()',
				'options'=>array('minLength'=>'1'),
                'htmlOptions'=>array('id'=>'Usuario_objeto_custo1', 'class'=>'span2'),
            ));
            ?>
        </div>
    </div>	

<?php $this->endWidget(); ?>

<script type="text/javascript">
	function executeAutoCompleteObjetoCusto1(){
		jQuery('#Usuario_objeto_custo1').autocomplete({'minLength':'1','source': "<?php echo $this->createUrl('/site/findOC', array('cc'=>'')); ?>"+$("#Usuario_centro_custo1").val()});
	}
	
	$().ready(function(){
		$('#Usuario_centro_custo1').blur(function(){executeAutoCompleteObjetoCusto1()});
	})
</script>