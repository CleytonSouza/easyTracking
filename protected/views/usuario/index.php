<?php
$this->breadcrumbs=array(
	'Usuarios',
);
$this->toolbar=array(
	array('label'=>'Adicionar','url'=>array('create'), 'type'=>'primary',),
);

$this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'usuario-grid',
	'type'=>'striped bordered condensed',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'email',
		'nome',
		'local',
		'ramal',
		array(
			'name'=>'Perfil.nome',
			'header'=>'Perfil',
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); 
