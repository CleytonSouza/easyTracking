<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Modificação de centro de custo';
$this->breadcrumbs=array('Centro de custo');
?>

<h2>Modificação de centro de custo</h2>
<div class="text">
Inclua o seu centro de custo e o seu objeto de custo <br />
</div>
<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'cc-form',
	'inlineErrors'=>false,
    'type'=>'horizontal',
)); ?>
	<?php echo $form->errorSummary($model); ?>
	
    <div class="control-group ">
        <label class="control-label required" for="Usuario_centro_custo">
            Centro de custo
            <span class="required">*</span>
        </label>
        <div class="controls">
            <?php
            $this->widget('zii.widgets.jui.CJuiAutoComplete',array(
                'model'=>$model,
                'attribute'=>'centro_custo',
                'sourceUrl'=>$this->createUrl('/site/findCC'),
				'options'=>array('minLength'=>'1', 'select'=> 'js:function( event, ui ) {executeAutoCompleteObjetoCusto()}'),
                'htmlOptions'=>array('class'=>'span2'),
            ));
            ?>
        </div>
    </div>
	
    <div class="control-group ">
        <label class="control-label required" for="Usuario_objeto_custo">
            Objeto de custo
            <span class="required">*</span>
        </label>
        <div class="controls">
            <?php
            $this->widget('zii.widgets.jui.CJuiAutoComplete',array(
                'model'=>$model,
                'attribute'=>'objeto_custo',
                'sourceUrl'=>'js: "'.$this->createUrl('/site/findOC', array('cc'=>'')).'"+$("#Usuario_centro_custo").val()',
                'options'=>array('minLength'=>'1'),
                'htmlOptions'=>array('class'=>'span2'),
            ));
            ?>
        </div>
    </div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Salvar',
        )); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
	function executeAutoCompleteObjetoCusto(){
		jQuery('#Usuario_objeto_custo').autocomplete({'minLength':'1','source': "<?php echo $this->createUrl('/site/findOC', array('cc'=>'')); ?>"+$("#Usuario_centro_custo").val()});
	}
	
	$().ready(function(){
		$('#Usuario_centro_custo').blur(function(){executeAutoCompleteObjetoCusto()});
	})
</script>