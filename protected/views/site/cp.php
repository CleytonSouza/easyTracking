<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Primeiro acesso';
$this->breadcrumbs=array('Primeiro acesso');
?>


<h2>Modificação de senha</h2>
<div class="text">
Olá, este é o seu primeiro acesso. <br />
Para melhorar a segurança, precisamos que você modifique a sua senha. <br />
</div>
<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'login-form',
	'inlineErrors'=>false,
    'type'=>'horizontal',
)); ?>
	
	
	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->passwordFieldRow($model,'senha'); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Salvar',
        )); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
