<?php
$this->breadcrumbs=array('ModificarMinhaSenha',
); ?> 

<?php
$usuario = Usuario::model()->findByPk(Yii::app()->user->id);
echo '<span id="modUserArea">';
    $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
        'id'=>'dlgUserEditPass',
        // additional javascript options for the dialog plugin
        'options'=>array(
            'title'=>'Modificar minha senha',
            'autoOpen'=>false,
            'width'=>'300px',
            'modal'=>true,
            'buttons' => array
            (
                'Salvar'=>'js:function(){$.post("'.$this->createAbsoluteUrl('/site/userEditPass').'", $("#modEditPassForm").serialize(), function(data, textStatus, jqXHR){if (data == ""){ $("#dlgUserEditPass").dialog("close"); location.reload();} else $("#modUserPassArea").html(data);})}',
                'Cancelar'=>'js:function(){$("#dlgUserEditPass").dialog("close");}',
            ),
        ),
    ));

    echo '<span id="modUserPassArea">';
    $this->renderPartial('/usuario/userEditPass', array('model'=>$usuario));
  echo '</span>';
    $this->endWidget('zii.widgets.jui.CJuiDialog');
?>

    <script type="text/javascript">

        $('a[href$="ModificarMinhaSenha"]').click(function(){
            $("#dlgUserEditPass").dialog("open"); return false;
        });
    </script>