<?php 
$this->breadcrumbs = array('Motoboy');
?>


<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">


  
  </head>

<div id="tabs">
  <ul>
    <li><a href="#tabs-1">Origem</a></li>
    <li><a href="#tabs-2">Destino</a></li>
    <li><a href="#tabs-3">Finalizar Solicitação</a></li>
  </ul>
  
  <form action="motoboy.php" method="POST">
<div id="tabs-1" style="margin-top: 30px">
     
         <legend>Origem do objeto</legend>
             <label>Nome</label> <input type="text" name="d_nome"> <br>
	         <label>Endereço</label> <input type="text" name="d_local"> <br>
	         <label>Número</label> <input type="text" name="d_numero"> <br>
	         <label>Andar</label> <input type="text" name="d_andar"> <br>
	         <label>Baia</label> <input type="text" name="d_baia"> <br>
	         <label>Bairro</label> <input type="text" name="d_bairro"> <br>
	         <label>Cidade</label> <input type="text" name="d_cidade"> <br>
	         <label>UF</label> <input type="text" name="d_uf"> <br>
	         <label>CEP</label> <input type="text" name="d_cep"> <br>
	         <label>Ramal</label> <input type="text" name="d_ramal"> <br>
	         <label>Area</label> <input type="text" name="d_area"> <br>
	
   </div>
   
 <div id="tabs-2" style="margin-top: 30px">
         <legend>Destino do objeto</legend>
             <label>Nome *</label> <input type="text" id="campo_obrigatorio" name="endereco[<?php echo uniqid();?>][nome]" > <br>
			 <label>CEP *</label> <input type="text" id="cep" name="endereco[<?php echo uniqid();?>][cep]" onBlur="getEndereco()"> <br>
	         <label>Endereço *</label> <input type="text" id="endereco" name="endereco[<?php echo uniqid();?>][endereco]"> <br>
	         <label>Número *</label> <input type="text" id="campo_obrigatorio" name="endereco[<?php echo uniqid();?>][numero]"> <br>
	         <label>Andar *</label> <input type="text" id="campo_obrigatorio" name="endereco[<?php echo uniqid();?>][andar]"> <br>
	         <label>Baia *</label> <input type="text" id="campo_obrigatorio" name="endereco[<?php echo uniqid();?>][baia]"> <br>
	         <label>Bairro *</label> <input type="text" id="bairro" name="endereco[<?php echo uniqid();?>][bairro]"> <br>
	         <label>Cidade *</label> <input type="text" id="cidade" name="endereco[<?php echo uniqid();?>][cidade]"> <br>
	         <label>UF *</label> <input type="text" id="uf" name="endereco[<?php echo uniqid();?>][uf]"> <br>
	         <label>Ramal *</label> <input type="text" id="campo_obrigatorio" name="endereco[<?php echo uniqid();?>][ramal]"> <br>
	         <label>Area *</label> <input type="text" id="campo_obrigatorio" name="endereco[<?php echo uniqid();?>][area]"> <br>
		   
				 <div id="barra">
	                 <input type="button" id="addMaisUmCampo" value="+">
	             </div> <br>
	<legend></legend> 
	  

</div> 
	 
	 <div id="tabs-3">
      <div id="mensagem"></div>
      <input id="enviar" name="enviar" type="submit" value="Salvar" class="btn btn-primary" /> 

	 

	 </div>
</div>


     
     </form>
	 
	 
	 </body>
</html>

  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script src="tabs.js"></script>
  <script src="completa_cep.js"></script>
  <script src="clona.js"></script>
  <script src="button.js"></script>
  <script src="submit.js"></script>
	