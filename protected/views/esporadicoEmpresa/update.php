<style>
.window{
    display:none;
    width:300px;
    height:300px;
    position:absolute;
    left:0;
    top:0;
    background:#FFF;
    z-index:9900;
    padding:10px;
    border-radius:10px;
}
 
#mascara{
    display:none;
    position:absolute;
    left:0;
    top:0;
    z-index:9000;
    background-color:#000;
}
 
.fechar{display:block; text-align:right;}
</style>

<script>
$(document).ready(function(){
    $("a[rel=modal]").click( function(ev){
        ev.preventDefault();
 
        var id = $(this).attr("href");
 
        var alturaTela = $(document).height();
        var larguraTela = $(window).width();
     
        //colocando o fundo preto
        $('#mascara').css({'width':larguraTela,'height':alturaTela});
        $('#mascara').fadeIn(1000); 
        $('#mascara').fadeTo("slow",0.8);
 
        var left = ($(window).width() /2) - ( $(id).width() / 2 );
        var top = ($(window).height() / 2) - ( $(id).height() / 2 );
     
        $(id).css({'top':top,'left':left});
        $(id).show();   
    });
 
    $("#mascara").click( function(){
        $(this).hide();
        $(".window").hide();
    });
 
    $('.fechar').click(function(ev){
        ev.preventDefault();
        $("#mascara").hide();
        $(".window").hide();
    });
});
</script>
<?php 
$this->breadcrumbs = array('Modificar Esporádico');
?>

<?php

include 'conexao\conexao.php';
$track_expo = $model->track_expo;
$sql = "SELECT local_entrega, nome, numero, andar, baia, bairro, cidade, uf, cep, ramal, area, email, ds_observacao FROM easytracking.tb_espo_entrega WHERE track_expo = '$track_expo'";
$result = mysql_query($sql, $conecta) or print(mysql_error());
      
?>

<?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>true, // display a larger alert block?
        'fade'=>true, // use transitions?
        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
        'alerts'=>array( // configurations per alert type
            'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
        ),
    )); ?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'crud-form',
	'enableAjaxValidation'=>false,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->label($model,'track_expo'); ?>
<?php echo $form->textField($model,'track_expo',array('class'=>'span2', 'disabled'=>true)); ?>

<?php echo Chtml::label('Coleta', 'Esporadico_d_local') ?>
<?php echo utf8_decode($form->textField($model,'d_local',array('class'=>'span3', 'disabled'=>true))); ?>
<?php echo Chtml::label('Nome', 'Esporadico_d_nome') ?>
<?php echo utf8_decode($form->textField($model,'d_nome',array('class'=>'span3', 'disabled'=>true)))?>
<?php echo Chtml::label('Número', 'Esporadico_d_numero') ?>
<?php echo $form->textField($model,'d_numero',array('class'=>'span1', 'disabled'=>true))?>
<?php echo Chtml::label('Andar', 'Esporadico_d_andar') ?>
<?php echo $form->textField($model,'d_andar',array('class'=>'span1', 'disabled'=>true))?>
<?php echo Chtml::label('CEP', 'Esporadico_d_cep') ?>
<?php echo $form->textField($model,'d_cep',array('class'=>'span2', 'disabled'=>true))?>
<?php echo Chtml::label('Bairro', 'Esporadico_d_bairro') ?>
<?php echo utf8_decode($form->textField($model,'d_bairro',array('class'=>'span2', 'disabled'=>true)))?>
<?php echo Chtml::label('Ramal', 'Esporadico_d_ramal') ?>
<?php echo utf8_decode($form->textField($model,'d_ramal',array('class'=>'span2', 'disabled'=>true)))?>

<br>
<br>

<div>
<?php 
if($result==true){
	$tabela = '<table class="table table-bordered">';
           $tabela .= '<tr class="success>';
           $tabela .= '<td colspan="5">Entrega</tr>';
           $tabela .= '</tr>';
           

	 
	 while($dados = mysql_fetch_array($result))
          {
		  $tabela .= '<tr>';
		  $tabela .= '<td>'.utf8_encode($dados['nome']).'</td>';
          $tabela .= '<td>'.utf8_encode($dados['local_entrega']).'</td>';
          $tabela .= '<td>'.$dados['numero'].'</td>';
          $tabela .= '<td>'.$dados['andar'].'</td>';
          $tabela .= '<td>'.$dados['baia'].'</td>';
          $tabela .= '<td>'.utf8_encode($dados['bairro']).'</td>';
		  $tabela .= '<td>'.utf8_encode($dados['cidade']).'</td>';
		  $tabela .= '<td>'.$dados['uf'].'</td>';
		  $tabela .= '<td>'.$dados['cep'].'</td>';
		  $tabela .= '<td>'.$dados['ramal'].'</td>';
		  $tabela .= '<td>'.utf8_encode($dados['area']).'</td>';
		  $tabela .= '<td>'.$dados['email'].'</td>';
		  $tabela .= '<td>'.utf8_encode($dados['ds_observacao']).'</td>';
          $tabela .= '</tr>';
		  }
		  }
		  else{
		    echo "Nenhum endereço de entrega preenchidoo";
		  }
		  
		  $tabela .= '</table>';
		  echo $tabela;
	  ?>
</div>    
 
 

 
<br>
<br>
<?php echo $form->dropDownListRow($model,'motos', array('SP 01'=>'SP 01', 'SP 02'=>'SP 02', 'SP 03'=>'SP 03', 'SP 04'=>'SP 04', 'SP 05'=>'SP 05', 'Backup 01'=>'Backup 01')); ?>
<br>
<br>
 <input id="enviar" name="enviar" type="submit" value="Salvar" class="btn btn-primary" />



<?php $this->endWidget(); ?>