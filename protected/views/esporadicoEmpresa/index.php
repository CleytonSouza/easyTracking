<?php

$templateView = '{view}{update}';
if (Yii::app()->user->isAdmin) 
	$templateView = '{view}{delete}';
elseif (Yii::app()->user->perms["modificar_esporadico"]) $templateView .= '{update}';
elseif (Yii::app()->user->perms["delete_esporadico"]) $templateView .= '{delete}';
?>

<script type="text/javascript">
var autoReloadGrid = false;

function ReloadGrid() {
    if (autoReloadGrid) $.fn.yiiGridView.update("esporadico-grid");
}

setInterval(function(){ReloadGrid();},10000);

$().ready(function(){
	$('#btnStartRefresh').click(function(){
		$('#btnStartRefresh').hide();
		$('#btnStopRefresh').show();
		autoReloadGrid = true;
		ReloadGrid();
	});

	$('#btnStopRefresh').click(function(){
		$('#btnStartRefresh').show();
		$('#btnStopRefresh').hide();
		autoReloadGrid = false;
	});
});
</script>

<?php
$this->breadcrumbs=array(
	'Motoboy',
);



$this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'esporadico-grid',
	//'type'=>'striped bordered condensed',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'afterAjaxUpdate' => 'reinstallDatePicker',
	'columns'=>array(
		
		array(
			'name'=>'track_expo',
			'type'=>'raw',
			'htmlOptions'=>array('width'=>'110px'),
		),

		
		//'os',
		array(
			'header'=>'Responsável',
			'name'=>'d_nome',
		),
		
		array(
		    'header'=>'Servico',
			'name'=>'servico',
			// 'htmlOptions'=>array('width'=>'110px'),
		),
		 		array(
			'header'=>'Hora da solicitação',
			'name'=>'ts_impressao',
			'value'=>'Converter::formatDateTime($data->ts_impressao)',
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model, 
                'attribute'=>'ts_impressao', 
			), true),
			'htmlOptions'=>array('width'=>'80px'),
		),
		array(
			'header'=>'Status',
			'name'=>'status',
			'htmlOptions'=>array('width'=>'70px'),			
		),
	

		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>$templateView,
			//'visible'=>Yii::app()->user->isAdmin || Yii::app()->user->perms["delete_esporadico"],
            'buttons'=>array(
                'aprovacao' => array(
                    'label'=>'',
                    'url'=>'Yii::app()->controller->createUrl("aprovacao", array("id"=>$data->id))',
                    'options'=>array('class'=>'icon-thumbs-up'),
                ),
            ),
            'htmlOptions'=>array('width'=>'60px'),
		),


	),
));

Yii::app()->clientScript->registerScript('re-install-date-picker', "
function reinstallDatePicker(id, data) {
    $('.CJuiDatePicker').datepicker(jQuery.extend({showMonthAfterYear:false},jQuery.datepicker.regional['pt-BR'],[]));
$('a.view').click(function(){
	jQuery.get($(this).attr('href'), function( data ){
		$('#viewBody').html(data);
		$('#dlgView').dialog('open');
	});
  return false;
});
}
");

Yii::app()->clientScript->registerScript('request-view', "
$('a.view').click(function(){
	jQuery.get($(this).attr('href'), function( data ){
		$('#viewBody').html(data);
		$('#dlgView').dialog('open');
	});
  return false;
});

");

$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'dlgView',
    // additional javascript options for the dialog plugin
    'options'=>array(
        'title'=>'Detalhes do Motoboy',
        'autoOpen'=>false,
		'modal'=>true,
		'width'=>'90%',
		'position'=>'center',
		'resizable'=>false,
		'buttons'=>array(
			array(
				'text'=>'Fechar',
				'click'=> new CJavaScriptExpression('function() { $( this ).dialog( "close" )}'),
			),
		),
    ),
));

echo '<div id="viewBody"> </div>';

$this->endWidget('zii.widgets.jui.CJuiDialog');

?>