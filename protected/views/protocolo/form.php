<script>
function desabilita(){
document.getElementById("enviar").disabled = true; // desabilitar
}
</script>

<input id="enviar" name="enviar" type="submit" value="Salvar" class="btn btn-primary"  onclick="desabilita();"/> 
<br>
<br>

<?php 

$this->breadcrumbs = array('Malote');
?>

<script type="text/javascript">
	function complete_campos(area, values){
		fields = $('#'+area+' input');	
		fields.eq(0).val(values.de);
		fields.eq(1).val(values.para);
		fields.eq(2).val(values.area);
		fields.eq(3).val(values.baia);
		fields.eq(4).val(values.andar);
	}
</script>

<?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>true, // display a larger alert block?
        'fade'=>true, // use transitions?
        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
        'alerts'=>array( // configurations per alert type
            'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
        ),
    )); ?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'crud-form',
	'enableAjaxValidation'=>false,
)); ?>


<?php echo $form->errorSummary($model); ?>

	<?php echo $form->labelex($model,'track'); ?>

	<?php
	$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
		'model'=>$model, 
		'attribute'=>'track',
		'sourceUrl'=>$this->createUrl('getCarta'),
		'options'=>array('minLength'=>'2', 'select'=> 'js:function( event, ui ) {complete_campos("complete", ui.item)}'),
		'htmlOptions'=>array('class'=>'span2'),
	));	
	?>
	<span id="complete">
		<?php echo $form->textFieldRow($model,'d_nome',array('class'=>'span5','maxlength'=>45)); ?>
		<?php echo $form->textFieldRow($model,'p_nome',array('class'=>'span5','maxlength'=>45)); ?>
		<?php echo $form->textFieldRow($model,'p_area',array('class'=>'span5','maxlength'=>45)); ?>
		<?php echo $form->textFieldRow($model,'p_baia',array('class'=>'span1','maxlength'=>45)); ?>
		<?php echo $form->textFieldRow($model,'p_andar',array('class'=>'span2','maxlength'=>45)); ?>
	</span>
	<?php echo $form->textFieldRow($model,'codCorreios',array('class'=>'span5')); ?>
    <?php echo $form->dropDownListRow($model,'transp', Yii::app()->params['protocolo.transportes']); ?>	
	<?php echo $form->textFieldRow($model,'obs',array('class'=>'span5')); ?>
	
<?php $this->endWidget(); ?>


