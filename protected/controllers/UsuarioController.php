<?php

class UsuarioController extends CrudController {
	public $modelClass = 'Usuario';
	
	public function accessRules() {
		return array(
			array('deny', 'users' => array('?')),
			array('allow', 'expression'=>'$user->isAdmin'),
			array('deny', 'users' => array('*')),
		);
	}
}
