<?php

class ProtocoloMobileController extends Controller {
	public $layout = '//layouts/crud';
	
	public function accessRules() {
		return array(
			array('allow', 'expression'=>'$user->isAdmin'),
			array('allow', 'expression'=>'Yii::app()->user->isGuest && Yii::app()->user->perms["criar_lote_mobile"] == 1'),
			array('allow','users' => array('*')),
			array('deny', 'users' => array('*')),
		);
	}
	
	public function loadModel($id) {
		$model = TrackMobile::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	public function actionView($id) {
		$this->renderPartial('view', array(
			'model' => $this->loadModel($id),
		), false, true);
	}
	
		public function actionDelete($id) {
		
		$model = TrackMobile::model()->findByPk($id);
		
				
		include 'conexao\conexao.php';
		
		$id = $model->chave;
		
		$pega_tracking = "SELECT tracking FROM tb_track WHERE chave = '$id'";
		$executa = mysql_query($pega_tracking, $conecta) or die(mysql_error());
		
		$query = mysql_fetch_assoc($executa);
		
		$tracking = $query['tracking'];
		sleep(1);
		
		$sql_delete_track_1 = "DELETE FROM tb_coleta_documentos_usuario WHERE tracking = '$tracking'";
		$result_delete_track = mysql_query($sql_delete_track_1, $conecta) or die(mysql_error());
		sleep(1);
		
		$sql_delete_track = "DELETE FROM tb_track WHERE chave = $id";
		$result_delete = mysql_query($sql_delete_track, $conecta) or die(mysql_error());

	}

	public function filters() {
		return array('accessControl');
	}
	
	public function actionGetCarta($term) {
		$term = '%'.$term.'%';
		$criteria = new CDbCriteria();
		$criteria->condition = 'track_v_v like :term';
		$criteria->params = array(':term'=>$term);
		$criteria->limit = 10;
		
		$models = Movimento::model()->findAll($criteria);
		$data = array();
		if (!empty($models))
			foreach ($models as &$user)
				$data[$user->id] = array(
					'id'    =>$user->track_v_v,
					'label' =>$user->track_v_v,
					'value' =>$user->track_v_v,
					
					'de'    =>$user->d_nome,
					'para'  =>$user->p_nome,
					'area'  =>$user->p_area,
					'baia'  =>$user->p_baia,
					'andar' =>$user->p_andar,
				);

		echo CJSON::encode($data);		
	}	
		
	public function actionCsv(){
		$criteria = Yii::app()->session['protocolo.criteria'];
		if (empty($criteria)) $this->redirect(array('index'));
		$models = Track::model()->findAll($criteria);
		
		$nome_arquivo = date('YmdHis');
		header("Content-type: application/vnd.ms-excel");
		header("Content-type: application/force-download");
		header("Content-Disposition: attachment; filename=$nome_arquivo.csv");
		header("Pragma: no-cache");

	  if (empty($models)) { 
		  echo 'Nenhuma informação para exportar';
		  return;
	  }

	  echo implode(';', array_keys($models[0]->attributes)).PHP_EOL;

	  foreach ($models as $model){
		 echo implode(';', $model->attributes).PHP_EOL; 
	  }		
	}
	
	public function actionIndex() {
		$model = new TrackMobile('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['TrackMobile']))
			$model->attributes = $_GET['TrackMobile'];

		$this->render('index', array('model' => $model));
	}	
	
	public function actionCriar() {
		$model = new ProtocoloMobileForm();
		if (isset($_POST['ProtocoloMobileForm'])) {
			$model->attributes = $_POST['ProtocoloMobileForm'];
			if ($model->validate()) {
				
				   
				
				include 'conexao\conexao.php'; 
								   
				   //Variavel
                    $tracking = $model->etiqueta;
					$id_usuario = 1;
					$d_email = $model->d_email;
					$u_empresa = $model->u_empresa;
                    $remetente = $model->d_nome;
                    $destinatario = $model->p_nome;
                    $depto = $model->p_area;
                    $baia = $model->p_baia;
                    $andar = $model->p_andar;
                    $obs = $model->obs;
                    $correio = $model->codCorreios;
                    $transp = $model->transp;
                    $ts_impressao = date('Y-m-d H:i:s');
                    $tipo = "Impresso";

                                               
                   $result = mysql_query("INSERT INTO easytracking.tb_track (tracking, d_email, u_empresa, remetente, destinatario, depto, baia, andar, obs, correio, transp, ts_impressao, tipo) VALUES 
                   ('".$tracking."', '".$d_email."', '".$u_empresa."', '".$remetente."', '".$destinatario."', '".$depto."', '".$baia."', '".$andar."', '".$obs."', '".$correio."', '".$transp."', '".$ts_impressao."', '".$tipo."')")or die(mysql_error());
 
  
                                         
                Yii::app()->user->setFlash('success', 'Protocolo gerado com sucesso.');
					$model = new ProtocoloMobileForm();
				}
				
				else 
					$model->addError ('', 'Falha na geração do arquivo. Contate o suporte');
			
		}

		$this->render('form', array('model' => $model));
	}
}
