<?php


class CartaController extends Controller{
	public $layout = '//layouts/crud';
	
	public function accessRules() {
		return array(
			array('allow', 'expression'=>'$user->isAdmin'),
			array('allow', 'expression'=>'!Yii::app()->user->isGuest && Yii::app()->user->perms["criar_carta"] == 1'),
			array('deny', 'users' => array('*')),
		);
	}
	
	public function filters() {
		return array('accessControl');
	}
	
	public function actionIndex() {
		$model = new Carta();
		
		include 'conexao\conexao.php';
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Carta'])) {
			$model->attributes = $_POST['Carta'];
			

		
		/*	if (!Yii::app()->user->isAdmin)
				$model->setUsuarioOrigem(); */
			
			if ($model->validate()) {
							
                
				//de
				
				
		
               $tracking = "C".date("YmdHms").rand(1,20);
           
				
				$d_nome = $model->d_nome;
				$d_endereco = $model->d_local;
				$d_andar = $model->d_andar;
				$d_baia = $model->d_baia;
				$d_ramal = $model->d_ramal;
				$d_area = $model->d_area;
				$d_email = $model->d_email;
				$u_empresa = $model->u_empresa;
				
				//para
				
				$p_nome = $model->p_nome;
				$p_endereco = $model->p_local;
				$p_andar = $model->p_andar;
				$p_baia = $model->p_baia;
				$p_ramal = $model->p_ramal;
				$p_area = $model->p_area;
				$p_email = $model->p_email;
				$obs = $model->texto;

				$ts_impressao = date('Y-m-d H:i:s');
				$status = "Impresso";
				
				$sql = "INSERT INTO easytracking.tb_prot_malote (track_carta, u_empresa, d_nome, d_local, d_andar, d_baia, d_ramal, d_area, d_email, obs, p_nome, p_local, p_andar, p_baia, p_ramal, p_email, ts_impressao, status)
                VALUES ('".$tracking."',  '".$u_empresa."', '".$d_nome."', '".$d_endereco."', '".$d_andar."', '".$d_baia."', '".$d_ramal."', '".$d_area."', '".$d_email."', 
                	'".$obs."', '".$p_nome."', '".$p_endereco."', '".$p_andar."', '".$p_baia."', '".$p_ramal."', '".$p_email."', '".$ts_impressao."', '".$status."')";

			   $resultado = mysql_query($sql, $conecta) or die(mysql_error()); 
				
				
				
				
					Yii::app()->user->setFlash('success', 'Carta gerada com sucesso.');
					$model = new Carta();
					//$this->redirect(Yii::app()->params['cartaArquivos'].$filename);
			}	
				else 
					$model->addError ('', 'Falha na geração do arquivo. Contate o suporte');
			
			}
		
		
		$this->render('form', array('model' => $model));
	}
	
	public function actionGetUsuarios($term) {
		$term = '%'.$term.'%';
		$models = Usuario::model()->findAll('email like :term', array(':term'=>$term));
		$data = array();
		if (!empty($models))
			foreach ($models as &$user)
				$data[$user->email] = array(
					'id'    =>$user->email,						
					'label' =>$user->email,						
					'value' =>$user->email,						
					'email' =>$user->email,						
					'nome'  =>$user->nome,
					'local' =>$user->local,
					'andar' =>$user->andar,
					'baia'  =>$user->baia,
					'ramal' =>$user->ramal,
					'area'  =>$user->area,
				);

		echo CJSON::encode($data);		
	}
}
