<?php
class RelatorioEsporadicoEntregaController extends Controller {
	public $layout = '//layouts/crud';
	
public function accessRules() {
		return array(
			array('allow', 'expression'=>'$user->isGuest'),
			array('allow', 'expression'=>'$user->isAdmin'),
			//array('allow', 'actions' => array('index', 'csv', 'view'), 'expression'=>'Yii::app()->user->perms["Relatorio"] == 1'),
			//array('allow', 'actions' => array('criar', 'getCarta'), 'expression'=>'Yii::app()->user->perms["protocolo_criar"] == 1'),
			//array('deny', 'users' => array('*')),
		);
	}
	
	public function loadModel($id) {
		         //Nome tabela.
		$model = RelatorioEsporadicoEntregaController::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	} 
     
	public function actionView($id) {
		$this->renderPartial('view', array(
			'model' => $this->loadModel($id),
		), false, true);
	}
	
	public function filters() {
		return array('accessControl');
	}
 
	
	public function actionIndex() {
		
		$empresa = Yii::app()->user->empresa;
	
	       if (isset($_POST['Enviar'])) {
	
        $ini = $_POST["datainicio"];
        $fim = $_POST["datafinal"];
     

      if ($_SERVER['REQUEST_METHOD'] == 'POST') {
         // $data = Pesquisar();
      if (!empty($data))

           $arquivo = 'MotoBoy.xls';

           // Criamos uma tabela HTML com o formato da planilha para excel
           $tabela = '<table border="32">';
           $tabela .= '<tr>';
           $tabela .= '<td colspan="14">Motoboy Entrega</tr>';
           $tabela .= '<td><b>track_expo</b></td>';
           $tabela .= '<td><b>nome</b></td>';
           $tabela .= '<td><b>local da entrega</b></td>';
           $tabela .= '<td><b>numero</b></td>';
           $tabela .= '<td><b>andar</b></td>';
           $tabela .= '<td><b>baia</b></td>';
           $tabela .= '<td><b>bairro</b></td>';
           $tabela .= '<td><b>cidade</b></td>';
           $tabela .= '<td><b>uf</b></td>';
           $tabela .= '<td><b>cep</b></td>';
           $tabela .= '<td><b>ramal</b></td>';
           $tabela .= '<td><b>area</b></td>';
           $tabela .= '<td><b>email</b></td>';
		   $tabela .= '<td><b>ts_entrega</b></td>';
		   
           $tabela .= '</tr>';

		   
include 'conexao\conexao.php';
 
 $sql = "SELECT * FROM tb_espo_entrega where ts_entrega between '$ini' AND '$fim' AND u_empresa = '$empresa' ORDER BY track_expo, ts_entrega  ASC";
 $results = mysql_query($sql)or die (mysql_error());
   

           $data = array();
   while ($linha = mysql_fetch_array($results)){

    
          $tabela .= '<tr>';
          $tabela .= '<td>'.$linha['track_expo'].'</td>';
          $tabela .= '<td>'.utf8_decode($linha['nome']).'</td>';
          $tabela .= '<td>'.utf8_decode($linha['local_entrega']).'</td>';
          $tabela .= '<td>'.$linha['numero'].'</td>';
          $tabela .= '<td>'.$linha['andar'].'</td>';
          $tabela .= '<td>'.$linha['baia'].'</td>';
          $tabela .= '<td>'.utf8_decode($linha['bairro']).'</td>';
          $tabela .= '<td>'.utf8_decode($linha['cidade']).'</td>';
          $tabela .= '<td>'.$linha['uf'].'</td>';
          $tabela .= '<td>'.$linha['cep'].'</td>';
          $tabela .= '<td>'.$linha['ramal'].'</td>';
          $tabela .= '<td>'.utf8_decode($linha['area']).'</td>';
          $tabela .= '<td>'.$linha['email'].'</td>';
		  $tabela .= '<td>'.$linha['ts_entrega'].'</td>';
          $tabela .= '</tr>';

 }
      $nome_arquivo = date('YmdHis');
     header("Content-type: application/vnd.ms-excel");
     header("Content-type: application/force-download");
     header("Content-Disposition: attachment; filename=$nome_arquivo.xls");
     header("Pragma: no-cache");
     echo $tabela;

     die();
    
   }

  }     
	   $model = new RelatorioEsporadicoEntrega('search');
		$model->unsetAttributes();  
		if (isset($_GET['RelatorioEsporadicoEntrega']))
			$model->attributes = $_GET['RelatorioEsporadicoEntrega'];

	    $this->render('RelatorioEsporadicoEntrega', array('model' => $model));   
     
      }
}
?>