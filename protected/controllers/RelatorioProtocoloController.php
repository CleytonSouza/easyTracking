<?php

class RelatorioProtocoloController extends Controller {
  public $layout = '//layouts/crud';
  
  public function accessRules() {
    return array(
      array('allow', 'expression'=>'$user->isGuest'),
      array('allow', 'expression'=>'$user->isAdmin'),
      //array('allow', 'actions' => array('index', 'csv', 'view'), 'expression'=>'Yii::app()->user->perms["Relatorio"] == 1'),
      //array('allow', 'actions' => array('criar', 'getCarta'), 'expression'=>'Yii::app()->user->perms["protocolo_criar"] == 1'),
      //array('deny', 'users' => array('*')),
      );
  }
  
  public function loadModel($id) {

    $model = RelatorioProtocolo::model()->findByPk($id);
    if ($model === null)
      throw new CHttpException(404, 'The requested page does not exist.');
    return $model;
  }

  public function actionView($chave) {
    $this->renderPartial('view', array(
      'model' => $this->loadModel($id),
      ), false, true);
  }
  
  public function filters() {
    return array('accessControl');
  }


  public function actionRelatorioAvulsodiario(){

   $this->render('RelatorioAvulsodiario');
  }

  public function actionIndex() {

    #$empresa = Yii::app()->user->empresa;

   $model = new RelatorioProtocolo('search');
    $model->unsetAttributes();  // clear any default values
    if (isset($_GET['RelatorioProtocolo']))
      $model->attributes = $_GET['RelatorioProtocolo'];

    $this->render('RelatorioProtocoloform', array('model' => $model));


  }
  

}
?>