<?php

class LocalController extends Controller {
	
	public function actionCep($term){
		$term = preg_replace('#[^0-9]#','',strip_tags($term));
		/* concatenar no php log.tlo_tx tipo  e   log.log_no log, */
		
		$sql1 = "select log.tlo_tx tipo, log.log_no log, bairro_ini.BAI_NO bairro, loc.loc_no cidade, log.CEP cep, log.ufe_sg uf
from vivo_cep_logradouro log
inner join vivo_cep_localidade loc on (loc.LOC_NU = log.LOC_NU)
inner join vivo_cep_bairro bairro_ini on (bairro_ini.BAI_NU = log.BAI_NU_INI)
where log.CEP = '{$term}'";

		$sql2 = "SELECT gu.GRU_ENDERECO log, bai.BAI_NO bairro, loc.LOC_NO cidade, gu.cep cep, gu.UFE_SG uf
FROM vivo.vivo_cep_grande_usuario gu
inner join vivo_cep_bairro bai on (bai.BAI_NU = gu.bai_nu)
inner join vivo_cep_localidade loc on (loc.loc_nu = gu.loc_nu)
where gu.cep = {$term}";

		$list = Yii::app()->db->createCommand($sql1)->query();
		$res=array();
		foreach($list as $item){
			$res = $item; 
			$res['log'] = $item['tipo'].' '.$item['log'];
		}
		
		if (count($res) == 0) {
			$list = Yii::app()->db->createCommand($sql2)->query();
			$res=array();
			foreach($list as $item){
				$res = $item; 
			}
		}
		
		echo CJSON::encode($res);
	}
	
	public function actionIndex($term){
		$cep = $term+0;
        $term = '%'.$term.'%';
		//if (strlen($cep))

        $data = Local::model()->findAll(array(
            'condition'=>'nome like :term or logradouro like :term or bairro like :term or cidade like :term or cep like :term',
            'params'=>array(':term'=>$term),
           // 'distinct' => true,
        ));
        $res = array();

        if (!empty($data))
            foreach ($data as &$model)
                $res[$model->id] = array(
                    'id'          =>$model->id,
                    'label'       =>$model->nome,
                    'value'       =>$model->nome,
                    'nome'        =>$model->nome,
                    'logradouro'  =>$model->logradouro,
                    'numero'      =>$model->numero,
                    'andar'       =>$model->andar,
                    'complemento' =>$model->complemento,
                    'bairro'      =>$model->bairro,
                    'cidade'      =>$model->cidade,
                    'cep'         =>$model->cep,
                    'uf'          =>$model->uf,
                );

        echo CJSON::encode($res);
	}
}

