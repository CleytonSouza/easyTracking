<?php

class AuditSlaController extends Controller {

	private $usuariosCache = null;
	
	public function accessRules() {
		return array(
			array('deny', 'expression'=>'$user->isGuest'),
			array('allow', 'expression'=>'$user->isAdmin'),
			array('allow', 'actions' => array('delete'), 'expression'=>'Yii::app()->user->perms["audit"] == 1'),
			array('allow', 'expression'=>'Yii::app()->user->perms["audit"] == 1'),
			array('deny', 'users' => array('*')),
		);
	}
	
	public function filters() {
		return array('accessControl', 'postOnly + delete');
	}


	public function actionIndex() {
		$model = new AuditSla('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['AuditSla']))
			$model->attributes = $_GET['AuditSla'];
		$this->render('index', array('model' => $model));

        #$this->render('index');
	}	
}
// numero da carta tem no máximo 15
