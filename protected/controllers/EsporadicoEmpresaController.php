<?php

class EsporadicoEmpresaController extends Controller {
	

	#Yii::import('ext.yii-mail.*');
	private $usuariosCache = null;

	public function accessRules() {
		return array(
			array('deny', 'expression'=>'$user->isGuest'),
			array('allow', 'expression'=>'$user->isAdmin || $user->isTransp'),
			array('allow', 'actions' => array('index', 'view'), 'expression'=>'Yii::app()->user->perms["esporadico"] == 1'),
			array('allow', 'actions' => array('update'), 'expression'=>'Yii::app()->user->perms["modificar_esporadico"] == 1'),
			array('allow','users' => array('*')),
			array('deny', 'users' => array('*')),
			);
	}
	
	public function filters() {
		return array('accessControl');
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id) {
		$this->renderPartial('view', array(
			'model' => $this->loadModel($id),
			), false, true);
	}

    /**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
    public function actionCsv(){
    	$criteria = Yii::app()->session['espo.criteria'];
    	if (empty($criteria)) $this->redirect(array('index'));
    	$models = Esporadico::model()->findAll($criteria);

    	$nome_arquivo = date('YmdHis');
    	header("Content-type: application/vnd.ms-excel");
    	header("Content-type: application/force-download");
    	header("Content-Disposition: attachment; filename=$nome_arquivo.csv");
    	header("Pragma: no-cache");

    	if (empty($models)) { 
    		echo 'Nenhuma informação para exportar';
    		return;
    	}

    	echo implode(';', array_keys($models[0]->attributes)).PHP_EOL;

    	foreach ($models as $model){
    		echo implode(';', $model->attributes).PHP_EOL; 
    	}		
    }


/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id) {

		$this->layout = '//layouts/crud';
		$model = $this->loadModel($id);
		$model->scenario = 'update';
		
		if (isset($_POST['EsporadicoEmpresa'])) {
			
			$model->attributes = $_POST['EsporadicoEmpresa'];
			$track_expo = $model->track_expo;
			$motos = $model->motos;

           	include 'conexao\conexao.php';
			$sqlcoleta = "SELECT track_expo FROM easytracking.tb_endereco_coleta_externo WHERE track_expo = '$track_expo'";
			$resultado = mysql_query($sqlcoleta, $conecta) or die(mysql_error());
			$track = mysql_fetch_array($resultado);			
 
            if($track_expo === $track['0']){

                $track_expo = $model->track_expo;
				$motos = $model->motos;
                 
                 sleep(1);
				include 'conexao\conexao.php';
                         #Pega o usuario que ira executar o serviço
				$sql_usuario = "SELECT id FROM easytracking.tb_usuario WHERE nome = '$motos'";
				$result_usuario = mysql_query($sql_usuario, $conecta) or die(mysql_error());
				$id_usuario = mysql_fetch_array($result_usuario);
				mysql_close($conecta);
                
                sleep(1);
				include 'conexao\conexao.php';
				$status1 = "UPDATE `easytracking`.`tb_endereco_coleta_externo` SET fl_pendente_coleta = 'S', id_usuario = '$id_usuario[0]'
				WHERE `tb_endereco_coleta_externo`.`track_expo` = '$track_expo'";
				$atualiza_status = mysql_query($status1, $conecta) or die(mysql_error());
				mysql_close($conecta);

                sleep(1);
				include 'conexao\conexao.php';
				$status3 = "UPDATE `easytracking`.`tb_endereco_entrega_externo` SET `fl_pendente_entrega` = 'S', `id_usuario` = '$id_usuario[0]' 
				WHERE `tb_endereco_entrega_externo`.`track_expo` = '$track_expo'";
				$atualiza_status3 = mysql_query($status3, $conecta) or die(mysql_error());
				mysql_close($conecta);

				sleep(1);
				include 'conexao\conexao.php';
				$status4 = "DELETE  FROM `easytracking`.`tb_status_enderecos_entrega_externo` WHERE track_expo = '$track_expo'";
				$atualiza_status4 = mysql_query($status4, $conecta) or die(mysql_error());
				mysql_close($conecta);

                sleep(1);
				include 'conexao\conexao.php';
				$status2 = "UPDATE `easytracking`.`tb_espo` SET `status` = 'Em transporte' 
				WHERE `tb_espo`.`track_expo` = '$track_expo'";
				$atualiza_status1 = mysql_query($status2, $conecta) or die(mysql_error());
				mysql_close($conecta);            

      			$this->redirect(array('index'));

                        } #if $track === $track
                         else {
                               
			$sql_usuario = "SELECT id FROM easytracking.tb_usuario WHERE nome = '$motos'";
			$result_usuario = mysql_query($sql_usuario, $conecta) or die(mysql_error());
			$id_usuario = mysql_fetch_array($result_usuario);
			mysql_close($conecta);
			
	        #____  Inserir na tabela  tb_endereço_coleta_externo ______#
			include 'conexao\conexao.php';
			$sql_coleta = "SELECT n_pedido, d_nome, d_local, d_numero, d_andar, d_baia, d_bairro, d_cidade, d_uf, d_cep, d_ramal, d_area, d_email, obs FROM easytracking.tb_espo WHERE track_expo = '$track_expo'";
			$result_coleta = mysql_query($sql_coleta, $conecta) or die(mysql_error());
			
			while($dados_coleta = mysql_fetch_array($result_coleta))
			{
				$npedido_coleta = $dados_coleta["n_pedido"];
				$nome_coleta = $dados_coleta["d_nome"];
				$local_coleta = $dados_coleta["d_local"];
				$numero_coleta = $dados_coleta["d_numero"];
				#$andar_coleta = NULL;
				$baia_coleta = $dados_coleta["d_baia"];
				$bairro_coleta = $dados_coleta["d_bairro"];
				$cidade_coleta = $dados_coleta["d_cidade"];
				$uf_coleta = $dados_coleta["d_uf"];
				$cep_coleta = $dados_coleta["d_cep"];
				$ramal_coleta = $dados_coleta["d_ramal"];
				$area_coleta = $dados_coleta["d_area"];
				if(isset($dados_coleta["email"])){					 
					$email = $dados_coleta["email"];
				}
				else{
					$email = "NULL";
				}
				$obs = $dados_coleta["obs"];

				$sql_endereco_coleta = "INSERT INTO easytracking.tb_endereco_coleta_externo (track_expo, id_usuario, nome, local_coleta, numero, andar, baia, bairro, cidade, uf, cep, ramal, area, email, ds_observacao)
				VALUES ('".$track_expo."', '".$id_usuario['0']."', '".$nome_coleta."', '".$local_coleta."', '".$numero_coleta."', NULL, '".$baia_coleta."', '".$bairro_coleta."', '".$cidade_coleta."', 
				'".$uf_coleta."', '".$cep_coleta."', '".$ramal_coleta."', '".$area_coleta."', '".$email."', '".$npedido_coleta."')";

				$result_endereco_coleta = mysql_query($sql_endereco_coleta, $conecta) or die(mysql_error()); 
				mysql_close($conecta);

			}	
			include 'conexao\conexao.php';
			$sql_entrega = "SELECT a.nome, a.local_entrega, a.numero, a.andar, a.baia, a.bairro, 
                                   a.cidade, a.uf, a.cep, a.ramal, a.area, a.email, a.ds_observacao, b.n_pedido 
                                   FROM easytracking.tb_espo_entrega as a
                                   LEFT JOIN easytracking.tb_espo as b
                                   ON a.track_expo = b.track_expo
                                   WHERE a.track_expo = '$track_expo'";
			$result_entrega = mysql_query($sql_entrega, $conecta) or die(mysql_error());
			while($dados_entrega = mysql_fetch_array($result_entrega))
			{
				$npedido = $dados_entrega["n_pedido"];
				$nome_entrega = $dados_entrega["nome"];
				$local_entrega = $dados_entrega["local_entrega"];
				$numero_entrega = $dados_entrega["numero"];         
	  		    #$andar_entrega = NULL;
                $baia_entrega = $dados_entrega["baia"];
    			$bairro_entrega = $dados_entrega["bairro"];
				$cidade_entrega = $dados_entrega["cidade"];
				$uf_entrega = $dados_entrega["uf"];
				$cep_entrega = $dados_entrega["cep"];
				$ramal_entrega = $dados_entrega["ramal"];
				$area_entrega = $dados_entrega["area"];
				$obs = $dados_entrega["ds_observacao"];
				$email_entrega = "";

				$sql_endereco_entrega = "INSERT INTO easytracking.tb_endereco_entrega_externo (track_expo, id_usuario, nome, local_entrega, numero, andar, baia, bairro, cidade, uf, cep, ramal, area, email, ds_observacao)
				VALUES ('".$track_expo."', '".$id_usuario['0']."', '".$nome_entrega."', '".$local_entrega."', '".$numero_entrega."', NULL, '".$baia_entrega."', '".$bairro_entrega."', '".$cidade_entrega."', 
				'".$uf_entrega."', '".$cep_entrega."', '".$ramal_entrega."', '".$area_entrega."', '".$email_entrega."', '".$npedido."')";
				$result_endereco_entrega = mysql_query($sql_endereco_entrega, $conecta) or die(mysql_error()); 
			
			   $status = "UPDATE `easytracking`.`tb_espo` SET `status` = 'Em transporte' WHERE `tb_espo`.`track_expo` = '$track_expo'";
			  $atualiza_status = mysql_query($status, $conecta) or die(mysql_error());
			}			  
			  mysql_close($conecta);
				 #___ Fim ____#	 
              $this->redirect(array('index')); 

             }#else

			$this->redirect(array('index')); 
		} #if isset

		$this->render('update', array('model' => $model));
	}


	public function actionAprovacao($id){
		$this->layout = '//layouts/crud';
		$model = $this->loadModel($id);
		$model->scenario = 'update';

		if (isset($_POST['Esporadico'])) {
			$model->attributes = $_POST['Esporadico'];
			if ($model->save())
				HttpRequest::Post(Yii::app()->params['atualizaMecomo'], $model->attributes);
			$this->redirect(array('index'));
		}

		$this->render('aprovacao', array('model' => $model));
	}
	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
		$model = $this->loadModel($id);
		HttpRequest::Post(Yii::app()->params['deleteEsporadico'], $model->attributes);
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex() {
		$model = new EsporadicoEmpresa('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['EsporadicoEmpresa']))
			$model->attributes = $_GET['EsporadicoEmpresa'];


		$this->render('index', array('model' => $model));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id) {
		$model = EsporadicoEmpresa::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model) {
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'esporadico-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
/*
	public function actionCriar() {
	    
		$this->layout = '//layouts/crud';
		$model = new EsporadicoEmpresaForm();

		if (isset($_POST['EsporadicoEmpresaForm'])) {
			$model->attributes = $_POST['EsporadicoEmpresaForm'];
	
			
			if (!Yii::app()->user->isAdmin)
				$model->setUsuarioOrigem();
			
			if ($model->validate()) {
				$user = Usuario::model()->findByPk(Yii::app()->user->id);
				$data = HttpRequest::Post(Yii::app()->params['esporadicoAddr'], array_merge(array('centro_custo'=>$user->centro_custo), $model->attributes));
				$reg = explode("\r\n", $data);
			
			       echo $model->d_email;
				   die();

            $model = new EsporadicoEmpresaForm();
        
					
					
				}
				else 
					$model->addError ('', 'Falha na geração do arquivo. Contate o suporte');
			}
		
		
		$this->render('form', array('model' => $model));
	}
*/	
	public function actionGetUsuarios($term) {
		$term = '%'.$term.'%';
		$models = Usuario::model()->findAll('email like :term', array(':term'=>$term));
		$data = array();
		if (!empty($models))
			foreach ($models as &$user)
				$data[$user->email] = array(
					//'id'    =>$user->email,
					//'label' =>$user->email,
					//'value' =>$user->email,
					//'email' =>$user->email,
					//'nome'  =>$user->nome,
					//'local' =>$user->local,
					//'andar' =>$user->andar,
					//'baia'  =>$user->baia,
					//'ramal' =>$user->ramal,
					//'area'  =>$user->area,
					//'ccusto'=>$user->centro_custo,
					//'nome_local'=>$user->nome_local,
					//'numero'=>$user->nunero,
					//'bairro'=>$user->bairro,
					//'cidade'=>$user->cidade,
					//'uf'=>$user->uf,
					//'cep'=>$user->cep,
					'empresa'=>$user->empresa,
					
					);

			echo CJSON::encode($data);		
		}	
	}
