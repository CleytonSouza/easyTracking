<?php

class MovimentoController extends Controller {

	private $usuariosCache = null;
	
	public function accessRules() {
		return array(
			array('deny', 'expression'=>'$user->isGuest'),
			array('allow', 'expression'=>'$user->isAdmin'),
			array('allow', 'actions' => array('delete'), 'expression'=>'Yii::app()->user->perms["delete_movimento"] == 1'),
			array('allow', 'expression'=>'Yii::app()->user->perms["movimento"] == 1'),
			array('deny', 'users' => array('*')),
		);
	}
	
	public function filters() {
		return array('accessControl', 'postOnly + delete');
	}

	public function actionDelete($id) {
		
		$model = Movimento::model()->findByPk($id);
		
				
		include 'conexao\conexao.php';
		
		$id = $model->id;
		
		$sql_delete = "DELETE FROM tb_prot_malote WHERE id = $id";
		$result_delete = mysql_query($sql_delete, $conecta) or die(mysql_error());

	}
			
	public function actionGetUsuarios($term) {
		if (!empty($this->usuariosCache)) return $this->usuariosCache;
		
		$term = '%'.$term.'%';
		
		/* Pegando nomes de usuários "DE" */
		$modelUserList = Movimento::model()->findAll(array(
			'select' => 'd_email, d_nome, d_local, d_andar, d_baia, d_ramal, d_area',
			'condition'=>'d_email like :term',
			'params'=>array(':term'=>$term),
			'distinct' => true,
		));

		if (!empty($modelUserList))
			foreach ($modelUserList as &$user)
				$this->usuariosCache[$user->d_email] = array(
					'id'    =>$user->d_email,						
					'label' =>$user->d_email,						
					'value' =>$user->d_email,						
					'email' =>$user->d_email,						
					'nome'  =>$user->d_nome,
					'local' =>$user->d_local,
					'andar' =>$user->d_andar,
					'baia'  =>$user->d_baia,
					'ramal' =>$user->d_ramal,
					'area'  =>$user->d_area,						
				);

		/* Pegando nomes de usuários "PARA" */
		$modelUserList = Movimento::model()->findAll(array(
			'select' => 'p_email, p_nome, p_local, p_andar, p_baia, p_ramal, p_area',
			'condition'=>'p_email like :term',
			'params'=>array(':term'=>$term),
			'distinct' => true,
		));

		if (!empty($modelUserList))
			foreach ($modelUserList as &$user)
				$this->usuariosCache[$user->p_email] = array(
					'id'    =>$user->p_email,						
					'label' =>$user->p_email,						
					'value' =>$user->p_email,						
					'email' =>$user->p_email,						
					'nome'  =>$user->p_nome,
					'local' =>$user->p_local,
					'andar' =>$user->p_andar,
					'baia'  =>$user->p_baia,
					'ramal' =>$user->p_ramal,
					'area'  =>$user->p_area,						
				);

		echo CJSON::encode($this->usuariosCache);
	}
	
	public function actionIndex() {
		$model = new Movimento('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Movimento']))
			$model->attributes = $_GET['Movimento'];

		$this->render('index', array('model' => $model));
	}	
}
// numero da carta tem no máximo 15
