<?php

class EsporadicoController extends Controller {
	
	private $usuariosCache = null;

	public function accessRules() {
		return array(
			array('deny', 'expression'=>'$user->isGuest'),
			array('allow', 'expression'=>'$user->isAdmin || $user->isTransp'),
			array('allow', 'actions' => array('criar', 'getUsuarios'), 'expression'=>'Yii::app()->user->perms["criar_esporadico"] == 1'),
			array('allow', 'actions' => array('index', 'view'), 'expression'=>'Yii::app()->user->perms["esporadico"] == 1'),
			array('allow', 'actions' => array('update'), 'expression'=>'Yii::app()->user->perms["modificar_esporadico"] == 1'),
			array('allow', 'actions' => array('delete'), 'expression'=>'Yii::app()->user->perms["delete_esporadico"] == 1'),
			array('allow', 'actions' => array('aprovacao'), 'expression'=>'Yii::app()->user->perms["aprova_esporadico"] == 1'),
			array('allow','users' => array('*')),
			array('deny', 'users' => array('*')),
			);
	}
	
	public function filters() {
		return array('accessControl');
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id) {
		$this->renderPartial('view', array(
			'model' => $this->loadModel($id),
			), false, true);
	}
	
    /**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
    public function actionCsv(){
    	$criteria = Yii::app()->session['espo.criteria'];
    	if (empty($criteria)) $this->redirect(array('index'));
    	$models = Esporadico::model()->findAll($criteria);
    	
    	$nome_arquivo = date('YmdHis');
    	header("Content-type: application/vnd.ms-excel");
    	header("Content-type: application/force-download");
    	header("Content-Disposition: attachment; filename=$nome_arquivo.csv");
    	header("Pragma: no-cache");

    	if (empty($models)) { 
    		echo 'Nenhuma informação para exportar';
    		return;
    	}

    	echo implode(';', array_keys($models[0]->attributes)).PHP_EOL;

    	foreach ($models as $model){
    		echo implode(';', $model->attributes).PHP_EOL; 
    	}		
    }


	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id) {

		$this->layout = '//layouts/crud';
		$model = $this->loadModel($id);
		$model->scenario = 'update';
		
		if (isset($_POST['Esporadico'])) {
			$model->attributes = $_POST['Esporadico'];
			$track_expo = $model->track_expo;
			$empresa = $model->empresa;
			
			include 'conexao\conexao.php';
			
			$sql_espo = "UPDATE easytracking.tb_espo SET empresa = '$empresa', status = 'Transportadora' WHERE track_expo = '$track_expo'";
			$result_espo = mysql_query($sql_espo, $conecta) or die(mysql_error());
			sleep(2);
			mysql_close($conecta);
			$this->redirect(array('index')); 

		}


		$this->render('update', array('model' => $model));
	}

	
	public function actionAprovacao($id){
		$this->layout = '//layouts/crud';
		$model = $this->loadModel($id);
		$model->scenario = 'update';

		if (isset($_POST['Esporadico'])) {
			$model->attributes = $_POST['Esporadico'];
			if ($model->save())
				HttpRequest::Post(Yii::app()->params['atualizaMecomo'], $model->attributes);
			$this->redirect(array('index'));
		}

		$this->render('aprovacao', array('model' => $model));
	}
	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
		$model = $this->loadModel($id);
		HttpRequest::Post(Yii::app()->params['deleteEsporadico'], $model->attributes);
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex() {
		$model = new Esporadico('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Esporadico']))
			$model->attributes = $_GET['Esporadico'];


		$this->render('index', array('model' => $model));
	}

       	/**
	 *Ocorrencia (Pag(Principal))
	 */
       	public function actionOcorrenciaColeta() {



       		$this->render('ocorrenciaColeta');
       	}

     	/**
	 *Ocorrencia (Pag(Principal))
	 */
     	public function actionOcorrenciaEntrega() {



     		$this->render('ocorrenciaEntrega');
     	}




	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id) {
		$model = Esporadico::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model) {
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'esporadico-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionCriar() {
		
		$this->layout = '//layouts/crud';
		$model = new EsporadicoForm();

		if (isset($_POST['EsporadicoForm'])) {
			$model->attributes = $_POST['EsporadicoForm'];
			
			
			if (!Yii::app()->user->isAdmin)
				$model->setUsuarioOrigem();
			
			if ($model->validate()) {
				$data = HttpRequest::Post(Yii::app()->params['esporadicoAddr'], array_merge(array('centro_custo'=>$user->centro_custo), $model->attributes));
				$reg = explode("\r\n", $data);
				if ($reg[0] == 'HTTP/1.1 200 200 OK' && $reg[2] == 'Content-Type: application/pdf') {
					$filename = substr($reg[4], 39, -1);
					$link = Yii::app()->params['esporadicoArquivos'].$filename;
					
				}
				Yii::app()->user->setFlash('success', 'Esporadico gerado com sucesso.<strong></strong>.');
				
				$model = new EsporadicoForm();
				
				
				
			}
			else 
				$model->addError ('', 'Falha na geração do arquivo. Contate o suporte');
		}
		
		
		$this->render('form', array('model' => $model));
	}
	

	
	public function actionGetUsuarios($term) {
		$term = '%'.$term.'%';
		$models = Usuario::model()->findAll('email like :term', array(':term'=>$term));
		$data = array();
		if (!empty($models))
			foreach ($models as &$user)
				$data[$user->email] = array(
					//'id'    =>$user->email,
					//'label' =>$user->email,
					//'value' =>$user->email,
					//'email' =>$user->email,
					//'nome'  =>$user->nome,
					//'local' =>$user->local,
					//'andar' =>$user->andar,
					//'baia'  =>$user->baia,
					//'ramal' =>$user->ramal,
					//'area'  =>$user->area,
					//'ccusto'=>$user->centro_custo,
					//'nome_local'=>$user->nome_local,
					//'numero'=>$user->nunero,
					//'bairro'=>$user->bairro,
					//'cidade'=>$user->cidade,
					//'uf'=>$user->uf,
					//'cep'=>$user->cep,
					
					);

			echo CJSON::encode($data);		
		}	
	}
