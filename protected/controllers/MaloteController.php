<?php

class MaloteController extends Controller {
	
   private $usuariosCache = null;

	public $layout = '//layouts/crud';

	
	public function accessRules() {
		return array(
			array('deny', 'expression'=>'$user->isGuest'),
			array('allow', 'expression'=>'$user->isAdmin'),
			array('allow', 'expression'=>'Yii::app()->user->perms["criar_malote"] == 1'),
			array('deny', 'users' => array('*')),
		);
	}
	
	public function filters() {
		return array('accessControl');
	}
		
	public function actionIndex() {
		$model = new Malote();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Malote'])) {
			$model->attributes = $_POST['Malote'];
			if (!isset($_POST['carta']) || count($_POST['carta']) <= 0)
				$model->addError ('', 'Nenhuma carta encontrada');
			else
				if ($model->validate()) {

					include 'conexao\conexao.php'; 

				    $tracking = "M".date('YmdHis').rand(1,20);
				    $de = $model->de;
				    $para = $model->para;
				    $lacre = $model->lacre;
                    $andar = $model->p_andar;
                    $transp = $model->servico;
                    $status = "Malote";
                    $ts_malote = date('Y-m-d H:i:s');

					$sendData = array('cartas'=>implode(',',$_POST['carta']));
					//$sendData = array_merge($sendData, $model->attributes);
					unset($sendData['tracks']);


					$tracks = array('track_carta'=>explode(',',$sendData['cartas']));
                      
                      foreach($tracks['track_carta'] as $track){
						$sql = "UPDATE tb_prot_malote SET track_malote = '$tracking', malote = '$lacre', p_andar = '$andar', servico = '$transp', status = '$status', ts_malote = '$ts_malote' WHERE track_carta = '$track'";
						$update = mysql_query($sql, $conecta) or die(mysql_error());
				}		

                        define('MPDF_PATH', 'pdf/');
                        include(MPDF_PATH.'mpdf.php');
                        $mpdf=new mPDF();

						
						$conteudo_pdf = '<!DOCTYPE html>';
						$conteudo_pdf .= '<body>';
						$conteudo_pdf .= '<p style="text-align:center; font-weight:bold; padding-top:5mm;">Malote</p>';
						$conteudo_pdf .= '<b>De: </b> ' . $de. '<br />'. '<br />'; 
						$conteudo_pdf .= '<b>Para: </b>' . $para. '<br />'. '<br />'; 
                        $conteudo_pdf .= '<b>Cartas: </b>' .$sendData['cartas'] .'<br />'; 
						$conteudo_pdf .= '</body>';
						$conteudo_pdf .= '</html>';
						
						
                        $mpdf->WriteHTML($conteudo_pdf);
					   
						
						$mpdf->Output('documentos/malote/'.$tracking.'.pdf','F');
						
						$link = '/pitneybowes/documentos/malote/'.$tracking.'.pdf';
                    
						Yii::app()->user->setFlash('success', 'Malote gerado com sucesso.<strong><a href="'.$link.'">'.$tracking.'</a></strong>');

						$model = new Malote();
					}
					
					else 
						$model->addError ('', 'Falha na geração do arquivo. Contate o suporte');
				
		}

		$this->render('form', array('model' => $model));
	}

	public function actionGetUsuarios($term) {
		$term = '%'.$term.'%';
		$models = Usuario::model()->findAll('nome like :term', array(':term'=>$term));
		$data = array();
		if (!empty($models))
			foreach ($models as &$user)
				$data[$user->nome] = array(
					'id'    =>$user->nome,						
					'label' =>$user->nome,						
					'value' =>$user->nome,						
					'email' =>$user->email,						
					'nome'  =>$user->nome,
					'local' =>$user->local,
					'andar' =>$user->andar,
					'baia'  =>$user->baia,
					'ramal' =>$user->ramal,
					'area'  =>$user->area,
				);

		echo CJSON::encode($data);		
	}
}
