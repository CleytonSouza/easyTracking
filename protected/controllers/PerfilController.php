<?php

class PerfilController extends CrudController {
	public $modelClass = 'Perfil';

	public function accessRules() {
		return array(
			array('deny', 'expression'=>'$user->isGuest'),
			array('allow', 'expression'=>'$user->isAdmin'),
			array('deny', 'users' => array('*')),
		);
	}
}
