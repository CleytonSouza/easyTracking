<?php

class LoteController extends Controller {
	public $layout = '//layouts/crud';
	
	public function accessRules() {
		return array(
			array('allow', 'expression'=>'$user->isAdmin'),
			array('allow', 'expression'=>'Yii::app()->user->isGuest && Yii::app()->user->perms["criar_lote_mobile"] == 1'),
			array('allow','users' => array('*')),
			array('deny', 'users' => array('*')),
		);
	}
	
	public function filters() {
		return array('accessControl');
	}
		
	public function actionIndex() {
		$model = new Lote();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Lote'])) {
			$model->attributes = $_POST['Lote'];
                        if (!isset($_POST['carta']) || count($_POST['carta']) <= 0){
							$model->addError ('', 'O campo código de barras não foi preenchido');
						}
                        
						if (isset($_POST['carta'])){
			
				if ($model->validate()) {
					
					$sendData = array('cartas'=>implode(',',$_POST['carta']));
					$sendData = array_merge($sendData, $model->attributes);
					$data = HttpRequest::Post(Yii::app()->params['loteAddr'], $_POST['carta']);
					$reg = explode("\r\n", $data);
					//unset($sendData['tracks']);
					
					include 'conexao\conexao.php'; 
								   
				   //Variavel
                    $tracking = "L".date('YmdHis').rand(1,20);
                    $remetente = utf8_encode($model->de);
                    $destinatario = utf8_encode($model->para);
					$local_origem = utf8_encode($model->local_origem);
                    $local_destino = utf8_encode($model->local_destino);
                    $obs = utf8_encode($model->obs);
                    $transp = utf8_encode($model->transporte);
                    $data_chegada = $model->data_chegada;
					$d_email = $model->d_email;
					$u_empresa = $model->u_empresa;

if(isset($data_chegada)){
	  $chegada = $data_chegada;
}

else{
	$chegada = NULL;
}

					
                    $ts_impressao = date('Y-m-d H:i:s');
                    $tipo = "Impresso";
                    $tracks = utf8_encode($sendData['cartas']);
					$lote = "Lote";


					
					
                                               
                   $result = mysql_query("INSERT INTO easytracking.tb_track (tracking, d_email, u_empresa, track_cartas, local_origem, local_destino, data_chegada, remetente, destinatario, obs, transp, ts_impressao, tipo, lote) VALUES 
                   ('".$tracking."', '".$d_email."', '".$u_empresa."', '".$tracks."', '".$local_origem."', '".$local_destino."', '".$chegada."',  '".$remetente."', 
                   '".$destinatario."', '".$obs."', '".$transp."', '".$ts_impressao."', '".$tipo."', '".$lote."')")or die(mysql_error());

				                 
                Yii::app()->user->setFlash('success', 'Protocolo gerado com sucesso.<strong><a href="http://easypb.pitneybowes.com.br/pitneybowes/gera_codigo_barras/codigo_barras.php?tracking='.$tracking.'">Clique aqui para imprimir o código de barras</a></strong>.');
				$model = new Lote();
				}	
					else 
						$model->addError ('', 'Falha na geração do arquivo. Contate o suporte');
                                
		}
                }
		$this->render('form', array('model' => $model));
	}
}