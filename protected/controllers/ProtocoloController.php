<?php

class ProtocoloController extends Controller {
	public $layout = '//layouts/crud';
	
	public function accessRules() {
		return array(
			array('allow', 'expression'=>'$user->isAdmin'),
			array('allow', 'expression'=>'Yii::app()->user->isGuest && Yii::app()->user->perms["criar_lote_mobile"] == 1'),
			array('allow','users' => array('*')),
			array('deny', 'users' => array('*')),
		);
	}
	
	public function loadModel($id) {
		$model = Track::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	public function actionView($id) {
		$this->renderPartial('view', array(
			'model' => $this->loadModel($id),
		), false, true);
	}

	public function filters() {
		return array('accessControl');
	}
	
	public function actionGetCarta($term) {
		$term = '%'.$term.'%';
		$criteria = new CDbCriteria();
		$criteria->condition = 'track_carta like :term';
		$criteria->params = array(':term'=>$term);
		$criteria->limit = 10;
		
		$models = Movimento::model()->findAll($criteria);
		$data = array();
		if (!empty($models))
			foreach ($models as &$user)
				$data[$user->id] = array(
					'id'    =>$user->track_carta,
					'label' =>$user->track_carta,
					'value' =>$user->track_carta,
					
					'de'    =>$user->d_nome,
					'para'  =>$user->p_nome,
					'area'  =>$user->p_area,
					'baia'  =>$user->p_baia,
					'andar' =>$user->p_andar,
				);

		echo CJSON::encode($data);		
	}	
		
	public function actionCsv(){
		$criteria = Yii::app()->session['protocolo.criteria'];
		if (empty($criteria)) $this->redirect(array('index'));
		$models = Track::model()->findAll($criteria);
		
		$nome_arquivo = date('YmdHis');
		header("Content-type: application/vnd.ms-excel");
		header("Content-type: application/force-download");
		header("Content-Disposition: attachment; filename=$nome_arquivo.csv");
		header("Pragma: no-cache");

	  if (empty($models)) { 
		  echo 'Nenhuma informação para exportar';
		  return;
	  }

	  echo implode(';', array_keys($models[0]->attributes)).PHP_EOL;

	  foreach ($models as $model){
		 echo implode(';', $model->attributes).PHP_EOL; 
	  }		
	}
	
	public function actionIndex() {
		$model = new Track('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Track']))
			$model->attributes = $_GET['Track'];

		$this->render('index', array('model' => $model));
	}	
	
public function actionCriar() {
  
 
        $model = new ProtocoloForm();
		if (isset($_POST['ProtocoloForm'])) {
			$model->attributes = $_POST['ProtocoloForm'];
			if ($model->validate()) {
			
			include 'conexao\conexao.php'; 
								   
				   //Variavel
                    $tracking = date('YmdHis').rand(1,20);
					$d_email = $model->d_email;
					$u_empresa = $model->u_empresa;
                    $remetente = $model->d_nome;
                    $destinatario = $model->p_nome;
                    $depto = $model->p_area;
                    $baia = $model->p_baia;
                    $andar = $model->p_andar;
                    $obs = $model->obs;
                    $correio = $model->codCorreios;
                    $transp = $model->transp;
                    $ts_impressao = date('Y-m-d H:i:s');
                    $tipo = "Impresso";

                                               
                   $result = mysql_query("INSERT INTO easytracking.tb_track (tracking, remetente, d_email, destinatario, depto, baia, andar, obs, correio, transp, ts_impressao, tipo, u_empresa) VALUES 
                   ('".$tracking."', '".$remetente."', '".$d_email."', '".$destinatario."', '".$depto."', '".$baia."', '".$andar."', '".$obs."', '".$correio."', '".$transp."', '".$ts_impressao."', '".$tipo."', '".$u_empresa."')")or die(mysql_error());

                                         
                Yii::app()->user->setFlash('success', 'Protocolo gerado com sucesso.<strong><a href="http://easypb.pitneybowes.com.br/pitneybowes/gera_codigo_barras/codigo_barras.php?tracking='.$tracking.'">Clique aqui para imprimir o código de barras</a></strong>.');
                  $model = new ProtocoloForm();

                 
				}
			}	
		$this->render('form', array('model' => $model));
		}
}
