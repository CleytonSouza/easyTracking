<?php

Yii::import('ext.yii-mail.*');

class SiteController extends Controller {

	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}

	/* Change Password */
	public function actionCp() {
		$user = Usuario::model()->findByPk(Yii::app()->user->id);
		if ($user->first <> 0) $this->redirect(array('/'));
		
		if (isset($_POST['Usuario']['senha'])) {
			if ($user->senha == $_POST['Usuario']['senha'])
				$user->addError('senha', 'A senha não pode ser igual a anterior');
			else {
				$user->first = 1;
				$user->senha = $_POST['Usuario']['senha'];
				if ($user->validate(array('senha'=>$_POST['Usuario']['senha']), false)){
					$user->saveAttributes(array('senha', 'first'));
					$this->redirect(array('/'));
				}
			}
		}
		$this->render('cp', array('model' => $user));
	}
	
	public function actionFindCC($term){
        $term = $term.'%';
        $data = CentroCusto::model()->findAll(array(
			'select'=>'centro_custo',
			'condition'=>'centro_custo like :centro_custo',
			'params'=>array(':centro_custo'=>$term),
			'limit'=>10,
			'distinct' => true,
		));
        $res = array();
		//var_dump($data);
        if (!empty($data))
            foreach ($data as &$model)
                $res[] = array(
                    'id'          =>$model->centro_custo,
                    'label'       =>$model->centro_custo,
                    'value'       =>$model->centro_custo,
                );

        echo CJSON::encode($res);
	}

	public function actionFindOC($cc, $term){
        $term = $term.'%';
        $data = CentroCusto::model()->findAll(array(
			'select'=>'objeto_custo',
			'condition'=>'centro_custo = :centro_custo and objeto_custo like :objeto_custo',
			'params'=>array(':centro_custo'=>$cc, ':objeto_custo'=>$term),
			'limit'=>20,
			'distinct' => true,
		));
        $res = array();
		//var_dump($data);
        if (!empty($data))
            foreach ($data as &$model)
                $res[] = array(
                    'id'          =>$model->objeto_custo,
                    'label'       =>$model->objeto_custo,
                    'value'       =>$model->objeto_custo,
                );

        echo CJSON::encode($res);
	}
	
	public function actionEmpresa($empresa, $term){
        $term = $term.'%';
        $data = Empresa::model()->findAll(array(
			'select'=>'RSocial',
			'condition'=>'RSocial = :RSocial like :RSocial',
			'params'=>array(':empresa'=>$empresa, ':empresa'=>$term),
			'limit'=>20,
			'distinct' => true,
		));
        $res = array();
		//var_dump($data);
        if (!empty($data))
            foreach ($data as &$model)
                $res[] = array(
                    'id'          =>$model->RSocial,
                    'label'       =>$model->RSocial,
                    'value'       =>$model->RSocial,
                );

        echo CJSON::encode($res);
	}

	/* Change centro_custo */
	public function actionCc() {
		$user = Usuario::model()->findByPk(Yii::app()->user->id);
		if (!empty($user->centro_custo) && !empty($user->objeto_custo)) $this->redirect(array('/'));
		
		if (isset($_POST['Usuario'])) {
			$user->centro_custo = $_POST['Usuario']['centro_custo'];
			$user->objeto_custo = $_POST['Usuario']['objeto_custo'];
			
			if ($user->validate(array('centro_custo', 'objeto_custo'))){
				$user->save(false);
				$this->redirect(array('/'));
			}
		}
		$this->render('cc', array('model' => $user));
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact() {
		$model = new ContactForm;
		if (isset($_POST['ContactForm'])) {
			$model->attributes = $_POST['ContactForm'];
			if ($model->validate()) {
				$name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
				$subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
				$headers = "From: $name <{$model->email}>\r\n" .
						"Reply-To: {$model->email}\r\n" .
						"MIME-Version: 1.0\r\n" .
						"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
				Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact', array('model' => $model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin() {
		$model = new LoginForm;

		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login()) {
				/*
				$user = Usuario::model()->findByPk(Yii::app()->user->id);
				if ($user->first == 0)
					Yii::app()->controller->redirect(array('/site/cp/'));
				else {
					$this->redirect(Yii::app()->user->returnUrl);
				}
				*/
				$this->redirect(Yii::app()->user->returnUrl);
			}
		}
		// display the login form
		$this->render('login', array('model' => $model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout() {
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionGetLocal($term) {
		$term = '%' . $term . '%';
		$models = Usuario::model()->findAll(array(
			'select' => 'local',
			'condition' => 'local like :term',
			'params' => array(':term' => $term),
			'distinct' => true,
		));

		$data = array();
		if (!empty($models))
			foreach ($models as &$user)
				$data[$user->local] = array(
					'id' => $user->local,
					'label' => $user->local,
					'value' => $user->local,
				);

		echo CJSON::encode($data);
	}

	public function actionGetUsuario($term) {
		$term = '%' . $term . '%';
		$models = Usuario::model()->findAll(array(
			'select' => 'id, nome, email, empresa',
			'condition' => 'email like :term',
			'params' => array(':term' => $term),
			'distinct' => true,
		));

		$data = array();
		if (!empty($models))
			foreach ($models as &$user)
				$data[] = array(
					'id' => $user->id,
					//'empresa' => $user->empresa,
					'label' => $user->nome . (empty($user->email) ? '' : " <{$user->email}>"),
					'value' => $user->email,
				);

		echo CJSON::encode($data);
	}

	public function actionConfirm($c) {
		$model = Usuario::model()->find('hash = :hash', array(':hash' => $c));
		if (empty($model)) {
			$this->render('invalidregistercode', array('model' => $model));
			return;
		}
		$model->hash = '';
		$model->confirmado = 1;
		$model->save();
		$this->render('registerconfirm', array('model' => $model));
	}

	public function actionModUser() {
		$model = Usuario::model()->findByPk(Yii::app()->user->id);
		$model->attributes = $_POST['Usuario'];
		if (!$model->save())
			$this->renderPartial('/usuario/userEdit', array('model' => $model));
	}

	public function actionUserEditPass() {
		$model = Usuario::model()->findByPk(Yii::app()->user->id);
		if ($model) {
			$ultimaSenha = $model->senha;
			$model->scenario = 'mod_senha';
			$model->attributes = $_POST['Usuario'];
			if ($model->save()) {
				$hs = new HistoricoSenha();
				$hs->usuario = Yii::app()->user->id;
				$hs->hash = $ultimaSenha;
				$hs->date = date('Y-m-d');
				if (!$hs->save()) $model->addErrors($hs->getErrors());
				return;
			}
			$this->renderPartial('/usuario/userEditPass', array('model' => $model));
		}
		
	}

	public function actionRegister() {
		$mc = 'Usuario';
		$model = new Usuario('register');

		if (isset($_POST[$mc])) {

         $model->attributes = $_POST[$mc];			
			
			include 'conexao\conexao.php';
			
			$empresa = $model->empresa;
			
			//pega id do perfil 
			$sql = "SELECT id FROM tb_perfil WHERE empresa_perfil = '$empresa' AND nome LIKE 'admin%'";
			    $resultado_sql = mysql_query($sql, $conecta) or print_r(mysql_error());
			         $id = mysql_fetch_assoc($resultado_sql);
			 
			 
			
			$model->ativo = 1;
			$model->first = 1;
			$model->perfil = 5;
			$model->confirmado = 0;
			$model->empresa = $empresa;
            $model->perfil = $id['id'];	
			
			$model->hash = md5($model->email . mt_rand());
			if ($model->save()) {
				$message = new YiiMailMessage('Registro de e-mail');
				$message->view = 'register';

				$message->setBody(array('model' => $model), 'text/html');

				$message->addTo($model->email);
				$message->from = Yii::app()->params['registerEmail'];
				Yii::app()->mail->send($message);

				$this->render('register2', array('model' => $model));
				Yii::app()->end();
			}
		}

		$this->render('register', array('model' => $model));
	}

	public function actionRenew() {
		if (isset($_POST['LoginForm']['username'])) {
			$model = Usuario::model()->find('email=:email', array(':email' => $_POST['LoginForm']['username']));
			if (!$model)
				Yii::app()->user->setFlash('error', "E-mail não encontrado.");
			elseif (!$model->ativo)
				Yii::app()->user->setFlash('error', "Este usuário está inativo. Contate o administrador do sistema.");
			else {
				$model->hash = md5($model->email . mt_rand());
				if ($model->save()) {
					$message = new YiiMailMessage('Recuperação de senha');
					$message->view = 'renew';

					$message->setBody(array('model' => $model), 'text/html');

					$message->addTo($model->email);
					$message->from = Yii::app()->params['registerEmail'];
					Yii::app()->mail->send($message);

					$this->renderText('Sua senha foi enviada para o seu e-mail.');
					Yii::app()->end();
				}
			}
		}
		$this->render('renew');
	}

	public function actionRenewconfirm($c) {
		$model = Usuario::model()->find('hash = :hash', array(':hash' => $c));
		if (empty($model))
			throw new CHttpException(404, 'O código de confirmação esta errado ou não foi encontrado.');
		if (Yii::app()->request->isPostRequest) {
			$model->scenario = 'mod_senha';
			$model->attributes = $_POST['Usuario'];
			$model->hash = '';
			if ($model->save()) {
				$this->renderText('Sua senha foi modificada com sucesso.');
				return;
			}
		}
		$this->render('renewconfirm', array('model' => $model));
	}
	

}
