<?php

class ModificarMinhaSenhaController extends Controller{
	public $layout = '//layouts/crud';
	
	public function accessRules() {
		return array(
			array('deny', 'expression'=>'$user->isGuest'),
			array('allow', 'expression'=>'$user->isAdmin'),
			//array('allow', 'expression'=>'!Yii::app()->user->isGuest && Yii::app()->user->perms["criar_carta"] == 1'),
			array('allow', 'users' => array('*')),
			array('deny', 'users' => array('*')),
		);
	}
	public function loadModel($id) {
		         //Nome tabela.
		$model = ModificarMinhaSenha::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	} 

	public function filters() {
		return array('accessControl');

	}
	
    public function actionIndex() {
        
	   $model = new ModificarMinhaSenha('search');
		$model->unsetAttributes();  
		if (isset($_GET['ModificarMinhaSenha']))
			$model->attributes = $_GET['ModificarMinhaSenha'];

	    $this->render('ModificarMinhaSenha', array('model' => $model));   
             
      }
	
	
	public function actionGetUsuarios($term) {
		$term = '%'.$term.'%';
		$models = Usuario::model()->findAll('email like :term', array(':term'=>$term));
		$data = array();
		if (!empty($models))
			foreach ($models as &$user)
				$data[$user->email] = array(
					'id'    =>$user->email,						
					'label' =>$user->email,						
					'value' =>$user->email,						
					'email' =>$user->email,						
					'nome'  =>$user->nome,
					'local' =>$user->local,
					'andar' =>$user->andar,
					'baia'  =>$user->baia,
					'ramal' =>$user->ramal,
					'area'  =>$user->area,
				);

		echo CJSON::encode($data);		
	}
}
