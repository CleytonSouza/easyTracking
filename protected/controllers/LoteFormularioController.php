<?php

class LoteFormularioController extends Controller {
	public $layout = '//layouts/crud';
	
	public function accessRules() {
		return array(
			array('allow', 'expression'=>'$user->isAdmin'),
			array('allow', 'expression'=>'Yii::app()->user->isGuest && Yii::app()->user->perms["criar_lote_formulario"] == 1'),
			array('allow','users' => array('*')),
			array('deny', 'users' => array('*')),
		);
	}
	
	public function filters() {
		return array('accessControl');
	}
		
	public function actionIndex() {
		$model = new LoteFormulario();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['LoteFormulario'])) {
			$model->attributes = $_POST['LoteFormulario'];
                        if (!isset($_POST['carta']) || count($_POST['carta']) <= 0){
							$model->addError ('', 'O campo código de barras não foi preenchido');
						}
                        
			if (isset($_POST['carta'])){
			
				if ($model->validate()) {
					
					$sendData = array('cartas'=>implode(',',$_POST['carta']));
					$sendData = array_merge($sendData, $model->attributes);
					unset($sendData['tracks']);

					include 'barcode\Barcode39.php';
					include 'conexao\conexao.php'; 
								   
				   //Variavel
                    $tracking = "L".date('YmdHis').rand(1,20);
                    $remetente = $model->de;
                    $destinatario = $model->para;
					$local_origem = $model->local_origem;
                    $local_destino = $model->local_destino;
                    $obs = $model->obs;
                    $transp = $model->transporte;
                    $data_chegada = $model->data_chegada;
					$d_email = $model->d_email;
					$u_empresa = $model->u_empresa;

                    if(isset($data_chegada)){
	                    $chegada = $data_chegada;
                    }

                         else{
	                          $chegada = NULL;
                         }

					
                    $ts_impressao = date('Y-m-d H:i:s');
                    $tipo = "Impresso";
                    $tracks = utf8_encode($sendData['cartas']);
					$lote = "Lote";


					
					
                                               
                   $result = mysql_query("INSERT INTO easytracking.tb_track (tracking, d_email, u_empresa, track_cartas, local_origem, local_destino, data_chegada, remetente, destinatario, obs, transp, ts_impressao, tipo, lote) VALUES 
                   ('".$tracking."', '".$d_email."', '".$u_empresa."', '".$tracks."', '".$local_origem."', '".$local_destino."', '".$chegada."',  '".$remetente."', 
                   '".$destinatario."', '".$obs."', '".$transp."', '".$ts_impressao."', '".$tipo."', '".$lote."')")or die(mysql_error());

                             
                            // display new barcode 
                            $bc = new Barcode39($tracking);
                   
                   define('MPDF_PATH', 'pdf/');
                        include(MPDF_PATH.'mpdf.php');
                        $mpdf=new mPDF();
						
						$conteudo_pdf = '<!DOCTYPE html>';
						$conteudo_pdf .= '<body>';
						$conteudo_pdf .= '<p style="text-align:center; font-weight:bold; padding-top:5mm;">Malote</p>';
						$conteudo_pdf .= '<b>De: </b> ' . $remetente. '<br />'. '<br />';
						$conteudo_pdf .= '<b>Para: </b>' . $destinatario. '<br />'. '<br />';
                        $conteudo_pdf .= '<b>Cartas: </b>' .$sendData['cartas'] .'<br />';
						$conteudo_pdf .= '</body>';
						$conteudo_pdf .= '</html>';
						
						
                        $mpdf->WriteHTML($conteudo_pdf);
					   
						
						$mpdf->Output('documentos/malote/'.$tracking.'.pdf','F');
						
						$link = '/pitneybowes/documentos/malote/'.$tracking.'.pdf';
                    
						Yii::app()->user->setFlash('success', 'Lote gerado com sucesso.<strong><a href="'.$link.'">'.$tracking.'</a><a href="http://easypb.pitneybowes.com.br/pitneybowes/gera_codigo_barras/codigo_barras.php?tracking='.$tracking.'"><br>Clique aqui para imprimir o código de barras</a></strong>');

				                 
             //  Yii::app()->user->setFlash('success', 'Protocolo gerado com sucesso.<strong><a href="http://easypb.pitneybowes.com.br/pitneybowes/gera_codigo_barras/codigo_barras.php?tracking='.$tracking.'">Clique aqui para imprimir o código de barras</a></strong>.');
				$model = new Lote();
				}	
					else 
						$model->addError ('', 'Falha na geração do arquivo. Contate o suporte');
                                
		}
                }
		$this->render('form', array('model' => $model));
	}
}