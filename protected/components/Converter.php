<?php

class Converter {
	public static function formatDateTime($dateTime){
		if ($dateTime == '0000-00-00 00:00:00') $dateTime = null;
		return Yii::app()->locale->dateFormatter->formatDateTime($dateTime);
	}

	public static function formatDateTimeToDB($dateTime){
		if (empty($dateTime)) return null;
		$ar = explode('/', $dateTime);
		return $ar[2].'-'.$ar[1].'-'.$ar[0];
	}
	
	public static function formatFloat($val){
		$val .='.00';
		$p1 = strpos($val, '.')+3;
		$p2 = strlen($val)-$p1+1;
		return str_replace('.', ',', substr($val, 0, $p1));
	}
}
