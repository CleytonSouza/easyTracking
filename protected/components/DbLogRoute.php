<?php

class DbLogRoute extends CDbLogRoute {

    protected function createLogTable($db,$tableName) {
        $db->createCommand()->createTable($tableName, array(
            'id'=>'pk',
            'level'=>'varchar(128)',
            'user'=>'varchar(128)',
            'ip'=>'varchar(40)',
            'category'=>'varchar(128)',
            'logtime'=>'integer',
            'message'=>'text',
        ));
    }

    protected function processLogs($logs) {
        $command=$this->getDbConnection()->createCommand();
        foreach($logs as $log) {
            $command->insert($this->logTableName,array(
                'level'=>$log[1],
                'user'=>Yii::app()->user->name,
                'ip'=>Yii::app()->request->getUserHostAddress(),
                'category'=>$log[2],
                'logtime'=>(int)$log[3],
                'message'=>$log[0],
            ));
        }
    }

} 