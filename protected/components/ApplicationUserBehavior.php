<?php

class ApplicationUserBehavior extends CBehavior
{
    /**
     * Declares events and the event handler methods
     * See yii documentation on behavior
     */
    public function events()
    {
        return array_merge(parent::events(), array(
            'onBeginRequest'=>'beginRequest',
        ));
    }

    /**
     * Load configuration that cannot be put in config/main
     */
    public function beginRequest() {
		
        $data=yii::app()->db->createCommand()
            ->select('invalid')
            ->from('sessions')
            ->where('id=:id',array(':id'=>Yii::app()->session->sessionID))
            ->queryScalar();
        if ($data == 1) Yii::app()->user->logout();
		
		if (!Yii::app()->user->isGuest){

			if (strpos(Yii::app()->request->url, Yii::app()->createUrl('/site/logout/'))  !== false) return; 
			if (strpos(Yii::app()->request->url, Yii::app()->createUrl('/site/cp/'))      !== false) return; 
			#if (strpos(Yii::app()->request->url, Yii::app()->createUrl('/site/cc/'))      !== false) return; 
			#if (strpos(Yii::app()->request->url, Yii::app()->createUrl('site/findCC/'))   !== false) return; 
			#if (strpos(Yii::app()->request->url, Yii::app()->createUrl('/site/findOC/'))  !== false) return; 
			$user = Usuario::model()->findByPk(Yii::app()->user->id);
		
			if ($user->first == 0) {
				Yii::app()->request->redirect(Yii::app()->createUrl('/site/cp/'));
				return;
			}
			#if (empty($user->centro_custo) || empty($user->objeto_custo)) {
			#	Yii::app()->request->redirect(Yii::app()->createUrl('/site/cc/'));
			#	return;
			#} 
		}
    }
}