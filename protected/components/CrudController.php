<?php

abstract class CrudController extends Controller{
	public $layout = '//layouts/crud';
	public $modelClass = null;
	
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', 
		);
	}

	public function accessRules() {
		return array(
			
			// allow authenticated user to perform 'create' and 'update' actions
			array('allow', 
				'actions' => array('index', 'view', 'list'),
				'users' => array('@'),
			),
			
			// allow admin user to perform 'admin' and 'delete' actions
			array('allow', 
				'actions' => array('create', 'update', 'delete'),
				'expression'=>'$user->isAdmin',
			),
			
			// deny all users
			array('deny', 'users' => array('*'),
			),
		);
	}
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id) {
		$this->render('view', array('model' => $this->loadModel($id)));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
		$mc = $this->modelClass;
		$model = new $mc;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST[$mc])) {
			$model->attributes = $_POST[$mc];
			if ($model->save())
				$this->redirect(array('index', 'id' => $model->id));
		}

		$this->render('form', array('model' => $model));
	}	
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id) {
		$mc = $this->modelClass;
		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST[$mc])) {
			$model->attributes = $_POST[$mc];
			if ($model->save())
				$this->redirect(array('index', 'id' => $model->id));
		}

		$this->render('form', array('model' => $model));
	}
	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		} else
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
	}
	
	/**
	 * Manages all models.
	 */
	public function actionIndex() {
		$mc = $this->modelClass;
		$model = new $mc('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET[$mc]))
			$model->attributes = $_GET[$mc];

		$this->render('index', array('model' => $model));
	}
	
	/**
	 * Lists all models.
	 */
	public function actionList() {
		$dataProvider = new CActiveDataProvider($this->modelClass);
		$this->render('list', array('dataProvider' => $dataProvider));
	}
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id) {
		$mc = $this->modelClass;
		$model = $mc::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}	

	protected function performAjaxValidation($model) {
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	

}
