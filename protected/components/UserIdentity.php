<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {
	
	private $_id = null;
	private $_name = null;

	public function authenticate() {
		$model = Usuario::model()->find('email = :email and senha = :senha', array(
			':email'=>$this->username,
			':senha'=>$this->password,
		));
		
		if (empty($model)) $this->errorCode = self::ERROR_UNKNOWN_IDENTITY;
		elseif (!$model->ativo) $this->errorCode = self::ERROR_INACTIVE;
		else {
			$model->hash = null;
			$model->save();
			$this->setState('userData', $model->attributes);
			
			$this->errorCode = self::ERROR_NONE;
			$perfil = Perfil::model()->findByPk($model->perfil);
			if (empty($perfil)) 
				$perfil = new Perfil();

			$this->_id = $model->id;
			$this->setState('perms', $perfil->attributes);
			$this->setState('isAdmin', $model->email == Yii::app()->params['adminEmail'] || $perfil->admin);
			$this->setState('isTransp', $perfil->transp);
			$this->setState('isGeren', $perfil->geren);
			$this->setState('email', $model->email);
			$this->setState('empresa', $model->empresa);
			$this->setState('perfil', $model->perfil);
			$this->setState('local', $model->local);
			$this->setState('centro_custo', $model->centro_custo);
			$this->_name = $model->nome;
			
		}
		return !$this->errorCode;
	}
	
	public function getName() {
		return $this->_name;
	}	
	
	public function getId(){
		return $this->_id;
	}

}
