<?php

class ASession extends CDbHttpSession {

    //public function getSessionID() {
    //    return Yii::app()->user->isGuest? -1 : Yii::app()->user->id;
   // }

    protected function createSessionTable($db,$tableName) {
        switch($db->getDriverName())  {
            case 'mysql':
                $blob='LONGBLOB';
                break;
            case 'pgsql':
                $blob='BYTEA';
                break;
            case 'sqlsrv':
            case 'mssql':
            case 'dblib':
                $blob='VARBINARY(MAX)';
                break;
            default:
                $blob='BLOB';
                break;
        }
        $db->createCommand()->createTable($tableName,array(
            'id'=>'CHAR(32) PRIMARY KEY',
            'expire'=>'integer',
            'data'=>$blob,
            'user'=>'varchar(45)',
            'ip'=>'varchar(19)',
            'invalid'=>'integer',
        ));
    }

    public function writeSession($id,$data) {
        try {
            $expire=time()+$this->getTimeout();
            $db=$this->getDbConnection();
            $db->createCommand()->update($this->sessionTableName, array('invalid'=>1), 'user=:user and id<>:id', array(':user'=>Yii::app()->user->id, ':id'=>$id));
            if($db->getDriverName()=='sqlsrv' || $db->getDriverName()=='mssql' || $db->getDriverName()=='dblib')
                $data=new CDbExpression('CONVERT(VARBINARY(MAX), '.$db->quoteValue($data).')');
            if($db->createCommand()->select('id')->from($this->sessionTableName)->where('id=:id',array(':id'=>$id))->queryScalar()===false)
                $db->createCommand()->insert($this->sessionTableName,array(
                    'id'=>$id,
                    'data'=>$data,
                    'expire'=>$expire,
                    'user'=>Yii::app()->user->id,
                    'invalid'=>0,
                ));
            else
                $db->createCommand()->update($this->sessionTableName,array(
                    'data'=>$data,
                    'expire'=>$expire,
                    'user'=>Yii::app()->user->id,
                    'invalid'=>0,
                ),'id=:id',array(':id'=>$id));
        }
        catch(Exception $e) {
            if(YII_DEBUG)
                echo $e->getMessage();
            // it is too late to log an error message here
            return false;
        }
        return true;
    }
/*
    public function readSession($id)  {
        $db=$this->getDbConnection();
        if($db->getDriverName()=='sqlsrv' || $db->getDriverName()=='mssql' || $db->getDriverName()=='dblib')
            $select='CONVERT(VARCHAR(MAX), data)';
        else
            $select='data';
        $data=$db->createCommand()
            ->select($select)
            ->from($this->sessionTableName)
            ->where('expire>:expire AND id=:id',array(':expire'=>time(),':id'=>$id))
            ->queryScalar();
        return $data===false?'':$data;
    }
*/
}