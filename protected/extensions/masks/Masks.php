<?php

if (!defined('DS')) define('DS', DIRECTORY_SEPARATOR);

class Masks extends CWidget {
  
  protected $clientScript;
  
  public $selector = null;
  public $options = null;
  
  public function init() {
      $this->clientScript = Yii::app()->clientScript;
  }

  private function registerScripts() {
      $this->clientScript->registerCoreScript('jquery');
      $publishUrlBase = Yii::app()->assetManager->publish(dirname(__FILE__).DS.'assets');
      $this->clientScript->registerScriptFile($publishUrlBase.'/jquery.meio.mask.min.js');
  }

  private function registerStartScript() {
      if (is_null($this->selector)) $this->selector = 'input:text';

      if (is_array($this->selector)) $selector = implode(',',$this->selector);
      else $selector = $this->selector;

      $js = str_replace('\"', "'", '$("'.$selector.'").setMask('. CJSON::encode($this->options) .');');

      $scriptId = __CLASS__.'.'.md5($selector);
      $this->clientScript->registerScript($scriptId, $js);
  }

  public function run() {
      $this->registerScripts();
      $this->registerStartScript();
  }
}
