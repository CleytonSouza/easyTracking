<style>
    #modUserForm div {
        height: 35px;
    }
</style>
<?php
  $model->senha = '';
  $model->confirmaSenha = '';
  $model->scenario = 'mod_senha';
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'modEditPassForm',
   // 'type'=>'horizontal',
	'enableAjaxValidation'=>false,
    //'hideInlineErrors'=>true,
)); ?>

	<?php echo $form->errorSummary($model); ?></span>
    <?php echo $form->labelEx($model,'senha', array('label'=>'Nova senha')); ?>
    <?php echo $form->passwordField($model,'senha', array('class'=>'span3')); ?>
    <?php echo $form->labelEx($model,'confirmaSenha'); ?>
    <?php echo $form->passwordField($model,'confirmaSenha', array('class'=>'span3')); ?>

	<div><?php //echo $form->passwordFieldRow($model,'senha',array('class'=>'span3','maxlength'=>12)); ?></div>
    <div><?php //echo $form->passwordFieldRow($model,'confirmaSenha',array('class'=>'span3','maxlength'=>12)); ?></div>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Modificar senha',
        )); ?>
    </div>
	
<?php $this->endWidget(); ?>
