<?php
$this->breadcrumbs=array(
    'Registro de usuários',
);
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'login-form',
    'inlineErrors'=>true,
    'type'=>'horizontal',
    'clientOptions'=>array(
        //'validateOnSubmit'=>true,
    ),
)); ?>
<?php
        Yii::app()->controller->widget('application.extensions.masks.Masks', array(
            'selector'=>'#tel',
            'options'=>array(
                'mask'=>'99 9999-99999',
                //'type'=>'reverse',
                'defaultValue'=>'',
            ),
        ));
?>
    <script type="text/javascript">
        function complete_campos(values){
            $('#Usuario_local').val(values.logradouro);
            $('#Usuario_numero').val(values.numero);
            $('#Usuario_andar').val(values.andar);
            $('#Usuario_baia').val(values.complemento);

            $('#Usuario_bairro').val(values.bairro);
            $('#Usuario_cidade').val(values.cidade);
            $('#Usuario_uf').val(values.uf);
            $('#Usuario_cep').val(values.cep);

        }
    </script>

<?php echo $form->errorSummary($model); ?>
<?php echo $form->textFieldRow($model,'nome',array('class'=>'span6','maxlength'=>60)); ?>
<?php echo $form->textFieldRow($model,'email',array('class'=>'span6','maxlength'=>60)); ?>
<?php echo $form->passwordFieldRow($model,'senha',array('class'=>'span2','maxlength'=>32)); ?>
<?php echo $form->passwordFieldRow($model,'confirmaSenha',array('class'=>'span2','maxlength'=>32)); ?>
<?php echo $form->textFieldRow($model,'matricula',array('class'=>'span2','maxlength'=>45));?>
<?php //echo $form->textFieldRow($model,'nome_local',array('class'=>'span5','maxlength'=>60)); ?>

    <div class="control-group ">
        <label class="control-label required" for="Usuario_nome_local">
            Local de Trabalho
            <span class="required">*</span>
        </label>
        <div class="controls">
            <?php
            $this->widget('zii.widgets.jui.CJuiAutoComplete',array(
                'model'=>$model,
                'attribute'=>'nome_local',
                'sourceUrl'=>$this->createUrl('/local'),
                'options'=>array('minLength'=>'2', 'select'=> 'js:function( event, ui ) {complete_campos(ui.item)}'),
                'htmlOptions'=>array('class'=>'span5'),
            ));
            ?>
        </div>
    </div>

<?php echo $form->textFieldRow($model,'local',array('class'=>'span5','maxlength'=>60)); ?>
<?php echo $form->textFieldRow($model,'numero',array('class'=>'span2','maxlength'=>45)); ?>
<?php echo $form->textFieldRow($model,'andar',array('class'=>'span1','maxlength'=>45)); ?>
<?php echo $form->textFieldRow($model,'baia',array('class'=>'span3','maxlength'=>45)); ?>
<?php echo $form->textFieldRow($model,'bairro',array('class'=>'span5','maxlength'=>60)); ?>
<?php echo $form->textFieldRow($model,'cidade',array('class'=>'span5','maxlength'=>60)); ?>
<?php echo $form->textFieldRow($model,'uf',array('class'=>'span1','maxlength'=>2)); ?>
<?php echo $form->textFieldRow($model,'cep',array('class'=>'span1','maxlength'=>9)); ?>
<?php echo $form->textFieldRow($model,'ramal',array('id'=>'tel', 'class'=>'span2','maxlength'=>45)); ?>
<?php echo $form->textFieldRow($model,'area',array('class'=>'span2','maxlength'=>45)); ?>
<?php echo $form->textFieldRow($model,'email_gestor_responsavel',array('class'=>'span5','maxlength'=>60)); ?>
<?php echo $form->textFieldRow($model,'centro_custo',array('class'=>'span5','maxlength'=>60)); ?>
<?php echo $form->dropDownListRow($model,'empresa', array('Arezzo'=>'Arezzo')); ?>

 
    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Registrar',
        )); ?>
    </div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	function executeAutoCompleteObjetoCusto(){
		jQuery('#Usuario_objeto_custo').autocomplete({'minLength':'1','source': "<?php echo $this->createUrl('/site/findOC', array('cc'=>'')); ?>"+$("#Usuario_centro_custo").val()});
	}
	
	$().ready(function(){
		$('#Usuario_centro_custo').blur(function(){executeAutoCompleteObjetoCusto()});
	})
</script>