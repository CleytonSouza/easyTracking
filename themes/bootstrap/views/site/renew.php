<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */
$model = new LoginForm();
$this->pageTitle=Yii::app()->name . ' - Recuperar senha';
$this->breadcrumbs=array('Recuperar senha');
?>

<h2>Recuperar senha</h2>
Será enviado para você um código de confirmação para o e-mail informado no seu cadastro. <br /><br />

<?php
$flashMessages = Yii::app()->user->getFlashes();
if ($flashMessages) {
    echo '<div class="flashes">';
    foreach($flashMessages as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
    echo '</div>';
}
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'renew-form',
	'inlineErrors'=>false,
    'type'=>'horizontal',
	'enableClientValidation'=>false,
)); ?>
	
	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'username'); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Enviar',
        )); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->