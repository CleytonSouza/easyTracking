<?php
$showMenu = false;
$perfil = new Perfil();
$user = Usuario::model()->findByPk(Yii::app()->user->id);
$exibir_relatorios = false;
if (!Yii::app()->user->isGuest) {
	if     (strpos(Yii::app()->request->url, Yii::app()->createUrl('/site/logout/'))  !== false) $showMenu = false; 
	elseif (strpos(Yii::app()->request->url, Yii::app()->createUrl('/site/cp/'))      !== false) $showMenu = false; 
	elseif (strpos(Yii::app()->request->url, Yii::app()->createUrl('/site/cc/'))      !== false) $showMenu = false;
	elseif (strpos(Yii::app()->request->url, Yii::app()->createUrl('site/findCC/'))   !== false) $showMenu = false;
	elseif (strpos(Yii::app()->request->url, Yii::app()->createUrl('/site/findOC/'))  !== false) $showMenu = false;
	else $showMenu = true;
	if ($showMenu) $perfil = Usuario::model()->findByPk(Yii::app()->user->id)->Perfil;
	
	$exibir_relatorios = $perfil->rel_cr || $perfil->rel_ce || $perfil->rel_motoboy;
	
}

?>

<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="en" />
        <link rel="shortcut icon" href="images/icone.ico"> 
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />
		<title><?php echo CHtml::encode($this->pageTitle); ?></title>
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/main.css" />

		<?php Yii::app()->bootstrap->register(); ?>
	</head>

	<body>
		<?php
			$this->widget('bootstrap.widgets.TbNavbar', array(
				'type' => 'inverse',
				'brand'=>  CHtml::image('images/logonovo.png'),
				'fluid'=>true,
				'collapse'=>true,
				'items' => array(
					array(
						'class' => 'bootstrap.widgets.TbMenu',
						'items' => array(
#Malote
/*array('label' => 'Malotes', 'url' => array('/movimento'), 'active'=>false, 'visible' => !Yii::app()->user->isGuest && $perfil->movimento, 'items'=>array(
				 array('label' => 'Auditi', 'url' => array('/movimento'), 'active'=>false, 'visible' => $perfil->movimento),
				 array('label' => 'Fechar Malote', 'url' => array('/malote'), 'active'=>false, 'visible' => $perfil->criar_malote)
				 array('label' => 'Criar carta', 'url' => array('/carta'), 'active'=>false, 'visible' => $perfil->criar_carta),
				 )),*/
#Moto-Boy
#array('label' => 'Motoboy', 'url' => array('#'), 'active'=>false, 'visible' => !Yii::app()->user->isGuest && $perfil->esporadico , 'items'=>array(
#				 array('label' => 'Solicitações', 'url' => array('/esporadico'), 'active'=>false, 'visible' => $perfil->esporadico),
				 #array('label' => 'Criar', 'url' => array('/esporadico/criar'), 'active'=>false, 'visible' => $perfil->criar_esporadico),
				 #array('label' => 'SLA', 'url' => array('/AuditSla/'), 'active'=>false, 'visible' => $perfil->audit),
#				 )),	
#Moto-Boy
				 array('label' => 'Motoboy', 'url' => array('#'), 'active'=>false, 'visible' => !Yii::app()->user->isGuest && $perfil->esporadico , 'items'=>array(
				 	array('label' => 'Solicitações', 'url' => array('/esporadico'), 'active'=>false, 'visible' => $perfil->esporadico),
				    array('label' => 'Reversa', 'url' => array('/esporadico/criar'), 'active'=>false, 'visible' => $perfil->criar_esporadico),
				 	array('label' => 'Ocorrência Coleta', 'url' => array('/esporadico/ocorrenciaColeta'), 'active'=>false, 'visible' => $perfil->ocorrencia_esporadico),
				 	array('label' => 'Ocorrência Entrega', 'url' => array('/esporadico/ocorrenciaEntrega'), 'active'=>false, 'visible' => $perfil->ocorrencia_esporadico),
				 	array('label' => 'SLA', 'url' => array('/AuditSla/'), 'active'=>false, 'visible' => $perfil->audit),
				 	)),	
#Protocolo
#array('label' => 'Protocolo', 'url' => array('#'), 'active'=>false, 'visible' => !Yii::app()->user->isGuest && $perfil->pesquisa_pre_impresso, 'items'=>array(
#		         array('label' => 'Pesquisa', 'url' => array('/protocoloMobile'), 'active'=>false, 'visible' => $perfil->pesquisa_pre_impresso),
#		           #impresso 
#		         array('label' => 'Criar', 'url' => array('/protocolo/criar'), 'active'=>false, 'visible' =>  $perfil->protocolo_criar),
#		           #pré-impresso      
#		         array('label' => 'Criar', 'url' => array('/protocoloMobile/criar'), 'active'=>false, 'visible' =>  $perfil->protocolo_pre_impresso),
						  
#				 array('label' => 'Lote', 'url' => array('/lote'), 'active'=>false, 'visible' => $perfil->criar_lote),
#				 array('label' => 'Lote Formulário', 'url' => array('/loteFormulario'), 'active'=>false, 'visible' => $perfil->criar_lote_formulario),
#				 array('label' => 'Pesquisa Unitário', 'url' => array('/pesquisaUnitario'), 'active'=>false, 'visible' => $perfil->pesquisar_lote),
#				
#				 )),	
#esporadico da empresa de motoboy
array('label' => 'Motoboy', 'url' => array('#'), 'active'=>false, 'visible' => !Yii::app()->user->isGuest && $perfil->motoboy_empresa, 'items'=>array(
				array('label' => 'Pesquisa', 'url' => array('/esporadicoEmpresa'), 'active'=>false, 'visible' => $perfil->motoboy_empresa),
				)),		
                
#array('label' => 'Relatórios', 'url' => array('/Relatorios'), 'active'=>false, 'visible' =>!Yii::app()->user->isGuest && $perfil->relatorios, 'items'=>array(							
#               array('label' => 'SLA', 'url' => array('/relatorioprotocolo/'), 'active'=>false, 'visible' => $perfil->relatorio), 
#              array('label' => 'Gráfico', 'url' => array('/relatorioesporadico/'), 'active'=>false, 'visible' => $perfil->relatorio), 
#							 
#					 )), #relatorios
 array('label' => 'Relatórios', 'url' => array('/Relatorios'), 'active'=>false, 'visible' =>!Yii::app()->user->isGuest && $perfil->relatorios, 'items'=>array(							
	 	#array('label' => 'SLA', 'url' => array('/relatorioprotocolo/'), 'active'=>false, 'visible' => $perfil->relatorio), 
	 	#array('label' => 'Ocorrência', 'url' => array('/esporadico/ocorrencia'), 'active'=>false, 'visible' => $perfil->ocorrencia_esporadico),
	 	#array('label' => 'Gráfico', 'url' => array('/relatorioesporadico/'), 'active'=>false, 'visible' => $perfil->relatorio), 

				 	array('label' => 'SLA', 'url' => array('#'), 'active'=>false, 'visible' => $perfil->relatorios, 'items'=>array(

				 		array('label' => 'Mês', 'url' => array('/relatorioprotocolo/'), 'active'=>false, 'visible' => $perfil->relatorio),
				 		array('label' => 'Diário', 'url' => array('/relatorioprotocolo/RelatorioAvulsodiario'), 'active'=>false, 'visible' => $perfil->relatorio), 
	 	               #array('label' => 'Ocorrência', 'url' => array('/esporadico/ocorrencia'), 'active'=>false, 'visible' => $perfil->ocorrencia_esporadico),
				 		)),

				 	array('label' => 'Gráfico', 'url' => array('/relatorioesporadico/'), 'active'=>false, 'visible' => $perfil->relatorio), 


					 )), #relatorios	 




				  ),#array inicio menu  - items
			   ),# array classe tb menu
					
			   


			   array(
					  'class' => 'bootstrap.widgets.TbMenu',
					  'htmlOptions' => array('class' => 'pull-right'),
					  'items' => array(
						  array('label' => 'Administração', 'url' => '#', 'visible' => !Yii::app()->user->isGuest && Yii::app()->user->isAdmin, 'items' => array(
									array('label' => 'Usuários', 'url' => array('/usuario')),
									array('label' => 'Perfil de acesso', 'url' => array('/perfil')),
									//array('label' => 'Relatório', 'url' => array('/relatorio')),
	//								'---',
	//								array('label' => 'Parâmetros', 'url' => '#'),
								)),#administração
		                        array('label' => 'Login', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
							    
							    array('label' => Yii::app()->user->name, 'url' => '#', 'id'=>'modUser', 'visible' => $showMenu, 'items' => array(
								
                                array('label' => 'Modificar meus dados', 'url' => 'modificar-meus-dados'),
								array('label' => 'Modificar minha senha', 'url' => 'modificar-minha-senha'),								
								array('label' => 'Logout', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest),
							)),
							array('label' => 'Logout', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest && !$showMenu),
						), #array items
					), #array menu administração
				), # items widget
			 )# array bootstrap
			);#fechando widget
		?>

		<div class="container" id="page">

			<?php if (isset($this->breadcrumbs)): ?>
				<?php
				$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
					'links' => $this->breadcrumbs,
				));
				?><!-- breadcrumbs -->
<?php endif ?>

<?php echo $content; ?>

			<div class="clear"></div>

		</div><!-- page -->

<?php
if (!Yii::app()->user->isGuest) :
$usuario = Usuario::model()->findByPk(Yii::app()->user->id);
echo '<span id="dialogArea">';
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'dlgUserEdit',
    'options'=>array(
        'title'=>'Modificar meus dados',
        'autoOpen'=>false,
        'width'=>'90%',
		'height'=>'580',
        'modal'=>true,
        'buttons' => array
        (
            'Salvar'=>'js:function(){$.post("'.$this->createAbsoluteUrl('/site/moduser').'", $("#modUserForm").serialize(), function(data, textStatus, jqXHR){if (data == "") {$("#dlgUserEdit").dialog("close"); location.reload(); } else $("#modUserArea").html(data);})}',
            'Cancelar'=>'js:function(){$("#dlgUserEdit").dialog("close");}',
        ),
    ),
));

echo '<span id="modUserArea">';
$this->renderPartial('/usuario/userEdit', array('model'=>$usuario));
echo '</span>';

$this->endWidget('zii.widgets.jui.CJuiDialog');

    $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
        'id'=>'dlgUserEditPass',
        // additional javascript options for the dialog plugin
        'options'=>array(
            'title'=>'Modificar minha senha',
            'autoOpen'=>false,
            'width'=>'300px',
            'modal'=>true,
            'buttons' => array
            (
                'Salvar'=>'js:function(){$.post("'.$this->createAbsoluteUrl('/site/userEditPass').'", $("#modEditPassForm").serialize(), function(data, textStatus, jqXHR){if (data == ""){ $("#dlgUserEditPass").dialog("close"); location.reload();} else $("#modUserPassArea").html(data);})}',
                'Cancelar'=>'js:function(){$("#dlgUserEditPass").dialog("close");}',
            ),
        ),
    ));

    echo '<span id="modUserPassArea">';
    $this->renderPartial('/usuario/userEditPass', array('model'=>$usuario));
    echo '</span>';
    $this->endWidget('zii.widgets.jui.CJuiDialog');
endif;
echo '</span>';
?>
    <script type="text/javascript">
        $('a[href$="modificar-meus-dados"]').click(function(){
            $("#dlgUserEdit").dialog("open"); return false;
        });

        $('a[href$="modificar-minha-senha"]').click(function(){
            $("#dlgUserEditPass").dialog("open"); return false;
        });
    </script>
	</body>
</html>