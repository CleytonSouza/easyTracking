<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="row">
    <div class="span" style="float: none;">
        <div id="content">
<?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
    'buttons'=>$this->toolbar,
)); ?>			
            <?php echo $content; ?>
<?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
    'buttons'=>$this->toolbar,
)); ?>			
        </div><!-- content -->
    </div>
</div>
<?php $this->endContent(); ?>
<script type="text/javascript">
	$('.btn-primary').click(function(){
	  $('#crud-form').submit();
	});
</script>