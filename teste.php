
<script>
// Set the original/default language
var lang = "en";
var currentClass = "currentLang";

// Load the language lib
google.load("language",1);

// When the DOM is ready....
window.addEvent("domready",function(){
	// Retrieve the DIV to be translated.
	var translateDiv = document.id("languageBlock");
	// Define a function to switch from the currentlanguage to another
	var callback = function(result) {
		if(result.translation) {
			translateDiv.set("html",result.translation);
		}
	};
	// Add a click listener to update the DIV
	$$("#languages a").addEvent("click",function(e) {
		// Stop the event
		if(e) e.stop();
		// Get the "to" language
		var toLang = this.get("rel");
		// Set the translation into motion
		google.language.translate(translateDiv.get("html"),lang,toLang,callback);
		// Set the new language
		lang = toLang;
		// Add class to current
		this.getSiblings().removeClass(currentClass);
		this.addClass(currentClass);
	});
});
</script>

<script src="https://www.google.com/jsapi?key=YOUR_GOOGLE_KEY"></script>
<div id="languages"><p>
	<a href="?lang=en" rel="en">English</a> / <a href="?lang=es" rel="es">Spanish</a> / <a href="?lang=it" rel="it">Italian</a> /
	<a href="?lang=fr" rel="fr">French</a>
</p></div>

<div id="languageBlock">
	<p>Lights go out and I can't be saved <br />
	Tides that I tried to swim against  <br />
	Brought me down upon my knees  <br />
	Oh I beg, I beg and plead </p>

	<p>Singin', come out if things aren't said  <br />
	Shoot an apple off my head  <br />
	And a, trouble that can't be named  <br />
	Tigers waitin' to be tamed </p>

	<p>Singing, yooooooooooooo ohhhhhh  <br />
	Yoooooooooooo ohhhhhh </p>

	<p>Home, home, where I wanted to go <br /> 
	Home, home, where I wanted to go  <br />
	Home, home, where I wanted to go  <br />
	Home, home, where I wanted to go</p>
</div>